[![Version](https://img.shields.io/static/v1?label=version&message=0.1.0&color=informational)]()
[![Maintenance](https://img.shields.io/badge/maintenance-activly--developed-brightgreen.svg)]()
[![Crates.io](https://img.shields.io/crates/v/dungeon-cell)](https://crates.io/crates/dungeon-cell)
[![docs.rs](https://img.shields.io/docsrs/dungeon-cell)](https://docs.rs/dungeon-cell/0.0.3/dungeon_cell/)
[![Crates.io](https://img.shields.io/crates/l/dungeon-cell)](#license)

# dungeon-cell

Cell and Cell-like types that can store any type without dynamic memory.

Currently only the [`DungeonCore`] primitive is implemented (unsized, cell, stack vec, ... are in progress).

##### Example
```rust
use dungeon_cell::{DungeonCore, layout_for};

// a default DungeonCore can store any 'static type that fits in the layout
let mut core = DungeonCore::<layout_for!(String)>::default();

// we can store a i32 and get it back
core.store(1234);
assert_eq!(core.take(), Some(1234i32));

// we can store a f64 and get it back
core.store(1.234);
assert_eq!(core.take(), Some(1.234f64));

// lets get adventurous and store a String
core.store(String::from("hello world"));
assert_eq!(core.take(), Some(String::from("hello world")));

// we can't take a type the core isn't storing
core.store(1234);
assert_eq!(core.take(), None::<f32>);

// we can borrow both unique and shared
core.store(1234);
*core.borrow_mut::<i32>().unwrap() += 10;
assert_eq!(core.borrow(), Some(&1244i32));
```

## Features

- `"alloc"` - Enable support for `alloc` types.
- `"std"` - Enable support for `std` types. Also enables `"alloc"`.
- `"unsize"` - Enable use of the nightly feature `unsize`. Requires a nightly compiler.
- `"many_arg_fn"` - Enable implementations for functions with 11-30 arguments.

## No-std Support

This crate is `#![no_std]` by default, it can be used anywhere Rust can.

## Minimum Supported Rust Version

Requires Rust 1.64.0.

This crate follows the ["Latest stable Rust" policy](https://gist.github.com/alexheretic/d1e98d8433b602e57f5d0a9637927e0c). The listed MSRV won't be changed unless needed.
However, updating the MSRV anywhere up to the latest stable at time of release
is allowed.

<br>

#### License

<sup>
Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in dungeon-cell by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
</sub>
