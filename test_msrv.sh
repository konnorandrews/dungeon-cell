#!/bin/bash

export RUSTFLAGS="-D warnings"

versions=(
  "1.70.0"
  "1.69.0"
  "1.68.0"
  "1.67.0"
  "1.66.0"
  "1.65.0"
  "1.64.0" 
)

for i in "${versions[@]}"
do
cargo +$i test --tests --features=std
if [ $? -ne 0 ]
then
  echo "\n $i failed"
  exit 1
fi

cargo +$i test --doc --features=std
if [ $? -ne 0 ]
then
  echo "\n $i failed"
  exit 1
fi
done

echo "All tests passed"
