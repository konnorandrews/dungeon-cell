[![Version](https://img.shields.io/static/v1?label=version&message={{version}}&color=informational)]()
[![Maintenance](https://img.shields.io/badge/maintenance-activly--developed-brightgreen.svg)]()
[![Crates.io](https://img.shields.io/crates/v/dungeon-cell)](https://crates.io/crates/dungeon-cell)
[![docs.rs](https://img.shields.io/docsrs/dungeon-cell)](https://docs.rs/dungeon-cell/0.0.3/dungeon_cell/)
[![Crates.io](https://img.shields.io/crates/l/dungeon-cell)](#license)

# {{crate}}

{{readme}}

<br>

#### License

<sup>
Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in dungeon-cell by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
</sub>
