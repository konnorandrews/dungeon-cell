//! Raw byte buffer to store other type's values.

use core::mem::MaybeUninit;
use core::ptr::{addr_of, addr_of_mut};

use crate::compile_time::{
    const_transmute, const_usize_assert, SizeSmallerOrEqualTo,
};
use crate::marker_traits::IsSize;

/// Array of maybe uninit bytes.
///
/// This type acts as a `[MaybeUninit<u8>; N]` where `N` is given by the generic `Size`.
#[repr(transparent)]
pub struct Buffer<Size: IsSize> {
    /// Byte buffer of type `[MaybeUninit<u8>; Size::VALUE]`.
    pub buffer: Size::Buffer,
}

impl<Size: IsSize> Copy for Buffer<Size> {}

impl<Size: IsSize> Clone for Buffer<Size> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<Size: IsSize> Buffer<Size> {
    /// Create uninitialized buffer.
    ///
    /// All bytes in the returned buffer will be in an uninitialized state
    /// and cannot be read.
    pub const fn uninit() -> Self {
        Self {
            buffer: Size::UNINIT_BUFFER,
        }
    }

    /// Create buffer storing a value.
    ///
    /// This creates a uninitialized buffer then moves the value into it.
    /// Bytes not needed to represent the value will be left in an uninitialized state.
    ///
    /// The returned buffer will very likely not be properly aligned to allow creating a
    /// borrow/pointer to the value. To fix this, wrap the returned buffer in a
    /// [`Aligned`][crate::align::Aligned].
    pub const fn new<T>(value: T) -> Self {
        const_usize_assert::<SizeSmallerOrEqualTo<T, Size>>();

        // SAFETY: We know `Size::Buffer` is an array of maybe uninit bytes.
        // And we know that it's size is equal to or larger than the size of `T`
        // because of the above assert. Thus, its safe to transmute from `T`
        // to the byte buffer.
        let buffer = unsafe { const_transmute::<T, Size::Buffer>(value) };

        Self { buffer }
    }

    /// Get a borrow of the byte buffer.
    pub const fn get(&self) -> &[MaybeUninit<u8>] {
        // SAFETY: `self.buffer` is `Size::VALUE` bytes long because of the definition
        // of `IsSize`. We don't need to worry about initialization because we
        // make a slize of maybe uninit bytes. We don't mutate the data while the lifetime
        // exists because `&self` is a borrow of the wrapper.
        unsafe {
            core::slice::from_raw_parts(
                addr_of!(self.buffer) as *const MaybeUninit<u8>,
                Size::VALUE,
            )
        }
    }

    /// Get a mutable borrow of the byte buffer.
    pub fn get_mut(&mut self) -> &mut [MaybeUninit<u8>] {
        // SAFETY: `self.buffer` is `Size::VALUE` bytes long because of the definition
        // of `IsSize`. We don't need to worry about initialization because we
        // make a slize of maybe uninit bytes. We don't mutate the data while the lifetime
        // exists because `&self` is a borrow of the wrapper.
        unsafe {
            core::slice::from_raw_parts_mut(
                addr_of_mut!(self.buffer) as *mut MaybeUninit<u8>,
                Size::VALUE,
            )
        }
    }

    /// Convert buffer into a value of type `T`.
    ///
    /// # Safety
    /// The buffer must be storing a valid value of type `T`.
    pub const unsafe fn into_value<T>(self) -> T {
        // Note: we don't have to worry about alignment here because the transmute
        // (like the std one) automatically forces the alignment.

        // SAFETY: By the safety requirement of the function we know the buffer
        // is storing a value of `T`. So we can transmute from the buffer to `T`.
        unsafe { const_transmute::<Size::Buffer, T>(self.buffer) }
    }

    /// Get a borrow of the byte buffer.
    pub const fn as_ptr(this: *const Self) -> *const MaybeUninit<u8> {
        // The struct is `repr(transparent)` so the pointer will be to the wrapped array.
        this as _
    }

    /// Get pointer to first byte.
    ///
    /// This is useful for unsafe code.
    pub const fn as_ptr_mut(this: *mut Self) -> *mut MaybeUninit<u8> {
        // The struct is `repr(transparent)` so the pointer will be to the wrapped array.
        this as _
    }
}
