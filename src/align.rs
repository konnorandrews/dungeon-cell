//! Alignment forcing wrappers.

mod unaligned;

use crate::compile_time::const_transmute;
use crate::marker_traits::IsAlignment;

pub use unaligned::Unaligned;

/// Force minimum alignment of a type.
///
/// # Invariants
///
/// The wrapped [`Self::value`] is guaranteed, for specifically unsafe code, to have
/// a minimum alignment of [`Alignment::VALUE`][crate::marker_traits::IsAlignment::VALUE]
/// bytes. The wrapped `value` is also guaranteed
/// to have a zero offset from the start of the alignment block. This makes
/// it sound to construct a reference the wrapped `value` for a different type
/// with an alignment of maximum [`Alignment::VALUE`][crate::marker_traits::IsAlignment::VALUE]
/// bytes, given the other unsafe rules
/// are followed.
///
/// Additionally the layout of this struct is `#[repr(C)]` with a single field
/// `value` of type `T` and a private zero sized type with an alignment of
/// [`Alignment::VALUE`][crate::marker_traits::IsAlignment::VALUE] bytes.
///
/// # Examples
///
/// ```
/// use std::mem::*;
/// use dungeon_cell::align::Aligned;
/// use dungeon_cell::layout::Alignment;
///
/// // 1 byte aligned buffer
/// let x: [u8; 4] = 1234567890u32.to_ne_bytes();
/// assert_eq!(size_of_val(&x), 4);
/// assert_eq!(align_of_val(&x), 1);
///
/// // 4 byte aligned buffer
/// let y = Aligned::<_, Alignment<4>>::new(x);
/// assert_eq!(size_of_val(&y), 4);
/// assert_eq!(align_of_val(&y), 4);
///
/// // this cast back to a u32 is sound because Aligned
/// // makes the value field aligned to 4 bytes
/// let z = &y.value as *const [u8; 4] as *const u32;
/// let z = unsafe { &*z };
/// assert_eq!(z, &1234567890);
/// ```
///
#[repr(C)]
pub struct Aligned<T, Alignment: IsAlignment> {
    /// Stored value.
    ///
    /// This field is guaranteed to be aligned to
    /// [`Alignment::VALUE`][crate::marker_traits::IsAlignment::VALUE] bytes.
    pub value: T,

    /// ZST to force the alignment of this struct.
    ///
    /// "The alignment of the struct is the alignment of the most-aligned field in it."
    /// <https://doc.rust-lang.org/reference/type-layout.html#reprc-structs>
    _alignment_marker: <Alignment as IsAlignment>::Marker,
}

impl<T: Copy, Alignment: IsAlignment> Copy for Aligned<T, Alignment> {}

impl<T: Clone, Alignment: IsAlignment> Clone for Aligned<T, Alignment> {
    fn clone(&self) -> Self {
        Self {
            value: self.value.clone(),
            _alignment_marker: <Alignment as IsAlignment>::MARKER_INSTANCE,
        }
    }
}

impl<T, Alignment: IsAlignment> Aligned<T, Alignment> {
    /// Construct a new aligned value.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::align::Aligned;
    /// use dungeon_cell::layout::Alignment;
    ///
    /// let a = Aligned::<_, Alignment<16>>::new(4);
    ///
    /// assert_eq!(a.value, 4);
    /// ```
    pub const fn new(value: T) -> Self {
        Self {
            value,
            _alignment_marker: <Alignment as IsAlignment>::MARKER_INSTANCE,
        }
    }

    /// Convert to the inner `T`.
    ///
    /// The returned value does not keep it's alignment and has the alignment of type `T`.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::align::Aligned;
    /// use dungeon_cell::layout::Alignment;
    ///
    /// let aligned = Aligned::<_, Alignment<16>>::new(4);
    /// let num = Aligned::into_inner(aligned);
    ///
    /// assert_eq!(num, 4);
    /// ```
    pub const fn into_inner(self) -> T {
        // SAFETY: This stuct has `repr(C)` with the type `T` first and a ZST after it.
        unsafe { const_transmute::<Self, T>(self) }
    }
}

#[cfg(test)]
mod test {
    use crate::layout::Alignment;
    use core::mem::{align_of, align_of_val, size_of, size_of_val};

    use super::*;

    #[test]
    fn aligned_is_just_a_wrapper_around_a_value() {
        let x = Aligned::<_, Alignment<1>>::new(123i32);

        assert_eq!(size_of_val(&x), size_of::<i32>());
        assert_eq!(align_of_val(&x), align_of::<i32>());

        assert_eq!(Aligned::into_inner(x), 123);

        let x = Aligned::<_, Alignment<1>>::new(String::from("abc"));

        assert_eq!(size_of_val(&x), size_of::<String>());
        assert_eq!(align_of_val(&x), align_of::<String>());

        assert_eq!(Aligned::into_inner(x), "abc");
    }

    #[test]
    fn aligned_forces_minimum_alignment() {
        // u32 already has an alignment of 4.
        assert_eq!(align_of::<Aligned::<u32, Alignment<1>>>(), 4);
        assert_eq!(align_of::<Aligned::<u32, Alignment<2>>>(), 4);
        assert_eq!(align_of::<Aligned::<u32, Alignment<4>>>(), 4);

        assert_eq!(align_of::<Aligned::<u32, Alignment<8>>>(), 8);
        assert_eq!(align_of::<Aligned::<u32, Alignment<64>>>(), 64);
        assert_eq!(
            align_of::<Aligned::<u32, Alignment<536_870_912>>>(),
            536_870_912
        );

        // no nudge
        assert_eq!(align_of::<Aligned::<u8, Alignment<1>>>(), 1);

        // little nudge
        assert_eq!(align_of::<Aligned::<u8, Alignment<2>>>(), 2);

        // BIG nugde
        assert_eq!(
            align_of::<Aligned::<u8, Alignment<536_870_912>>>(),
            536_870_912
        );
    }
}
