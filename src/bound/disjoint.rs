use crate::marker_traits::IsBound;

use super::{traits, Bound};

/// Check if dynamic trait bound is disjoint from another dynamic trait bound.
///
/// A bound is disjoint when it contains none of the same traits.
/// For example, `T: X + Y` is disjoint from `T: Z`.
///
/// The [`Empty`][super::bounds::Empty] bound is considered disjoint from all bounds.
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented outside of the implementations here.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::{Disjoint, bounds};
/// use dungeon_cell::marker_traits::IsBound;
///
/// fn test<A: Disjoint<B> + IsBound, B: IsBound>() {}
///
/// test::<bounds::Empty, bounds::Empty>();
/// test::<bounds::Send, bounds::Sync>();
/// test::<bounds::Copy, bounds::Normal>();
/// ```
/// ```compile_fail
/// use dungeon_cell::bound::{Disjoint, bounds};
/// use dungeon_cell::marker_traits::IsBound;
///
/// fn test<A: Disjoint<B> + IsBound, B: IsBound>() {}
///
/// test::<bounds::CloneCopy, bounds::CopyClone>();
/// test::<bounds::Normal, bounds::Normal>();
/// ```
pub trait Disjoint<T: IsBound>: IsBound {}

impl<
        SendB,
        SyncB,
        CopyB,
        CloneB,
        UnpinB,
        DebugB,
        SendA,
        SyncA,
        CopyA,
        CloneA,
        UnpinA,
        DebugA,
    > Disjoint<Bound<SendB, SyncB, CopyB, CloneB, UnpinB, DebugB>>
    for Bound<SendA, SyncA, CopyA, CloneA, UnpinA, DebugA>
where
    Bound<SendA, SyncA, CopyA, CloneA, UnpinA, DebugA>: IsBound,
    Bound<SendB, SyncB, CopyB, CloneB, UnpinB, DebugB>: IsBound,
    SendA: DisjointHelper<SendB>,
    SyncA: DisjointHelper<SyncB>,
    CopyA: DisjointHelper<CopyB>,
    CloneA: DisjointHelper<CloneB>,
    UnpinA: DisjointHelper<UnpinB>,
    DebugA: DisjointHelper<DebugB>,
{
}

trait DisjointHelper<T> {}

impl DisjointHelper<traits::Send> for traits::__ {}
impl DisjointHelper<traits::Sync> for traits::__ {}
impl DisjointHelper<traits::Copy> for traits::__ {}
impl DisjointHelper<traits::Clone> for traits::__ {}
impl DisjointHelper<traits::Unpin> for traits::__ {}
impl DisjointHelper<traits::Debug> for traits::__ {}
impl DisjointHelper<traits::__> for traits::__ {}

impl DisjointHelper<traits::__> for traits::Send {}
impl DisjointHelper<traits::__> for traits::Sync {}
impl DisjointHelper<traits::__> for traits::Copy {}
impl DisjointHelper<traits::__> for traits::Clone {}
impl DisjointHelper<traits::__> for traits::Unpin {}
impl DisjointHelper<traits::__> for traits::Debug {}
