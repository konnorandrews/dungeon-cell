/// Type operator to check if a type is a given type.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::type_eq::TypeEq;
///
/// fn example<T: TypeEq<Other = i32>>(_: T) {}
///
/// example(123);
/// ```
///
/// ```compile_fail
/// use dungeon_cell::bound::type_eq::TypeEq;
///
/// fn example<T: TypeEq<Other = i32>>(_: T) {}
///
/// example("test");
///
/// // error[E0271]: type mismatch resolving `<&str as TypeEq>::Other == i32`
/// //  --> src/bound/type_eq.rs:18:9
/// //   |
/// // 9 | example("test");
/// //   | ------- ^^^^^^ expected `i32`, found `&str`
/// //   | |
/// //   | required by a bound introduced by this call
/// //   |
/// ```
pub trait TypeEq: sealed::Sealed {
    /// The other type to check against.
    type Other: ?Sized;
}

// Impl TypeEq for all types and make Other be Self.
impl<T: ?Sized> TypeEq for T {
    type Other = Self;
}

mod sealed {
    pub trait Sealed {}

    impl<T: ?Sized> Sealed for T {}
}
