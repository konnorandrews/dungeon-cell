use super::boolean::True;

pub mod clone;
pub mod copy;
pub mod debug;
pub mod future;
pub mod send;
pub mod sync;
pub mod unpin;

pub type BSend<Required = True> = send::BSend<Required>;
pub type BSync<Required = True> = sync::BSync<Required>;
pub type BClone<Required = True> = clone::BClone<Required>;
pub type BCopy<Required = True> = copy::BCopy<Required>;
pub type BUnpin<Required = True> = unpin::BUnpin<Required>;
pub type BDebug<Required = True> = debug::BDebug<Required>;
pub type BFuture<Output, Required = True> = future::BFuture<Required, Output>;
