/// `true` value for [`Bool`].
pub enum True {}

/// `false` value for [`Bool`].
pub enum False {}

/// Type-level [`bool`] value.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
///
/// assert!(<True as Bool>::VALUE);
/// assert!(!<False as Bool>::VALUE);
/// ```
pub trait Bool: sealed::Sealed {
    /// Value of the type-level boolean.
    const VALUE: bool;
}

impl Bool for True {
    const VALUE: bool = true;
}

impl Bool for False {
    const VALUE: bool = false;
}

mod sealed {
    pub trait Sealed {}

    impl Sealed for super::True {}
    impl Sealed for super::False {}
}
