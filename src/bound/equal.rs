use crate::marker_traits::IsBound;

use super::{traits, Bound};

/// Check if dynamic trait bound is the same as another dynamic trait bound.
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented outside of the implementations here.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::{Equal, bounds};
/// use dungeon_cell::marker_traits::IsBound;
///
/// fn test<A: Equal<B> + IsBound, B: IsBound>() {}
///
/// test::<bounds::Empty, bounds::Empty>();
/// test::<bounds::Send, bounds::Send>();
/// test::<bounds::Normal, bounds::Normal>();
/// ```
/// ```compile_fail
/// use dungeon_cell::bound::{Equal, bounds};
/// use dungeon_cell::marker_traits::IsBound;
///
/// fn test<A: Equal<B> + IsBound, B: IsBound>() {}
///
/// test::<bounds::Copy, bounds::Empty>();
/// ```
pub trait Equal<T: IsBound>: IsBound {}

impl<
        SendB,
        SyncB,
        CopyB,
        CloneB,
        UnpinB,
        DebugB,
        SendA,
        SyncA,
        CopyA,
        CloneA,
        UnpinA,
        DebugA,
    > Equal<Bound<SendB, SyncB, CopyB, CloneB, UnpinB, DebugB>>
    for Bound<SendA, SyncA, CopyA, CloneA, UnpinA, DebugA>
where
    Bound<SendB, SyncB, CopyB, CloneB, UnpinB, DebugB>: IsBound,
    Bound<SendA, SyncA, CopyA, CloneA, UnpinA, DebugA>: IsBound,
    SendA: EqualHelper<SendB>,
    SyncA: EqualHelper<SyncB>,
    CopyA: EqualHelper<CopyB>,
    CloneA: EqualHelper<CloneB>,
    UnpinA: EqualHelper<UnpinB>,
    DebugA: EqualHelper<DebugB>,
{
}

pub trait EqualHelper<T> {}

impl EqualHelper<traits::__> for traits::__ {}
impl EqualHelper<traits::Send> for traits::Send {}
impl EqualHelper<traits::Sync> for traits::Sync {}
impl EqualHelper<traits::Copy> for traits::Copy {}
impl EqualHelper<traits::Clone> for traits::Clone {}
impl EqualHelper<traits::Unpin> for traits::Unpin {}
impl EqualHelper<traits::Debug> for traits::Debug {}
