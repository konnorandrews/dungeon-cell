use crate::marker_traits::IsBound;

use super::{traits, Bound};

/// Check if dynamic trait bound is a subset of another dynamic trait bound.
///
/// A dynamic trait bound is a subset of another if it requres less of the bounded type.
///
/// For example, `T: Send` is a subset of `T: Send + Sync`.
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented outside of the implementations here.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::{Subset, bounds};
/// use dungeon_cell::marker_traits::IsBound;
///
/// fn test<A: Subset<B> + IsBound, B: IsBound>() {}
///
/// test::<bounds::Empty, bounds::Copy>();
/// test::<bounds::Send, bounds::Normal>();
/// test::<bounds::Normal, bounds::Normal>();
/// ```
/// ```compile_fail
/// use dungeon_cell::bound::{Subset, bounds};
/// use dungeon_cell::marker_traits::IsBound;
///
/// fn test<A: Subset<B> + IsBound, B: IsBound>() {}
///
/// // only the Empty bound is a subset of the Empty bound
/// test::<bounds::Copy, bounds::Empty>();
/// ```
pub trait Subset<T: IsBound>: IsBound {}

impl<
        SendLarger,
        SyncLarger,
        CopyLarger,
        CloneLarger,
        UnpinLarger,
        DebugLarger,
        SendSmaller,
        SyncSmaller,
        CopySmaller,
        CloneSmaller,
        UnpinSmaller,
        DebugSmaller,
    >
    Subset<
        Bound<
            SendLarger,
            SyncLarger,
            CopyLarger,
            CloneLarger,
            UnpinLarger,
            DebugLarger,
        >,
    >
    for Bound<
        SendSmaller,
        SyncSmaller,
        CopySmaller,
        CloneSmaller,
        UnpinSmaller,
        DebugSmaller,
    >
where
    Bound<
        SendLarger,
        SyncLarger,
        CopyLarger,
        CloneLarger,
        UnpinLarger,
        DebugLarger,
    >: IsBound,
    Bound<
        SendSmaller,
        SyncSmaller,
        CopySmaller,
        CloneSmaller,
        UnpinSmaller,
        DebugSmaller,
    >: IsBound,
    SendSmaller: SubsetHelper<SendLarger>,
    SyncSmaller: SubsetHelper<SyncLarger>,
    CopySmaller: SubsetHelper<CopyLarger>,
    CloneSmaller: SubsetHelper<CloneLarger>,
    UnpinSmaller: SubsetHelper<UnpinLarger>,
    DebugSmaller: SubsetHelper<DebugLarger>,
{
}

pub trait SubsetHelper<T> {}

impl SubsetHelper<traits::Send> for traits::__ {}
impl SubsetHelper<traits::Sync> for traits::__ {}
impl SubsetHelper<traits::Copy> for traits::__ {}
impl SubsetHelper<traits::Clone> for traits::__ {}
impl SubsetHelper<traits::Unpin> for traits::__ {}
impl SubsetHelper<traits::Debug> for traits::__ {}
impl SubsetHelper<traits::__> for traits::__ {}

impl SubsetHelper<traits::Send> for traits::Send {}
impl SubsetHelper<traits::Sync> for traits::Sync {}
impl SubsetHelper<traits::Copy> for traits::Copy {}
impl SubsetHelper<traits::Clone> for traits::Clone {}
impl SubsetHelper<traits::Unpin> for traits::Unpin {}
impl SubsetHelper<traits::Debug> for traits::Debug {}
