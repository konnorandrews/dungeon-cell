use core::marker::PhantomData;

use crate::bound::boolean::{Bool, False, True};

use super::super::*;

/// Bound on the [`Clone`] trait.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
/// use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// use dungeon_cell::bound::traits::send::{BClone, CloneVTable};
///
/// // u8 implements Clone
/// assert!(!<VTable<u8, BClone<False>> as IsRequired>::Required::VALUE);
/// assert!(<VTable<u8, BClone<True>> as IsRequired>::Required::VALUE);
///
/// // *const u8 doesn't implement Clone
/// assert!(!<VTable<*const u8, BClone<False>> as IsRequired>::Required::VALUE);
/// ```
/// ```compile_fail
/// # use dungeon_cell::bound::boolean::{Bool, True, False};
/// # use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// # use dungeon_cell::bound::traits::send::{BClone, CloneVTable};
/// #
/// assert!(<VTable<*const u8, BClone<True>> as IsRequired>::Required::VALUE);
/// ```
pub struct BClone<Required: Bool>(PhantomData<Required>);

impl<This: ?Sized, Required: Bool> Dynamic<BClone<Required>> for This
where
    sealed::CloneVTableImpl<This, Required>: CloneVTable<This = This>,
{
    type VTable = sealed::CloneVTableImpl<This, Required>;
}

/// Trait for the [`Clone`] trait vtable.
///
/// See the [`BClone`] type for examples.
pub trait CloneVTable: WithSelf + IsRequired + sealed::Sealed {
    /// Clone the type using it's [`Clone`] impl.
    ///
    /// # Safety
    /// This function can only be called if `Self::Flag` is `Yes` or `Self::FLAG` is
    /// `true`.
    unsafe fn clone_unchecked(this: &Self::This) -> Self::This
    where
        Self::This: Sized;

    /// Clone the type using it's [`Clone`] impl.
    ///
    /// To call this function `Self::Flag` must be bounded to `Yes`.
    fn clone(this: &Self::This) -> Self::This
    where
        Self::This: Sized,
        Self::Required: TypeEq<Other = True>,
    {
        // SAFETY: Self::Flag must be Yes because of the bound above.
        unsafe { Self::clone_unchecked(this) }
    }
}

mod sealed {
    use super::*;

    pub trait Sealed {}

    /// Marker type that implements the [`Clone`] vtable for some `This`.
    pub struct CloneVTableImpl<This: ?Sized, Required>(
        PhantomData<(fn() -> *const This, Required)>,
    );

    impl<This: ?Sized, Required> WithSelf for CloneVTableImpl<This, Required> {
        type This = This;
    }

    impl<This: ?Sized, Required: Bool> IsRequired
        for CloneVTableImpl<This, Required>
    {
        type Required = Required;
    }

    // === Required: True ===

    impl<This: Clone> CloneVTable for CloneVTableImpl<This, True> {
        unsafe fn clone_unchecked(this: &Self::This) -> Self::This {
            This::clone(this)
        }
    }
    impl<This: Clone> sealed::Sealed for CloneVTableImpl<This, True> {}

    // === Required: False ===

    impl<This: ?Sized> CloneVTable for CloneVTableImpl<This, False> {
        unsafe fn clone_unchecked(_this: &Self::This) -> Self::This
        where
            Self::This: Sized,
        {
            unsafe { core::hint::unreachable_unchecked() };
        }
    }
    impl<This: ?Sized> sealed::Sealed for CloneVTableImpl<This, False> {}
}
