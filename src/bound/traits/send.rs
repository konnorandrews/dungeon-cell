use core::marker::PhantomData;

use crate::bound::boolean::{Bool, False, True};

use super::super::*;

/// Bound on the [`Send`] trait.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
/// use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// use dungeon_cell::bound::traits::send::{BSend, SendVTable};
///
/// // u8 implements Send
/// assert!(!<VTable<u8, BSend<False>> as IsRequired>::Required::VALUE);
/// assert!(<VTable<u8, BSend<True>> as IsRequired>::Required::VALUE);
///
/// // *const u8 doesn't implement Send
/// assert!(!<VTable<*const u8, BSend<False>> as IsRequired>::Required::VALUE);
/// ```
/// ```compile_fail
/// # use dungeon_cell::bound::boolean::{Bool, True, False};
/// # use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// # use dungeon_cell::bound::traits::send::{BSend, SendVTable};
/// #
/// assert!(<VTable<*const u8, BSend<True>> as IsRequired>::Required::VALUE);
/// ```
pub struct BSend<Required: Bool>(PhantomData<Required>);

impl<This: ?Sized, Required: Bool> Dynamic<BSend<Required>> for This
where
    sealed::SendVTableImpl<This, Required>: SendVTable<This = This>,
{
    type VTable = sealed::SendVTableImpl<This, Required>;
}

/// Trait for the [`Send`] trait vtable.
///
/// See the [`BSend`] type for examples.
pub trait SendVTable: WithSelf + sealed::Sealed {}

mod sealed {
    use super::*;

    pub trait Sealed {}

    /// Marker type that implements the [`Send`] vtable for some `This`.
    pub struct SendVTableImpl<This: ?Sized, Required>(
        PhantomData<(fn() -> *const This, Required)>,
    );

    impl<This: ?Sized, Required> WithSelf for SendVTableImpl<This, Required> {
        type This = This;
    }

    impl<This: ?Sized, Required: Bool> IsRequired
        for SendVTableImpl<This, Required>
    {
        type Required = Required;
    }

    // === Required: True ===

    impl<This: ?Sized + Send> SendVTable for SendVTableImpl<This, True> {}
    impl<This: ?Sized + Send> sealed::Sealed for SendVTableImpl<This, True> {}

    // === Required: False ===

    impl<This: ?Sized> SendVTable for SendVTableImpl<This, False> {}
    impl<This: ?Sized> sealed::Sealed for SendVTableImpl<This, False> {}
}
