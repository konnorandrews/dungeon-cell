use core::marker::PhantomData;
use core::pin::Pin;
use core::task::{Context, Poll};

use crate::bound::boolean::{Bool, False, True};

use super::super::*;

/// Bound on the [`Future`] trait.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
/// use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// use dungeon_cell::bound::traits::send::{BFuture, FutureVTable};
///
/// // u8 implements Future
/// assert!(!<VTable<u8, BFuture<False>> as IsRequired>::Required::VALUE);
/// assert!(<VTable<u8, BFuture<True>> as IsRequired>::Required::VALUE);
///
/// // *const u8 doesn't implement Future
/// assert!(!<VTable<*const u8, BFuture<False>> as IsRequired>::Required::VALUE);
/// ```
/// ```compile_fail
/// # use dungeon_cell::bound::boolean::{Bool, True, False};
/// # use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// # use dungeon_cell::bound::traits::send::{BFuture, FutureVTable};
/// #
/// assert!(<VTable<*const u8, BFuture<True>> as IsRequired>::Required::VALUE);
/// ```
pub struct BFuture<Required: Bool, Output>(
    PhantomData<(Required, fn() -> Output)>,
);

impl<This: ?Sized, Required: Bool, Output> Dynamic<BFuture<Required, Output>>
    for This
where
    sealed::FutureVTableImpl<This, Required, Output>:
        FutureVTable<This = This, Required = Required, Output = Output>,
{
    type VTable = sealed::FutureVTableImpl<This, Required, Output>;
}

/// Trait for the [`Future`] trait vtable.
///
/// See the [`BFuture`] type for examples.
pub trait FutureVTable: WithSelf + IsRequired + sealed::Sealed {
    type Output;

    unsafe fn poll_unchecked(
        this: Pin<&mut Self::This>,
        cx: &mut Context<'_>,
    ) -> Poll<Self::Output>;

    fn poll(
        this: Pin<&mut Self::This>,
        cx: &mut Context<'_>,
    ) -> Poll<Self::Output>
    where
        Self::Required: TypeEq<Other = True>,
    {
        unsafe { Self::poll_unchecked(this, cx) }
    }
}

mod sealed {
    use super::*;
    use core::future::Future;

    pub trait Sealed {}

    /// Marker type that implements the [`Future`] vtable for some `This`.
    pub struct FutureVTableImpl<This: ?Sized, Required, Output>(
        PhantomData<(fn() -> *const This, Required, fn() -> Output)>,
    );

    impl<This: ?Sized, Required, Output> WithSelf
        for FutureVTableImpl<This, Required, Output>
    {
        type This = This;
    }

    impl<This: ?Sized, Required: Bool, Output> IsRequired
        for FutureVTableImpl<This, Required, Output>
    {
        type Required = Required;
    }

    // === Required: True ===

    impl<This: Future> FutureVTable
        for FutureVTableImpl<This, True, <This as Future>::Output>
    {
        type Output = <This as Future>::Output;

        unsafe fn poll_unchecked(
            this: Pin<&mut Self::This>,
            cx: &mut Context<'_>,
        ) -> Poll<Self::Output> {
            This::poll(this, cx)
        }
    }
    impl<This: Future> sealed::Sealed
        for FutureVTableImpl<This, True, <This as Future>::Output>
    {
    }

    // === Required: False ===

    impl<This: ?Sized, Output> FutureVTable
        for FutureVTableImpl<This, False, Output>
    {
        type Output = Output;

        unsafe fn poll_unchecked(
            _this: Pin<&mut Self::This>,
            _cx: &mut Context<'_>,
        ) -> Poll<Self::Output> {
            unsafe { core::hint::unreachable_unchecked() };
        }
    }
    impl<This: ?Sized, Output> sealed::Sealed
        for FutureVTableImpl<This, False, Output>
    {
    }
}
