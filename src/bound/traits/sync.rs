use core::marker::PhantomData;

use crate::bound::boolean::{Bool, False, True};

use super::super::*;

/// Bound on the [`Sync`] trait.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
/// use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// use dungeon_cell::bound::traits::sync::{BSync, SyncVTable};
///
/// // u8 implements Sync
/// assert!(!<VTable<u8, BSync<False>> as IsRequired>::Required::VALUE);
/// assert!(<VTable<u8, BSync<True>> as IsRequired>::Required::VALUE);
///
/// // *const u8 doesn't implement Sync
/// assert!(!<VTable<*const u8, BSync<False>> as IsRequired>::Required::VALUE);
/// ```
/// ```compile_fail
/// # use dungeon_cell::bound::boolean::{Bool, True, False};
/// # use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// # use dungeon_cell::bound::traits::sync::{BSync, SyncVTable};
/// #
/// assert!(<VTable<*const u8, BSync<True>> as IsRequired>::Required::VALUE);
/// ```
pub struct BSync<Required: Bool>(PhantomData<Required>);

impl<This: ?Sized, Required: Bool> Dynamic<BSync<Required>> for This
where
    sealed::SyncVTableImpl<This, Required>: SyncVTable<This = This>,
{
    type VTable = sealed::SyncVTableImpl<This, Required>;
}

/// Trait for the [`Sync`] trait vtable.
///
/// See the [`BSync`] type for examples.
pub trait SyncVTable: WithSelf + sealed::Sealed {}

mod sealed {
    use super::*;

    pub trait Sealed {}

    /// Marker type that implements the [`Sync`] vtable for some `This`.
    pub struct SyncVTableImpl<This: ?Sized, Required>(
        PhantomData<(fn() -> *const This, Required)>,
    );

    impl<This: ?Sized, Required> WithSelf for SyncVTableImpl<This, Required> {
        type This = This;
    }

    impl<This: ?Sized, Required: Bool> IsRequired
        for SyncVTableImpl<This, Required>
    {
        type Required = Required;
    }

    // === Required: True ===

    impl<This: ?Sized + Sync> SyncVTable for SyncVTableImpl<This, True> {}
    impl<This: ?Sized + Sync> sealed::Sealed for SyncVTableImpl<This, True> {}

    // === Required: False ===

    impl<This: ?Sized> SyncVTable for SyncVTableImpl<This, False> {}
    impl<This: ?Sized> sealed::Sealed for SyncVTableImpl<This, False> {}
}
