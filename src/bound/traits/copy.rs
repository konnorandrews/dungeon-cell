use core::marker::PhantomData;

use crate::bound::boolean::{Bool, False, True};

use super::super::*;

/// Bound on the [`Copy`] trait.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
/// use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// use dungeon_cell::bound::traits::send::{BCopy, CopyVTable};
///
/// // u8 implements Copy
/// assert!(!<VTable<u8, BCopy<False>> as IsRequired>::Required::VALUE);
/// assert!(<VTable<u8, BCopy<True>> as IsRequired>::Required::VALUE);
///
/// // *const u8 doesn't implement Copy
/// assert!(!<VTable<*const u8, BCopy<False>> as IsRequired>::Required::VALUE);
/// ```
/// ```compile_fail
/// # use dungeon_cell::bound::boolean::{Bool, True, False};
/// # use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// # use dungeon_cell::bound::traits::send::{BCopy, CopyVTable};
/// #
/// assert!(<VTable<*const u8, BCopy<True>> as IsRequired>::Required::VALUE);
/// ```
pub struct BCopy<Required: Bool>(PhantomData<Required>);

impl<This: ?Sized, Required: Bool> Dynamic<BCopy<Required>> for This
where
    sealed::CopyVTableImpl<This, Required>: CopyVTable<This = This>,
{
    type VTable = sealed::CopyVTableImpl<This, Required>;
}

/// Trait for the [`Copy`] trait vtable.
///
/// See the [`BCopy`] type for examples.
pub trait CopyVTable: WithSelf + IsRequired + sealed::Sealed {
    /// Copy the type using it's [`Copy`] impl.
    ///
    /// # Safety
    /// This function can only be called if `Self::Flag` is `Yes` or `Self::FLAG` is
    /// `true`.
    unsafe fn copy_unchecked(this: &Self::This) -> Self::This
    where
        Self::This: Sized;

    /// Copy the type using it's [`Copy`] impl.
    ///
    /// To call this function `Self::Flag` must be bounded to `Yes`.
    fn copy(this: &Self::This) -> Self::This
    where
        Self::This: Sized,
        Self::Required: TypeEq<Other = True>,
    {
        // SAFETY: Self::Flag must be Yes because of the bound above.
        unsafe { Self::copy_unchecked(this) }
    }
}

mod sealed {
    use super::*;

    pub trait Sealed {}

    /// Marker type that implements the [`Copy`] vtable for some `This`.
    pub struct CopyVTableImpl<This: ?Sized, Required>(
        PhantomData<(fn() -> *const This, Required)>,
    );

    impl<This: ?Sized, Required> WithSelf for CopyVTableImpl<This, Required> {
        type This = This;
    }

    impl<This: ?Sized, Required: Bool> IsRequired
        for CopyVTableImpl<This, Required>
    {
        type Required = Required;
    }

    // === Required: True ===

    impl<This: Copy> CopyVTable for CopyVTableImpl<This, True> {
        unsafe fn copy_unchecked(this: &Self::This) -> Self::This {
            *this
        }
    }
    impl<This: Copy> sealed::Sealed for CopyVTableImpl<This, True> {}

    // === Required: False ===

    impl<This: ?Sized> CopyVTable for CopyVTableImpl<This, False> {
        unsafe fn copy_unchecked(_this: &Self::This) -> Self::This
        where
            Self::This: Sized,
        {
            unsafe { core::hint::unreachable_unchecked() };
        }
    }
    impl<This: ?Sized> sealed::Sealed for CopyVTableImpl<This, False> {}
}
