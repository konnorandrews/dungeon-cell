use core::fmt::{Error, Formatter};
use core::marker::PhantomData;

use crate::bound::boolean::{Bool, False, True};

use super::super::*;

/// Bound on the [`Debug`] trait.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
/// use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// use dungeon_cell::bound::traits::send::{BDebug, DebugVTable};
///
/// // u8 implements Debug
/// assert!(!<VTable<u8, BDebug<False>> as IsRequired>::Required::VALUE);
/// assert!(<VTable<u8, BDebug<True>> as IsRequired>::Required::VALUE);
///
/// // *const u8 doesn't implement Debug
/// assert!(!<VTable<*const u8, BDebug<False>> as IsRequired>::Required::VALUE);
/// ```
/// ```compile_fail
/// # use dungeon_cell::bound::boolean::{Bool, True, False};
/// # use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// # use dungeon_cell::bound::traits::send::{BDebug, DebugVTable};
/// #
/// assert!(<VTable<*const u8, BDebug<True>> as IsRequired>::Required::VALUE);
/// ```
pub struct BDebug<Required: Bool>(PhantomData<Required>);

impl<This: ?Sized, Required: Bool> Dynamic<BDebug<Required>> for This
where
    sealed::DebugVTableImpl<This, Required>: DebugVTable<This = This>,
{
    type VTable = sealed::DebugVTableImpl<This, Required>;
}

/// Trait for the [`Debug`] trait vtable.
///
/// See the [`BDebug`] type for examples.
pub trait DebugVTable: WithSelf + IsRequired + sealed::Sealed {
    unsafe fn fmt_unchecked(
        this: &Self::This,
        f: &mut Formatter<'_>,
    ) -> Result<(), Error>;

    fn fmt(this: &Self::This, f: &mut Formatter<'_>) -> Result<(), Error>
    where
        Self::Required: TypeEq<Other = True>,
    {
        unsafe { Self::fmt_unchecked(this, f) }
    }
}

mod sealed {
    use super::*;
    use core::fmt::Debug;

    pub trait Sealed {}

    /// Marker type that implements the [`Debug`] vtable for some `This`.
    pub struct DebugVTableImpl<This: ?Sized, Required>(
        PhantomData<(fn() -> *const This, Required)>,
    );

    impl<This: ?Sized, Required> WithSelf for DebugVTableImpl<This, Required> {
        type This = This;
    }

    impl<This: ?Sized, Required: Bool> IsRequired
        for DebugVTableImpl<This, Required>
    {
        type Required = Required;
    }

    // === Required: True ===

    impl<This: Debug> DebugVTable for DebugVTableImpl<This, True> {
        unsafe fn fmt_unchecked(
            this: &Self::This,
            f: &mut Formatter<'_>,
        ) -> Result<(), Error> {
            This::fmt(this, f)
        }
    }
    impl<This: Debug> sealed::Sealed for DebugVTableImpl<This, True> {}

    // === Required: False ===

    impl<This: ?Sized> DebugVTable for DebugVTableImpl<This, False> {
        unsafe fn fmt_unchecked(
            _this: &Self::This,
            _f: &mut Formatter<'_>,
        ) -> Result<(), Error> {
            unsafe { core::hint::unreachable_unchecked() }
        }
    }
    impl<This: ?Sized> sealed::Sealed for DebugVTableImpl<This, False> {}
}
