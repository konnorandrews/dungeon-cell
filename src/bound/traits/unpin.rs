use core::marker::PhantomData;

use crate::bound::boolean::{Bool, False, True};

use super::super::*;

/// Bound on the [`Unpin`] trait.
///
/// # Examples
/// ```
/// use dungeon_cell::bound::boolean::{Bool, True, False};
/// use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// use dungeon_cell::bound::traits::sync::{BUnpin, UnpinVTable};
///
/// // u8 implements Unpin
/// assert!(!<VTable<u8, BUnpin<False>> as IsRequired>::Required::VALUE);
/// assert!(<VTable<u8, BUnpin<True>> as IsRequired>::Required::VALUE);
///
/// // *const u8 doesn't implement Unpin
/// assert!(!<VTable<*const u8, BUnpin<False>> as IsRequired>::Required::VALUE);
/// ```
/// ```compile_fail
/// # use dungeon_cell::bound::boolean::{Bool, True, False};
/// # use dungeon_cell::bound::{Dynamic, VTable, IsRequired};
/// # use dungeon_cell::bound::traits::sync::{BUnpin, UnpinVTable};
/// #
/// assert!(<VTable<*const u8, BUnpin<True>> as IsRequired>::Required::VALUE);
/// ```
pub struct BUnpin<Required: Bool>(PhantomData<Required>);

impl<This: ?Sized, Required: Bool> Dynamic<BUnpin<Required>> for This
where
    sealed::UnpinVTableImpl<This, Required>: UnpinVTable<This = This>,
{
    type VTable = sealed::UnpinVTableImpl<This, Required>;
}

/// Trait for the [`Unpin`] trait vtable.
///
/// See the [`BUnpin`] type for examples.
pub trait UnpinVTable: WithSelf + sealed::Sealed {}

mod sealed {
    use super::*;

    pub trait Sealed {}

    /// Marker type that implements the [`Unpin`] vtable for some `This`.
    pub struct UnpinVTableImpl<This: ?Sized, Required>(
        PhantomData<(fn() -> *const This, Required)>,
    );

    impl<This: ?Sized, Required> WithSelf for UnpinVTableImpl<This, Required> {
        type This = This;
    }

    impl<This: ?Sized, Required: Bool> IsRequired
        for UnpinVTableImpl<This, Required>
    {
        type Required = Required;
    }

    // === Required: True ===

    impl<This: ?Sized + Unpin> UnpinVTable for UnpinVTableImpl<This, True> {}
    impl<This: ?Sized + Unpin> sealed::Sealed for UnpinVTableImpl<This, True> {}

    // === Required: False ===

    impl<This: ?Sized> UnpinVTable for UnpinVTableImpl<This, False> {}
    impl<This: ?Sized> sealed::Sealed for UnpinVTableImpl<This, False> {}
}
