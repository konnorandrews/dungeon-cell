//! Storage data structure for [`DungeonCore`][crate::DungeonCore].
//!
//! The traits in this module are not designed for general use. They are
//! included in the documentation for context.
//!
//! The following documentation is **not** a interface the module considers public.
//! It is unsound for any external crate to attempt to access the inner structure
//! of a [`Cleanup::Storage`] manually.
//!
//! Additionally, the [`CleanupHelper`] trait itself is not considered a public API
//! subject to semver. However, [`Cleanup`] is subject to semver because of its direct
//! use in the definition of [`DungeonCore`][crate::DungeonCore].
//!
//! # Constraints
//! - `DungeonCore` should be `Copy` if its vtable bound allows it.
//! - `DungeonCore` should automatically drop it's content when its dropped.
//! - `DungeonCore` should to be resiliant to storing `UnsafeCell`.
//! - `DungeonCore` should be const constructable.
//!
//! # Design
//! Because `Copy` and `Drop` are mutually exclusive this causes a big problem.
//! We can either implement `Copy` or `Drop` and we can't use where clauses to
//! selectivly disable them. To solve this issue we use a trait with an associated
//! type. This type will be either CopyStorage or DropStorage.
//!
//! However, this cases a new issue. Because the type is an associated type we cant
//! call methods or access fields on it from a const context. To get around this,
//! this module provides a set of const functions that transmute from the associated
//! type into the field types for either of the storage structs.
//!
//! Lastly we have the `UnsafeCell`. Because `UnsafeCell` is not `Copy`
//! (it could be in the future, but its already been denied once) we can't wrap the
//! `CopyStorage` in a `UnsafeCell`, but it also means no types with a `UnsafeCell`
//! inside them will be `Copy` so they can never use the `CopyStorage` anyways.
//! For `DropStorage` we must wrap the buffer storing the value in a `UnsafeCell`
//! to allow rustc to see that the buffer may contain a `UnsafeCell` containing type.

use core::cell::UnsafeCell;
use core::mem::{ManuallyDrop, MaybeUninit};
use core::ptr::{addr_of, addr_of_mut};

use crate::align::Aligned;
use crate::bound::{traits, Bound};
use crate::buffer::Buffer;
use crate::compile_time::const_transmute;
use crate::marker_traits::{IsBound, IsLayout, IsSize};
use crate::vtable::VTable;

/// Trait implemented on layouts that can be cleaned up by a given vtable.
///
/// The trait bound `L: Cleanup<V>` signifies that the vtable `V` can cleanup a value
/// with layout `L`.
///
/// This trait is implemented for all `V: VTable` in combination with
/// [`Layout`][crate::layout::Layout]. It is not possible to implement this trait
/// for custom types outside of this set.
///
/// The associated type [`Self::Storage`] will be cleaned up automatically when dropped.
/// This type is based on the vtable and the layout.
///
/// For dynamic bounds on the vtable that indicate the value is `Copy` then the associated type
/// will perform no cleanup operation. For all other cases the type will run the
/// drop impl from the vtable.
///
/// Additionally, the [`Self::Storage`] will contain a [`UnsafeCell`] around the internal
/// buffer if applicable. This is required to prevent some forms of UB when storing
/// interior mutable types.
pub trait Cleanup<V: VTable>: IsLayout {
    /// Storage for [`DungeonCore`][super::super::DungeonCore].
    type Storage;
}

impl<V: VTable, L: IsLayout> Cleanup<V> for L
where
    L: CleanupHelper<V, V::Bounds>,
{
    type Storage = <L as CleanupHelper<V, V::Bounds>>::Storage;
}

// `CleanupHelper` is included in the docs.
#[cfg(doc)]
pub use sealed::CleanupHelper;

// `CleanupHelper` is not part of the public API though.
#[cfg(not(doc))]
use sealed::CleanupHelper;

/// Prevent `DropStorage` and `CopyStorage` from being accessed directly outside the parent module.
mod sealed {
    use super::*;

    /// Helper trait for [`Cleanup`].
    ///
    /// This is required because rustc does not consider unique associated type bounds
    /// as being non-overlapping implementations. This helper gets around this by
    /// making the associated type `V::Bounds` visible in the trait generics.
    ///
    /// This trait is implemented for all possible combinations of valid [`Bound`],
    /// `V: VTable`, and [`Layout`][crate::layout::Layout]. Therefore, its not possible to implement this trait for custom
    /// types outside of this set.
    pub trait CleanupHelper<V: VTable, B: IsBound>: IsLayout {
        /// Type to set [`Cleanup::Storage`] to.
        type Storage;
    }

    /// Byte buffer and vtable.
    ///
    /// This form is used by [`CopyStorage`]. It will implement
    /// `Copy` when the vtable is `Copy`. The bounds on [`CleanupHelper`] makes
    /// sure the stored value is also `Copy` assuming the .
    ///
    /// The repr(C) is important for the transmutes later.
    #[repr(C)]
    struct BufferAndVtable<S: IsSize, V> {
        buffer: Buffer<S>,
        vtable: V,
    }

    impl<S: IsSize, V: Copy> Copy for BufferAndVtable<S, V> {}
    impl<S: IsSize, V: Clone> Clone for BufferAndVtable<S, V> {
        fn clone(&self) -> Self {
            Self {
                // The bounds on `CleanupHelper` and the code in `DungeonCore`
                // make sure that only `Copy` types are stored in this buffer.
                buffer: self.buffer,
                vtable: self.vtable.clone(),
            }
        }
    }

    /// Byte buffer wrapped in [`UnsafeCell`] and vtable.
    ///
    /// This form is used by [`DropStorage`]. This struct by itself doesn't implement
    /// [`Drop`].
    ///
    /// The repr(C) is important for the transmutes later.
    #[repr(C)]
    struct UnsafeCellBufferAndVtable<S: IsSize, V> {
        buffer: UnsafeCell<Buffer<S>>,
        vtable: V,
    }

    // /// Inner storage bounded by dynamic bound.
    // ///
    // /// This is the same as a [`Inner`] but with a stored dynamic trait bound.
    // /// This type doesn't force the data stored in `inner` to be actually bounded
    // /// by this dynamic trait bound.
    // ///
    // /// This type will implement/not implement [`Send`], [`Sync`], and [`Unpin`] if
    // /// the dynamic bound implies the value in `inner` does.
    // #[repr(transparent)]
    // struct BoundedInner<L: IsLayout, B: DynamicBound, V> {
    //     /// The aligned buffer and vtable.
    //     inner: Inner<L, V>,
    //
    //     /// Marker to cause the auto impl of `Send` to be enabled/disabled.
    //     _bound_marker: PhantomData<B::Marker>,
    // }
    //
    // impl<L: IsLayout, B: DynamicBound, V: Copy> Copy for BoundedInner<L, B, V> {}
    // impl<L: IsLayout, B: DynamicBound, V: Clone> Clone for BoundedInner<L, B, V> {
    //     fn clone(&self) -> Self {
    //         Self {
    //             inner: self.inner.clone(),
    //             _bound_marker: PhantomData,
    //         }
    //     }
    // }

    /// Storage that can implement [`Copy`].
    ///
    /// This storage does not run any cleanup code for the buffer on drop.
    ///
    /// All values stored in the buffer must be [`Copy`].
    /// This storage will implement [`Copy`] and [`Clone`] if the vtable does.
    #[repr(transparent)]
    pub struct CopyStorage<L: IsLayout, V> {
        /// `[[byte buffer][vtable] .. alignment padding ..]`
        aligned: Aligned<BufferAndVtable<L::Size, V>, L::Alignment>,
    }

    impl<L: IsLayout, V: Copy> Copy for CopyStorage<L, V> {}
    impl<L: IsLayout, V: Clone> Clone for CopyStorage<L, V> {
        fn clone(&self) -> Self {
            Self {
                aligned: self.aligned.clone(),
            }
        }
    }

    /// Storage that implements [`Drop`].
    ///
    /// This storage runs cleanup code for the buffer on drop.
    ///
    /// This storage will implement [`Clone`] if the vtable does and it has the `Clone`
    /// trait bound.
    #[repr(transparent)]
    pub struct DropStorage<L: IsLayout, V: VTable> {
        /// `[[byte buffer][vtable] .. alignment padding ..]`
        aligned: Aligned<UnsafeCellBufferAndVtable<L::Size, V>, L::Alignment>,
    }

    impl<L: IsLayout, V: VTable + Clone> Clone for DropStorage<L, V>
    where
        <V::Bounds as IsBound>::CloneMarker: Clone,
    {
        fn clone(&self) -> Self {
            // The clone operation will copy the new value into this buffer.
            // This buffer must be alligned as the clone will assume it is a pointer
            // to a `T`.
            let mut temp_buffer =
                Aligned::<_, L::Alignment>::new(Buffer::uninit());

            // Access the buffer and vtable at the same time.
            // This prevents any issues with borrows invalidating each other.
            let UnsafeCellBufferAndVtable { buffer, vtable } =
                &self.aligned.value;

            // Get pointer to the buffer. Its ok to access inside the UnsafeCell because
            // the other code in this module doesn't allow a mutable borrow or pointer
            // to live long enough to conflict.
            let buffer = Buffer::as_ptr(buffer.get());

            // SAFETY: We bounded on the CloneMarker implementing Clone.
            // And the buffer has a value for the vtable by the inveriants of the
            // accessors. The value in the buffer is also correctly aligned.
            // The temp_buffer is aligned properly by Aligned.
            unsafe {
                vtable.bound_impl().clone_value(
                    buffer,
                    Buffer::as_ptr_mut(&mut temp_buffer.value),
                )
            };

            // Construct new storage from the cloned buffer.
            Self {
                aligned: Aligned::new(UnsafeCellBufferAndVtable {
                    buffer: UnsafeCell::new(temp_buffer.value),
                    vtable: vtable.clone(),
                }),
            }
        }
    }

    impl<L: IsLayout, V: VTable> Drop for DropStorage<L, V> {
        fn drop(&mut self) {
            // Access the buffer and vtable at the same time.
            // This prevents any issues with borrows invalidating each other.
            let UnsafeCellBufferAndVtable { buffer, vtable } =
                &mut self.aligned.value;

            // SAFETY: Because of the invariants on the accessors we know the buffer
            // must contain a valid value for the vtable, and the value is aligned.
            unsafe {
                vtable
                    .bound_impl()
                    .drop_value(Buffer::as_ptr_mut(buffer.get_mut()))
            }
        }
    }

    /// Access the buffer and vtable of a storage.
    ///
    /// The resulting buffer pointer is safe to transmute into a type
    /// with interior mutability assuming the value in the buffer is of that type.
    ///
    /// # Safety
    /// - `storage` must point to a valid storage of the correct type.
    /// - The contents of the buffer must be droppable by the vtable when
    ///     the `storage` value is dropped. As such, these pointers should
    ///     be considered borrows and the inveriant must hold when the borrow
    ///     of `storage` ends.
    pub const unsafe fn storage_as_split_ptr<
        L: IsLayout + Cleanup<V>,
        V: VTable,
    >(
        storage: *const <L as Cleanup<V>>::Storage,
    ) -> (*const MaybeUninit<u8>, *const V) {
        // We must call the UnsafeCell api when it's being used to prevent possible
        // future UB. A UnsafeCell is not used when the vtable is bounded by Copy.
        // The optimizer should remove the unused branch in practice.
        if <V::Bounds as IsBound>::BOUND_BY_COPY {
            // The type of the storage must be a `CopyStorage` because of the impls
            // at the bottom of this module.
            let storage = storage.cast::<CopyStorage<L, V>>();

            // SAFETY: The storage is safe to deref for taking a pointer to it's field.
            // This is because of the safety requirements of this function.
            let buffer = Buffer::as_ptr(unsafe {
                addr_of!((*storage).aligned.value.buffer)
            });

            // SAFETY: The storage is safe to deref for taking a pointer to it's field.
            // This is because of the safety requirements of this function.
            let vtable = unsafe { addr_of!((*storage).aligned.value.vtable) };

            (buffer, vtable)
        } else {
            // The type of the storage must be a `DropStorage` because of the impls
            // at the bottom of this module.
            let storage = storage.cast::<DropStorage<L, V>>();

            // The pointer produced by raw_get will be safe to transmute into a
            // type with interior mutability.
            //
            // SAFETY: The storage is safe to deref for taking a pointer to it's field.
            // This is because of the safety requirements of this function.
            let buffer = Buffer::as_ptr(UnsafeCell::raw_get(unsafe {
                addr_of!((*storage).aligned.value.buffer)
            }));

            // SAFETY: The storage is safe to deref for taking a pointer to it's field.
            // This is because of the safety requirements of this function.
            let vtable = unsafe { addr_of!((*storage).aligned.value.vtable) };

            (buffer, vtable)
        }
    }

    /// Mutable access the buffer and vtable of a storage.
    ///
    /// The resulting buffer pointer is safe to transmute into a type
    /// with interior mutability assuming the value in the buffer is of that type.
    ///
    /// # Safety
    /// - `storage` must point to a valid storage of the correct type.
    /// - The contents of the buffer must be droppable by the vtable when
    ///     the `storage` value is dropped. As such, these pointers should
    ///     be considered borrows and the inveriant must hold when the borrow
    ///     of `storage` ends.
    pub unsafe fn storage_as_split_ptr_mut<
        L: IsLayout + Cleanup<V>,
        V: VTable,
    >(
        storage: *mut <L as Cleanup<V>>::Storage,
    ) -> (*mut MaybeUninit<u8>, *mut V) {
        // We must call the UnsafeCell api when it's being used to prevent possible
        // future UB. A UnsafeCell is not used when the vtable is bounded by Copy.
        // The optimizer should remove the unused branch in practice.
        if <V::Bounds as IsBound>::BOUND_BY_COPY {
            // The type of the storage must be a `CopyStorage` because of the impls
            // at the bottom of this module.
            let storage = storage.cast::<CopyStorage<L, V>>();

            // SAFETY: The storage is safe to deref for taking a pointer to it's field.
            // This is because of the safety requirements of this function.
            let buffer = Buffer::as_ptr_mut(unsafe {
                addr_of_mut!((*storage).aligned.value.buffer)
            });

            let vtable = {
                // SAFETY: The storage is safe to deref for taking a pointer to it's field.
                // This is because of the safety requirements of this function.
                unsafe { addr_of_mut!((*storage).aligned.value.vtable) }
            };

            (buffer, vtable)
        } else {
            // The type of the storage must be a `DropStorage` because of the impls
            // at the bottom of this module.
            let storage = storage.cast::<DropStorage<L, V>>();

            // The pointer produced by raw_get will be safe to transmute into a
            // type with interior mutability.
            //
            // SAFETY: The storage is safe to deref for taking a pointer to it's field.
            // This is because of the safety requirements of this function.
            let buffer = Buffer::as_ptr_mut(UnsafeCell::raw_get(unsafe {
                addr_of_mut!((*storage).aligned.value.buffer)
            }));

            let vtable = {
                // SAFETY: The storage is safe to deref for taking a pointer to it's field.
                // This is because of the safety requirements of this function.
                unsafe { addr_of_mut!((*storage).aligned.value.vtable) }
            };

            (buffer, vtable)
        }
    }

    /// Split the storage into the buffer and vtable.
    ///
    /// The returned buffer will not automatically have it's value cleaned
    /// up when its dropped.
    #[must_use]
    pub const fn split_storage<L: IsLayout + Cleanup<V>, V: VTable>(
        storage: <L as Cleanup<V>>::Storage,
    ) -> (ManuallyDrop<Buffer<L::Size>>, ManuallyDrop<V>) {
        /// Same representation as both the `DropStorage` and `CopyStorage`.
        /// We don't have to worry about the alignment as the inner struct is always
        /// placed with no offset. Additionally, we don't have to worry about the
        /// possible UnsafeCell because we have ownership so outstanding interior
        /// mutability doesn't matter.
        ///
        /// The ManuallyDrop wrapping the fields allows this function to be const.
        /// This is because it tells rustc we won't be running custom drop code in const.
        #[repr(C)]
        struct NoDrop<S: IsSize, V> {
            buffer: ManuallyDrop<Buffer<S>>,
            vtable: ManuallyDrop<V>,
            // alignment padding would go here in `DropStorage` and `CopyStorage`.
        }

        // Transmute storage into common type.
        let NoDrop { buffer, vtable }: NoDrop<L::Size, V> = {
            // SAFETY: Both `DropStorage` and `CopyStorage` have the same layout as
            // `NoDrop` minus their size. But const_transmute doesn't care about the
            // sizes as long as we are going to smaller, which we are.
            unsafe { const_transmute(storage) }
        };

        // Unwrap the fields and return as a tuple.
        (buffer, vtable)
    }

    /// Create storage from buffer and vtable.
    ///
    /// # Safety
    /// The `buffer` must be storing a value droppable by the `vtable`.
    pub const unsafe fn into_storage<L: IsLayout + Cleanup<V>, V: VTable>(
        buffer: Buffer<L::Size>,
        vtable: V,
    ) -> <L as Cleanup<V>>::Storage {
        // We must use the correct storage for the bounds.
        // The optimizer should remove the unused branch.
        if <V::Bounds as IsBound>::BOUND_BY_COPY {
            // SAFETY: We know by the impls bellow that when bounded by Copy the
            // storage type must be `CopyStorage`.
            unsafe {
                const_transmute::<CopyStorage<L, V>, _>(CopyStorage {
                    aligned: Aligned::new(BufferAndVtable { buffer, vtable }),
                })
            }
        } else {
            // SAFETY: We know by the impls bellow that when not bounded by Copy the
            // storage type must be `DropStorage`.
            unsafe {
                const_transmute::<DropStorage<L, V>, _>(DropStorage {
                    aligned: Aligned::new(UnsafeCellBufferAndVtable {
                        buffer: UnsafeCell::new(buffer),
                        vtable,
                    }),
                })
            }
        }
    }
}
pub(crate) use sealed::{
    into_storage, split_storage, storage_as_split_ptr, storage_as_split_ptr_mut,
};
use sealed::{CopyStorage, DropStorage};

// We know by the implementation of `IsBound` that the copy slot must be
// `traits::Copy` or `traits::__` it cannot be something else.

// Make the associated type for a dynamic bound with `Copy` the `CopyStorage` storage.
//
// The bound made here is very important to the safety of the above unsafe code.
impl<L: IsLayout, V, Send, Sync, Clone, Unpin, Debug>
    CleanupHelper<V, Bound<Send, Sync, traits::Copy, Clone, Unpin, Debug>> for L
where
    V: VTable<Bounds = Bound<Send, Sync, traits::Copy, Clone, Unpin, Debug>>,
    Bound<Send, Sync, traits::Copy, Clone, Unpin, Debug>: IsBound,
{
    /// The inner storage will not run `Drop` code and can be `Copy` if the vtable is.
    type Storage = CopyStorage<L, V>;
}

// Make the associated type for a dynamic bound without `Copy` the `DropStorage` storage.
//
// The bound made here is very important to the safety of the above unsafe code.
impl<L: IsLayout, V, Send, Sync, Clone, Unpin, Debug>
    CleanupHelper<V, Bound<Send, Sync, traits::__, Clone, Unpin, Debug>> for L
where
    V: VTable<Bounds = Bound<Send, Sync, traits::__, Clone, Unpin, Debug>>,
    Bound<Send, Sync, traits::__, Clone, Unpin, Debug>: IsBound,
{
    /// The inner stoage will run `Drop` code from the vtable.
    type Storage = DropStorage<L, V>;
}
