use core::fmt;

use crate::bound::{Dynamic, Subset};
use crate::marker_traits::IsBound;

use super::{
    BoundsImpl, ConstVTableOf, Descriptor, UniqueId, VTable, VTableOf,
    VTableSubset,
};

///
/// # Examples
///
/// ```
/// use dungeon_cell::vtable::{BasicVTable, VTableOf, VTable, Lifetime};
/// use dungeon_cell::bound::bounds;
///
/// // get for i32
/// let vtable = BasicVTable::<bounds::Normal, Lifetime>::new::<i32>();
///
/// assert_eq!(vtable.descriptor().type_name(), "i32");
/// assert_eq!(vtable.descriptor().size(), 4);
/// assert_eq!(vtable.descriptor().alignment(), 4);
///
/// // get vtable for f64
/// let vtable = <&BasicVTable<bounds::Normal, Lifetime> as VTableOf<f64>>::instance();
///
/// assert_eq!(vtable.descriptor().type_name(), "f64");
/// assert_eq!(vtable.descriptor().size(), 8);
/// assert_eq!(vtable.descriptor().alignment(), 8);
/// ```
#[repr(C)]
pub struct BasicVTable<B, Id> {
    bounds_impl: BoundsImpl<B>,
    descriptor: Descriptor<Id>,
}

impl<B, Id> Clone for BasicVTable<B, Id> {
    fn clone(&self) -> Self {
        Self {
            bounds_impl: self.bounds_impl,
            descriptor: self.descriptor,
        }
    }
}

impl<B, Id: Eq> fmt::Debug for BasicVTable<B, Id> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("LifetimeVTable")
            .field("type_name", &self.descriptor.type_name())
            .field("size", &self.descriptor.size)
            .field("alignment", &self.descriptor.alignment)
            .finish()
    }
}

impl<B: IsBound, Id: Eq> BasicVTable<B, Id> {
    /// Return the const instance of the vtable for `T`.
    ///
    /// `T` needs to implement [`StaticForm`].
    pub const fn new<T>() -> Self
    where
        T: Dynamic<B> + UniqueId<Id>,
    {
        <Self as ConstVTableOf<T>>::INSTANCE
    }
}

// SAFETY:
// The descriptor and bounds impl are always in sync.
// The Static Id type is well behaved because its built on LifetimeTypeId.
unsafe impl<B: IsBound, Id: Eq> VTable for BasicVTable<B, Id> {
    type Bounds = B;

    type Id = Id;

    fn descriptor(&self) -> &super::Descriptor<Self::Id> {
        &self.descriptor
    }

    fn bound_impl(&self) -> &super::BoundsImpl<Self::Bounds> {
        &self.bounds_impl
    }
}

// SAFETY: The type we describe doesnt change.
unsafe impl<B: IsBound, Id: Eq, Bnew: IsBound> VTableSubset<Bnew>
    for BasicVTable<B, Id>
where
    Bnew: Subset<B>,
{
    type Subset = BasicVTable<Bnew, Id>;

    fn into_subset(self) -> Self::Subset {
        BasicVTable {
            bounds_impl: self.bounds_impl.into_subset::<Bnew>(),
            descriptor: self.descriptor,
        }
    }
}

// SAFETY: The type we describe doesnt change.
unsafe impl<'a, B: IsBound, Id: Eq, Bnew: IsBound>
    VTableSubset<Bnew> for &'a BasicVTable<B, Id>
where
    Bnew: Subset<B>,
{
    type Subset = &'a BasicVTable<Bnew, Id>;

    fn into_subset(self) -> Self::Subset {
        // SAFETY: LifetimeVTable is repr(C) and BoundsImpl says its safe to transmute it
        // if we follow the Subset rule which we do.
        // Descriptor doesnt need to be repr(C) because it has the same type.
        unsafe {
            &*(self as *const BasicVTable<B, Id>
                as *const BasicVTable<Bnew, Id>)
        }
    }
}

// SAFETY: The returned vtable is for type T and the function doesn't ever panic.
unsafe impl<T, B: IsBound, Id: Eq> VTableOf<T>
    for BasicVTable<B, Id>
where
    T: Dynamic<B> + UniqueId<Id>,
{
    fn instance() -> Self {
        Self::new::<T>()
    }
}

// SAFETY: The type we describe doesnt change.
unsafe impl<T, B: IsBound, Id: Eq> ConstVTableOf<T>
    for BasicVTable<B, Id>
where
    T: Dynamic<B> + UniqueId<Id>,
{
    const INSTANCE: Self = Self {
        bounds_impl: BoundsImpl::new::<T>(),
        descriptor: Descriptor::new::<T>(),
    };
}

// SAFETY: The returned vtable is for type T and the function doesn't ever panic.
unsafe impl<T, B: IsBound, Id: Eq> VTableOf<T>
    for &BasicVTable<B, Id>
where
    T: Dynamic<B> + UniqueId<Id>,
{
    fn instance() -> Self {
        &ConstVTableOf::<T>::INSTANCE
    }
}

// SAFETY: The type we describe doesnt change.
unsafe impl<T, B: IsBound, Id: Eq> ConstVTableOf<T>
    for &BasicVTable<B, Id>
where
    T: Dynamic<B> + UniqueId<Id>,
{
    const INSTANCE: Self = &ConstVTableOf::<T>::INSTANCE;
}

#[cfg(test)]
mod test {
    use core::sync::atomic::AtomicBool;

    use crate::{bound::bounds, vtable::Static};

    use super::*;

    #[test]
    fn has_debug() {
        let _ = format!("{:?}", BasicVTable::<bounds::Normal, Static>::new::<i32>());
    }

    #[test]
    fn can_drop_a_t() {
        static Y: AtomicBool = AtomicBool::new(false);

        struct X {
            a: i32,
        }

        impl Drop for X {
            fn drop(&mut self) {
                assert_eq!(self.a, 123);

                Y.store(true, core::sync::atomic::Ordering::Relaxed);
            }
        }

        // let v = LifetimeVTable::new::<X, bounds::Empty>();
        //
        // let mut x = MaybeUninit::new(X { a: 123 });
        //
        // unsafe { v.drop_in_place(x.as_mut_ptr() as _) };
        //
        // assert!(Y.load(core::sync::atomic::Ordering::Relaxed));
    }

    #[test]
    fn vtable_interface() {
        fn assert_for<T: 'static>() {
            // let v = LifetimeVTable::new::<T, bounds::Empty>();
            //
            // assert_eq!(v.size(), core::mem::size_of::<T>());
            // assert_eq!(v.alignment(), core::mem::align_of::<T>());
            // assert_eq!(v.is_type::<T>());
            // let _ = v.type_name();

            // let _ = <LifetimeVTable as VTableOf<T, bounds::Empty>>::instance();
            // let _ = <&LifetimeVTable as VTableOf<T, bounds::Empty>>::instance();
            //
            // let _ = <LifetimeVTable as ConstVTableOf<T, bounds::Empty>>::INSTANCE;
            // let _ = <&LifetimeVTable as ConstVTableOf<T, bounds::Empty>>::INSTANCE;
        }

        assert_for::<i32>();
        assert_for::<String>();
        assert_for::<()>();
        assert_for::<u8>();
        assert_for::<[i32; 100]>();
    }
}
