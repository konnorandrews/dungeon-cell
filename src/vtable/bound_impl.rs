use core::marker::PhantomData;
use core::mem::MaybeUninit;
use core::ptr::addr_of;

use crate::bound::{Dynamic, Subset};
use crate::marker_traits::IsBound;

/// Implementation based on dynamic bound `B`.
///
/// This struct provides the ability to type erase the implementation of `drop`, `clone`, and
/// `Debug::fmt`. This type should be stored by a vtable to provide the implementations for those
/// methods.
///
/// # Invariants
/// It is safe to transmute (either via the actual transmute function, or pointers)
/// from `BoundsImpl<A>` to `BoundsImpl<B>` where `B: Subset<A>` as given by
/// [`Subset`][crate::bound::Subset]. This allows for the implementation of
/// [`VTableSubset`][super::VTableSubset] for borrows.
#[repr(C)]
pub struct BoundsImpl<B> {
    drop_impl: unsafe fn(data: *mut MaybeUninit<u8>),
    clone_impl: unsafe fn(
        data: *const MaybeUninit<u8>,
        data_other: *mut MaybeUninit<u8>,
    ),
    debug_impl: unsafe fn(
        data: *const MaybeUninit<u8>,
        f: *mut ::core::fmt::Formatter<'_>,
    ) -> Result<(), ::core::fmt::Error>,
    _phantom: PhantomData<B>,
}

impl<B> Copy for BoundsImpl<B> {}
impl<B> Clone for BoundsImpl<B> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<B: IsBound> BoundsImpl<B> {
    /// Automatically extract the implementations from the type.
    ///
    /// - [`Self::drop_value()`] is always implemented using [`drop_in_place`][std::ptr::drop_in_place].
    /// - [`Self::clone_value()`] will only be available if `B` contains the `Clone` bound.
    /// - [`Self::debug_value()`] will only be available if `B` contains the `Debug` bound.
    pub const fn new<T: Dynamic<B>>() -> Self {
        Self {
            drop_impl: drop_t_in_place::<T>,
            clone_impl: clone_t::<T, B>,
            debug_impl: debug_t::<T, B>,
            _phantom: PhantomData,
        }
    }

    /// Convert into a [`BoundsImpl`] with less trait bounds.
    ///
    /// The implementations will remain the same. This is safe to do because only
    /// adding traits to a bound could cause a implementation to be ran when it's not
    /// implemented already. Which can't happen with the `Bnew: Subset<B>` bound.
    pub const fn into_subset<Bnew: IsBound>(self) -> BoundsImpl<Bnew>
    where
        Bnew: Subset<B>,
    {
        BoundsImpl {
            drop_impl: self.drop_impl,
            clone_impl: self.clone_impl,
            debug_impl: self.debug_impl,
            _phantom: PhantomData,
        }
    }

    /// Drop value.
    ///
    /// The value will be invalid after calling this on it.
    ///
    /// # Safety
    /// The `data` buffer must contain a valid value of the type
    /// this [`BoundsImpl`] was created for.
    pub unsafe fn drop_value(&self, data: *mut MaybeUninit<u8>) {
        // SAFETY: Called via drop_value.
        unsafe {
            (self.drop_impl)(data);
        }
    }

    /// Clone value.
    ///
    /// # Safety
    /// - `B::BOUND_BY_CLONE` must be `true` and/or `B::CloneMarker: Clone`.
    /// - The `data` buffer must contain a valid value of the type
    /// this [`BoundsImpl`] was created for.
    /// - The `data_other` buffer must be large enough and aligned properly
    /// to store the value.
    pub unsafe fn clone_value(
        &self,
        data: *const MaybeUninit<u8>,
        data_other: *mut MaybeUninit<u8>,
    ) {
        // SAFETY: Called via clone_value.
        unsafe {
            (self.clone_impl)(data, data_other);
        }
    }

    /// Debug format value.
    ///
    /// # Safety
    /// - `B::BOUND_BY_DEBUG` must be `true` and/or `B::DebugMarker: Debug`.
    /// - The `data` buffer must contain a valid value of the type
    /// this [`BoundsImpl`] was created for.
    pub unsafe fn debug_value(
        &self,
        data: *const MaybeUninit<u8>,
        f: &mut ::core::fmt::Formatter<'_>,
    ) -> Result<(), ::core::fmt::Error> {
        // SAFETY: Called via debug_value.
        unsafe { (self.debug_impl)(data, f) }
    }
}

/// Drop a `T` in a buffer.
///
/// # Saftey
/// Must only be called through [`BoundsImpl::drop_value`].
unsafe fn drop_t_in_place<T>(data: *mut MaybeUninit<u8>) {
    // SAFETY:
    // We are allowed make `data` into a valid `T` because
    // of the safety requirements of BoundsImpl::drop_value.
    unsafe { ::core::ptr::drop_in_place::<T>(data.cast::<T>()) }
}

/// Clone a `T` in a buffer.
///
/// # Saftey
/// Must only be called through [`BoundsImpl::clone_value`].
unsafe fn clone_t<T, B: IsBound>(
    data: *const MaybeUninit<u8>,
    other_data: *mut MaybeUninit<u8>,
) where
    T: Dynamic<B>,
{
    // SAFETY: We know that data has a value of T via clone_value's safety requirements.
    let value = unsafe { &*data.cast::<T>() };

    // SAFETY: We know that B has Clone because of clone_value's safety requirements.
    let cloned = MaybeUninit::new(unsafe { T::clone_unchecked(value) });

    // SAFETY: We know cloned is valid to read (and initialized) because we just made it.
    // We also know other_data can have a value of T written into it because of
    // clone_value's safety requirements.
    // And we know they aren't overlaping because we just made cloned.
    unsafe {
        ::core::ptr::copy_nonoverlapping(
            addr_of!(cloned),
            other_data.cast::<MaybeUninit<T>>(),
            1,
        )
    }

    // The drop code for cloned will not run here because its in a MaybeUninit.
}

/// Debug format a `T` in a buffer.
///
/// # Saftey
/// Must only be called through [`BoundsImpl::debug_value`].
unsafe fn debug_t<T, B: IsBound>(
    data: *const MaybeUninit<u8>,
    f: *mut ::core::fmt::Formatter<'_>,
) -> Result<(), ::core::fmt::Error>
where
    T: Dynamic<B>,
{
    // SAFETY: By the safety requirements of debug_value we know data has a valid T.
    let value = unsafe { &*data.cast::<T>() };

    // SAFETY: Because this is called by debug_value we know that it was just a borrow.
    // We had to convert it to a pointer to allow const constructing a BoundsImpl.
    let f = unsafe { &mut *f };

    // SAFETY: We know B has Debug because of the safety requirements of debug_value.
    unsafe { T::debug_unchecked(value, f) }
}
