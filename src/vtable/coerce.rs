use core::hint::unreachable_unchecked;
use core::marker::PhantomData;
use core::mem::MaybeUninit;

use crate::AutoCoerce;
use crate::bound::Dynamic;
use crate::marker_traits::{IsBound, IsMut};
use crate::unsize::Mut;

use super::{
    BasicVTable, BoundsImpl, ConstVTableOf, Descriptor, UniqueId, VTable, VTableOf,
};

pub struct CoercionVTable<'a, U: ?Sized, M, V> {
    coerce_ref: unsafe fn(data: *const MaybeUninit<u8>) -> *const U,
    coerce_mut: unsafe fn(data: *mut MaybeUninit<u8>) -> *mut U,
    _phantom: PhantomData<fn() -> M>,

    vtable: &'a V,
}

impl<'a, U: ?Sized, M, V> Clone for CoercionVTable<'a, U, M, V>
where
    V: Clone,
{
    fn clone(&self) -> Self {
        Self {
            coerce_ref: self.coerce_ref,
            coerce_mut: self.coerce_mut,
            _phantom: PhantomData,
            vtable: self.vtable,
        }
    }
}

pub unsafe trait Coerce {
    type Target: ?Sized;

    unsafe fn coerce(&self, data: *const MaybeUninit<u8>) -> *const Self::Target;
}

pub unsafe trait CoerceMut: Coerce {
    unsafe fn coerce_mut(&self, data: *mut MaybeUninit<u8>) -> *mut Self::Target;
}

unsafe impl<'a, U: ?Sized, M: IsMut, V> Coerce for CoercionVTable<'a, U, M, V> {
    type Target = U;

    unsafe fn coerce(&self, data: *const MaybeUninit<u8>) -> *const U {
        unsafe { (self.coerce_ref)(data as _) }
    }
}

unsafe impl<'a, U: ?Sized, V> CoerceMut for CoercionVTable<'a, U, Mut<true>, V> {
    unsafe fn coerce_mut(&self, data: *mut MaybeUninit<u8>) -> *mut U {
        unsafe { (self.coerce_mut)(data as _) }
    }
}

unsafe impl<V: Coerce> Coerce for &V {
    type Target = V::Target;

    unsafe fn coerce(&self, data: *const MaybeUninit<u8>) -> *const Self::Target {
        unsafe { V::coerce(self, data) }
    }
}

unsafe impl<V: CoerceMut> CoerceMut for &V {
    unsafe fn coerce_mut(&self, data: *mut MaybeUninit<u8>) -> *mut Self::Target {
        unsafe { V::coerce_mut(self, data) }
    }
}

// SAFETY: The returned vtable is for type T and the function doesn't ever panic.
unsafe impl<'a, U: ?Sized + 'a, M: 'a, V, T> VTableOf<T>
    for &'a CoercionVTable<'a, U, M, V>
where
    V: VTable,
    T: Dynamic<V::Bounds>,
    T: AutoCoerce<'a, U, M, V>,
{
    fn instance() -> Self {
        T::AUTO.vtable()
    }
}

// SAFETY: The type we describe doesnt change.
unsafe impl<'a, U: ?Sized + 'a, M: 'a, V, T> ConstVTableOf<T>
    for &'a CoercionVTable<'a, U, M, V>
where
    V: VTable,
    T: Dynamic<V::Bounds>,
    T: AutoCoerce<'a, U, M, V>,
{
    const INSTANCE: Self = T::AUTO.vtable();
}

// // SAFETY: The returned vtable is for type T and the function doesn't ever panic.
// unsafe impl<T, B: IsBound, Id: Eq> VTableOf<T>
//     for &BasicVTable<B, Id>
// where
//     T: Dynamic<B> + UniqueId<Id>,
// {
//     fn instance() -> Self {
//         &ConstVTableOf::<T>::INSTANCE
//     }
// }
//
// // SAFETY: The type we describe doesnt change.
// unsafe impl<T, B: IsBound, Id: Eq> ConstVTableOf<T>
//     for &BasicVTable<B, Id>
// where
//     T: Dynamic<B> + UniqueId<Id>,
// {
//     const INSTANCE: Self = &ConstVTableOf::<T>::INSTANCE;
// }

unsafe impl<'a, U: ?Sized, M, V> VTable for CoercionVTable<'a, U, M, V>
where
    V: VTable,
{
    type Bounds = V::Bounds;

    type Id = V::Id;

    fn descriptor(&self) -> &Descriptor<Self::Id> {
        self.vtable.descriptor()
    }

    fn bound_impl(&self) -> &BoundsImpl<Self::Bounds> {
        self.vtable.bound_impl()
    }
}

impl<'a, U: ?Sized, V> CoercionVTable<'a, U, Mut<false>, V> {
    pub const fn new_ref<T>(
        coerce_ref: unsafe fn(data: *const MaybeUninit<u8>) -> *const U,
    ) -> Self
    where
        &'a V: ConstVTableOf<T>,
        T: Dynamic<<&'a V as VTable>::Bounds>
    {
        Self {
            coerce_ref,
            coerce_mut: |_| unsafe { unreachable_unchecked() },
            _phantom: PhantomData,
            vtable: <&'a V as ConstVTableOf<T>>::INSTANCE,
        }
    }
}

impl<'a, U: ?Sized, V> CoercionVTable<'a, U, Mut<true>, V> {
    pub const fn new_mut<T>(
        coerce_ref: unsafe fn(data: *const MaybeUninit<u8>) -> *const U,
        coerce_mut: unsafe fn(data: *mut MaybeUninit<u8>) -> *mut U,
    ) -> Self
    where
        &'a V: ConstVTableOf<T>,
        T: Dynamic<<&'a V as VTable>::Bounds>
    {
        Self {
            coerce_ref,
            coerce_mut,
            _phantom: PhantomData,
            vtable: <&'a V as ConstVTableOf<T>>::INSTANCE,
        }
    }
}

pub struct CoercionFor<T, V> {
    vtable: V,
    _phantom: PhantomData<fn(*const T)>,
}

impl<T, V> CoercionFor<T, V> {
    pub const unsafe fn new(vtable: V) -> Self {
        Self {
            vtable,
            _phantom: PhantomData,
        }
    }

    pub const fn vtable(
        &self,
    ) -> V
    where
        V: Copy,
    {
        self.vtable
    }
}

#[macro_export]
macro_rules! coerce {
    ($type:ty as $trait:ty) => {{
        $crate::coerce!($type as $trait, ref |x| x as _, mut |x| x as _)
    }};
    ($type:ty as ref $trait:ty) => {{
        $crate::coerce!($type as $trait, ref |x| x as _)
    }};
    (deref $type:ty as $unsized:ty) => {{
        $crate::coerce!(
            $type as $unsized,
            ref |x| ::core::ops::Deref::deref(unsafe { &*x }) as &$unsized,
            mut |x| ::core::ops::DerefMut::deref_mut(unsafe {&mut *x }) as &mut $unsized)
    }};
    (deref $type:ty as ref $unsized:ty) => {{
        $crate::coerce!(
            $type as $unsized,
            ref |x| ::core::ops::Deref::deref(unsafe { &*x }) as &$unsized
        )
    }};
    ($type:ty as $unsized:ty, ref |$ref_value:ident| $ref_expr:expr) => {
        unsafe {
            $crate::vtable::CoercionFor::<$type, _>::new(
                &$crate::vtable::CoercionVTable::new_ref::<$type>(
                    |
                        data: *const ::core::mem::MaybeUninit<u8>,
                    | -> *const $unsized {
                        let $ref_value = data as *const ::core::mem::MaybeUninit<u8>
                            as *const ::core::mem::MaybeUninit<$type> as *const $type;
                        {$ref_expr}
                    }
                )
            )
        }
    };
    ($type:ty as $unsized:ty, ref |$ref_value:ident| $ref_expr:expr, mut |$mut_value:ident| $mut_expr:expr) => {{
        $crate::vtable::CoercionVTable::<$type, $unsized, $crate::IsCoerceMut<true>> {
            _coerce_mut: ::core::marker::PhantomData,
            _marker: ::core::marker::PhantomData,
            vtable: &$crate::vtable::UnsizeVTable {
                core_vtable: <$crate::vtable::CoreVTable as $crate::vtable::ConstVTableOf<$type>>::INSTANCE,
                coerce_ref: |
                    data: *const ::core::mem::MaybeUninit<u8>,
                | -> *const $unsized {
                    let $ref_value = data as *const ::core::mem::MaybeUninit<u8>
                        as *const ::core::mem::MaybeUninit<$type> as *const $type;
                    {$ref_expr}
                },
                coerce_mut: |
                    data: *mut ::core::mem::MaybeUninit<u8>,
                | -> *mut $unsized {
                    let $mut_value = data as *mut ::core::mem::MaybeUninit<u8>
                        as *mut ::core::mem::MaybeUninit<$type> as *mut $type;
                    {$mut_expr}
                },
            }
        }
    }};
}
