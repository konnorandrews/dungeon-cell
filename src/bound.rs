use core::marker::PhantomData;

use self::boolean::{Bool, False, True};
use self::type_eq::TypeEq;

pub mod boolean;
pub mod type_eq;

pub mod traits;

/// A dynamic trait bound.
///
/// The trait is given by the generic `Bound`.
pub trait Dynamic<Bound> {
    /// The vtable that describes the properties the bound provides.
    type VTable: IsRequired + WithSelf<This = Self>;
}

/// Provide the `Self` type to an associated type.
pub trait WithSelf {
    /// The `Self` type.
    type This: ?Sized;
}

/// Flag for if a bound is required to hold.
pub trait IsRequired {
    /// If [`False`][boolean::False] then the bound is not required to hold.
    /// If [`True`][boolean::True] then the bound is required to hold.
    type Required: Bool;
}

/// Alias for the associated type of [`Dynamic`]: `VTable<T, Bound> = <T as Dynamic<Bound>>::VTable`
pub type VTable<T, Bound> = <T as Dynamic<Bound>>::VTable;

/// Send
/// Sync
/// Copy
/// Clone
/// Unpin
/// Debug
/// Display
/// PartialEq
/// Eq
/// PartialOrd
/// Ord
/// Hash
/// Error

mod demo {
    use super::traits::clone::CloneVTable;
    use super::traits::copy::CopyVTable;
    use super::traits::debug::DebugVTable;
    use super::traits::future::FutureVTable;
    use super::traits::send::SendVTable;
    use super::traits::sync::SyncVTable;
    use super::traits::unpin::UnpinVTable;
    use super::traits::{BClone, BCopy, BDebug, BFuture, BSend, BSync, BUnpin};
    use super::*;

    pub mod traits {
        use super::*;

        pub enum __ {}
        pub enum Send {}
        pub enum Sync {}
        pub enum Unpin {}
        pub enum Clone {}
        pub enum Copy {}
        pub enum Debug {}
        pub struct Future<Output>(PhantomData<fn() -> Output>);

        pub trait IsSend {
            type Value: Bool;
        }

        pub trait IsSync {
            type Value: Bool;
        }

        pub trait IsUnpin {
            type Value: Bool;
        }

        pub trait IsCloneCopy {
            type CloneValue: Bool;
            type CopyValue: Bool;
        }

        pub trait IsDebug {
            type Value: Bool;
        }

        pub trait IsFuture {
            type Output;

            type Value: Bool;
        }

        impl IsSend for __ {
            type Value = False;
        }

        impl IsSync for __ {
            type Value = False;
        }

        impl IsUnpin for __ {
            type Value = False;
        }

        impl IsCloneCopy for __ {
            type CloneValue = False;
            type CopyValue = False;
        }

        impl IsDebug for __ {
            type Value = False;
        }

        impl IsFuture for __ {
            type Output = ();
            type Value = False;
        }

        impl IsSend for Send {
            type Value = True;
        }

        impl IsSync for Sync {
            type Value = True;
        }

        impl IsUnpin for Unpin {
            type Value = True;
        }

        impl IsCloneCopy for Clone {
            type CloneValue = True;
            type CopyValue = False;
        }

        impl IsCloneCopy for Copy {
            type CloneValue = True;
            type CopyValue = True;
        }

        impl IsDebug for Debug {
            type Value = True;
        }

        impl<Output> IsFuture for Future<Output> {
            type Output = Output;
            type Value = True;
        }
    }
    use traits::*;

    pub struct Bound<
        Send: IsSend,
        Sync: IsSync,
        CloneCopy: IsCloneCopy,
        Debug: IsDebug,
        Unpin: IsUnpin,
        Future: IsFuture,
    >(PhantomData<(Send, Sync, CloneCopy, Debug, Unpin, Future)>);

    pub trait DynamicSet<Bound> {
        type Send: SendVTable + IsRequired + WithSelf<This = Self>;
        type Sync: SyncVTable + IsRequired + WithSelf<This = Self>;
        type Clone: CloneVTable + IsRequired + WithSelf<This = Self>;
        type Copy: CopyVTable + IsRequired + WithSelf<This = Self>;
        type Unpin: UnpinVTable + IsRequired + WithSelf<This = Self>;
        type Debug: DebugVTable + IsRequired + WithSelf<This = Self>;
        type Future: FutureVTable + IsRequired + WithSelf<This = Self>;
    }

    impl<
            This,
            Send: IsSend,
            Sync: IsSync,
            CloneCopy: IsCloneCopy,
            Debug: IsDebug,
            Unpin: IsUnpin,
            Future: IsFuture,
        > DynamicSet<Bound<Send, Sync, CloneCopy, Debug, Unpin, Future>>
        for This
    where
        This: Dynamic<BSend<<Send as IsSend>::Value>>,
        VTable<This, BSend<<Send as IsSend>::Value>>: SendVTable,

        This: Dynamic<BSync<<Sync as IsSync>::Value>>,
        VTable<This, BSync<<Sync as IsSync>::Value>>: SyncVTable,

        This: Dynamic<BClone<<CloneCopy as IsCloneCopy>::CloneValue>>,
        VTable<This, BClone<<CloneCopy as IsCloneCopy>::CloneValue>>:
            CloneVTable,

        This: Dynamic<BCopy<<CloneCopy as IsCloneCopy>::CopyValue>>,
        VTable<This, BCopy<<CloneCopy as IsCloneCopy>::CopyValue>>: CopyVTable,

        This: Dynamic<BUnpin<<Unpin as IsUnpin>::Value>>,
        VTable<This, BUnpin<<Unpin as IsUnpin>::Value>>: UnpinVTable,

        This: Dynamic<BDebug<<Debug as IsDebug>::Value>>,
        VTable<This, BDebug<<Debug as IsDebug>::Value>>: DebugVTable,

        This: Dynamic<
            BFuture<<Future as IsFuture>::Output, <Future as IsFuture>::Value>,
        >,
        VTable<
            This,
            BFuture<<Future as IsFuture>::Output, <Future as IsFuture>::Value>,
        >: FutureVTable,
    {
        type Send = VTable<This, BSend<<Send as IsSend>::Value>>;
        type Sync = VTable<This, BSync<<Sync as IsSync>::Value>>;
        type Clone =
            VTable<This, BClone<<CloneCopy as IsCloneCopy>::CloneValue>>;
        type Copy = VTable<This, BCopy<<CloneCopy as IsCloneCopy>::CopyValue>>;
        type Unpin = VTable<This, BUnpin<<Unpin as IsUnpin>::Value>>;
        type Debug = VTable<This, BDebug<<Debug as IsDebug>::Value>>;
        type Future = VTable<
            This,
            BFuture<<Future as IsFuture>::Output, <Future as IsFuture>::Value>,
        >;
    }
}

pub struct Demo<T, Bound>
where
    T: demo::DynamicSet<Bound>,
{
    value: T,
    _marker: PhantomData<Bound>,
}

unsafe impl<T, Bound> Send for Demo<T, Bound>
where
    T: demo::DynamicSet<Bound>,
    <T as demo::DynamicSet<Bound>>::Send: IsRequired<Required = True>,
{
}

fn test() {
    use demo::traits::*;
    use demo::Bound;

    let y = core::cell::Cell::new(32i32);
    let x = Demo::<_, Bound<Send, __, __, __, __, Future<i32>>> {
        value: async { 32 },
        // value: 101i32,
        _marker: PhantomData,
    };

    let x = Demo::<_, Bound<Send, __, __, __, __, __>> {
        value: 101i32,
        _marker: PhantomData,
    };
}
