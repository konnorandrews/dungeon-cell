//! Markers for generic configuration types.
//!
//! [`dungeon_cell`][crate] uses generic types for configuration. Each of these configuration
//! options only has a single type that is actually accepted. The traits
//! in this module mark those types. All of the following traits are [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
//! and cannot be implemented by external types. When a [`dungeon_cell`][crate] type needs a
//! generic with a bound of one of the following traits use the associated implementor type.

// mod alignment;
mod bound;
// mod layout;
// mod size;
// mod coerce;

// pub use alignment::*;
pub use bound::*;
// pub use layout::*;
// pub use size::*;
// pub use coerce::*;

mod sealed {
    pub trait Sealed {}
}
