/// Macro based on the one from `cfg_if` but simplified.
macro_rules! cfg_if {
    (
        if #[cfg($meta:meta)] {
            $($tokens:tt)*
        } else {
            $($tokens2:tt)*
        }
    ) => {
        #[cfg($meta)]
        $crate::cfg_if! {@_ $($tokens)* }

        #[cfg(not($meta))]
        $crate::cfg_if! {@_ $($tokens2)* }
    };
    (@_ $($tokens:tt)*) => {
        $($tokens)*
    };
}

pub(crate) use cfg_if;
