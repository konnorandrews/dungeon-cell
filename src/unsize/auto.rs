use crate::vtable::CoercionVTable;

mod _auto_special_case {
    use crate::{
        bound::Dynamic,
        marker_traits::IsBound,
        vtable::{CoercionFor, ConstVTableOf, UniqueId, VTable},
        AutoCoerce, Mut,
    };

    use super::*;

    // impl<T: 'static, U: 'static> AutoCoerceFrom<U, IsCoerceMut<false>> for T
    // where
    //     U: Deref<Target = T>,
    // {
    //     const AUTO: CoercionVTable<U, Self, IsCoerceMut<false>> =
    //         crate::coerce!(U as ref T);
    // }
    //
    // impl<T: 'static, U: 'static> AutoCoerceFrom<U, IsCoerceMut<true>> for T
    // where
    //     U: DerefMut<Target = T>,
    // {
    //     const AUTO: CoercionVTable<U, Self, IsCoerceMut<true>> =
    //         crate::coerce!(U as T);
    // }

    impl<'a, T, V> AutoCoerce<'a, str, Mut<false>, V> for T
    where
        T: Dynamic<<&'a V as VTable>::Bounds> + AsRef<str> + 'a,
        V: 'a,
        &'a V: ConstVTableOf<T>,
    {
        const AUTO: CoercionFor<T, &'a CoercionVTable<'a, str, Mut<false>, V>> =
            crate::coerce!(T as str, ref |x| unsafe { &*x }.as_ref());
    }

    // impl<'a, T: 'a> AutoCoerce<'a, str, Mut<true>> for T
    // where
    //     T: AsRef<str> + AsMut<str>,
    // {
    //     const AUTO: CoercionVTable<T, Self, Mut<true>> = crate::coerce!(T as str, ref |x| unsafe { &*x }.as_ref(), mut |x| unsafe { &mut *x }.as_mut());
    // }
    //
    // impl<T: 'static, U: 'static> AutoCoerceFrom<U, IsCoerceMut<false>> for [T]
    // where
    //     U: AsRef<[T]>,
    // {
    //     const AUTO: CoercionVTable<U, Self, IsCoerceMut<false>> =
    //         crate::coerce!(U as [T], ref |x| unsafe { &*x }.as_ref());
    // }
    //
    // impl<T: 'static, U: 'static> AutoCoerceFrom<U, IsCoerceMut<true>> for [T]
    // where
    //     U: AsRef<[T]> + AsMut<[T]>,
    // {
    //     const AUTO: CoercionVTable<U, Self, IsCoerceMut<true>> = crate::coerce!(U as [T], ref |x| unsafe { &*x }.as_ref(), mut |x| unsafe {&mut *x}.as_mut());
    // }
}

/*
#[cfg(feature = "unsize")]
pub mod unsize {
    use super::*;

    #[repr(transparent)]
    pub struct Unsize<T: ?Sized>(pub T);

    impl<T: ?Sized> Deref for Unsize<T> {
        type Target = T;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    impl<T: ?Sized> DerefMut for Unsize<T> {
        fn deref_mut(&mut self) -> &mut Self::Target {
            &mut self.0
        }
    }

    impl<T: 'static, U: 'static + ?Sized> AutoCoerceFrom<T, IsCoerceMut<true>>
        for Unsize<U>
    where
        T: core::marker::Unsize<U>,
    {
        const AUTO: CoercionVTable<T, Self, IsCoerceMut<true>> = crate::coerce!(T as Unsize<U>, ref |x| unsafe{ &*x } as &U as *const U as _, mut |x| unsafe {&mut *x} as &mut U as *mut U as _);
    }

    impl<T: 'static, U: 'static + ?Sized> AutoCoerceFrom<T, IsCoerceMut<false>>
        for Unsize<U>
    where
        T: core::marker::Unsize<U>,
    {
        const AUTO: CoercionVTable<T, Self, IsCoerceMut<false>> = crate::coerce!(T as Unsize<U>, ref |x| unsafe { &*x } as &U as *const U as _);
    }
}

#[macro_export]
macro_rules! auto_coerce_wrapper {
    ($vis:vis struct $name:ident<T: $trait:path>(pub T);) => {
        $vis struct $name<T: $trait>(pub T);

        impl<T: 'static + $trait> AutoCoerceFrom<$name<T>, IsCoerceMut<true>>
            for dyn $trait
        {
            const AUTO: dungeon_cell::vtable::CoercionVTable<
                $name<T>,
                Self,
                IsCoerceMut<true>,
            > = coerce!($name<T> as dyn $trait, ref |x| &unsafe { &*x }.0, mut |x| &mut unsafe { &mut *x }.0);
        }

        impl<T: 'static + $trait> AutoCoerceFrom<$name<T>, IsCoerceMut<false>>
            for dyn $trait
        {
            const AUTO: dungeon_cell::vtable::CoercionVTable<
                $name<T>,
                Self,
                IsCoerceMut<false>,
            > = coerce!($name<T> as dyn $trait, ref |x| &unsafe { &*x }.0);
        }
    }
}

#[macro_export]
macro_rules! auto_coerce {
    ($trait:ident) => {
        impl<T: 'static + $trait> AutoCoerceFrom<T, IsCoerceMut<true>>
            for dyn $trait
        {
            const AUTO: dungeon_cell::vtable::CoercionVTable<
                T,
                Self,
                IsCoerceMut<true>,
            > = coerce!(T as dyn $trait, ref |x| unsafe { &*x }, mut |x| unsafe { &mut *x });
        }

        impl<T: 'static + $trait> AutoCoerceFrom<T, IsCoerceMut<false>>
            for dyn $trait
        {
            const AUTO: dungeon_cell::vtable::CoercionVTable<
                T,
                Self,
                IsCoerceMut<false>,
            > = coerce!(T as dyn $trait, ref |x| unsafe { &*x });
        }
    }
}

macro_rules! impl_auto_coerce {
    ($(
        $([$($extra:tt)+])? $trait:ident
        $([
            $($([$($extra_generic:tt)+])? $generic:ident),*
            $(+ [
                $($([$($extra_associated_generic:tt)+])?
                $associated_name:ident = $associated_generic:ident),+
            ])?
        ])?
    ),+ $(,)?) => {
        $(
        impl<
            T: $($($extra)+ +)? $trait $(<$($generic,)* $($($associated_name = $associated_generic),+)?>)? + 'static
            $(
                $(, $generic: $($($extra_generic)+ +)? 'static)*
                $(
                    $(, $associated_generic: $($($extra_associated_generic)+ +)? 'static)+
                )?
            )?
        > $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<true>>
            for dyn $trait $(<$($generic,)* $($($associated_name = $associated_generic),+)?>)?
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<true>> =
                $crate::coerce!(T as dyn $trait $(<$($generic,)* $($($associated_name = $associated_generic),+)?>)?);
        }

        impl<
            T: $($($extra)+ +)? $trait $(<$($generic,)* $($($associated_name = $associated_generic),+)?>)? + 'static
            $(
                $(, $generic: $($($extra_generic)+ +)? 'static)*
                $(
                    $(, $associated_generic: $($($extra_associated_generic)+ +)? 'static)+
                )?
            )?
        >  $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<false>>
            for dyn $trait $(<$($generic,)* $($($associated_name = $associated_generic),+)?>)?
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<false>> =
                $crate::coerce!(T as ref dyn $trait $(<$($generic,)* $($($associated_name = $associated_generic),+)?>)?);
        }
        )+
    };
}

mod _core {
    use ::core::*;

    mod _alloc {
        use super::alloc::*;
        impl_auto_coerce!(GlobalAlloc);
    }

    mod _any {
        use super::any::*;
        impl_auto_coerce!(Any);
    }

    mod _borrow {
        use super::borrow::*;
        impl_auto_coerce!(Borrow[[?Sized] B], BorrowMut[[?Sized] B]);
    }

    // Clone is not object safe.

    mod _cmp {
        use super::cmp::*;

        // Eq, Ord are not object safe.
        impl_auto_coerce!(PartialEq[[?Sized] R], PartialOrd[[?Sized] R]);
    }

    mod _convert {
        use super::convert::*;

        // From, Into, TryFrom, TryInto are not object safe.
        impl_auto_coerce!(AsMut[[?Sized] U], AsRef[[?Sized] U]);
    }

    // Default is not object safe.

    mod _fmt {
        use super::fmt::*;
        impl_auto_coerce!(
            Binary, Debug, Display, LowerExp, LowerHex, Octal, Pointer,
            UpperExp, UpperHex, Write,
        );
    }

    mod _future {
        use super::future::*;
        impl_auto_coerce!(
            Future[+[[?Sized]Output=O]],
        );

        #[cfg(has_path_future_intofuture)]
        impl_auto_coerce!(
            IntoFuture[+[[?Sized]Output=O, [?Sized + Future<Output = O>]IntoFuture=F]]
        );
    }

    mod _hash {
        use super::hash::*;

        // Hash is not object safe.
        impl_auto_coerce!(
            BuildHasher[+[[?Sized]Hasher=H]],
            Hasher,
        );
    }

    mod _iter {
        use super::iter::*;

        // Extend, FromIterator, Product, Sum are not object safe.
        impl_auto_coerce!(
            DoubleEndedIterator[+[[?Sized] Item=I]],
            ExactSizeIterator[+[[?Sized] Item=I]],
            FusedIterator[+[[?Sized] Item=I]],
            IntoIterator[+[[?Sized] Item=I, [?Sized] IntoIter=II]],
            Iterator[+[[?Sized] Item=I]],
        );
    }

    mod _marker {
        use super::marker::*;

        // Copy, Sized are not object safe.
        impl_auto_coerce!(Send, Sync, Unpin);
    }

    mod _ops {
        use super::ops::*;

        // Drop should not be used as a trait object.
        // Fn, FnMut, FnOnce are implemented elsewhere.
        // RangeBounds is not object safe.
        impl_auto_coerce!(
            Add[R + [[?Sized] Output=O]],
            AddAssign[R],
            BitAnd[R + [[?Sized] Output=O]],
            BitAndAssign[R],
            BitOr[R + [[?Sized] Output=O]],
            BitOrAssign[R],
            BitXor[R + [[?Sized] Output=O]],
            BitXorAssign[R],
            Deref[+[[?Sized] Target=U]],
            DerefMut[+[[?Sized] Target=U]],
            Div[R + [[?Sized] Output=O]],
            DivAssign[R],
            Index[[?Sized] I +[[?Sized] Output=O]],
            IndexMut[[?Sized] I +[[?Sized] Output=O]],
            Mul[R + [[?Sized] Output=O]],
            MulAssign[R],
            Neg[+[[?Sized] Output=O]],
            Not[+[[?Sized] Output=O]],
            Rem[R + [[?Sized] Output=O]],
            RemAssign[R],
            Shl[R + [[?Sized] Output=O]],
            ShlAssign[R],
            Shr[R + [[?Sized] Output=O]],
            ShrAssign[R],
            Sub[R + [[?Sized] Output=O]],
            SubAssign[R],
        );
    }

    #[cfg(has_unwind_safe)]
    mod _panic {
        use super::panic::*;
        impl_auto_coerce!(RefUnwindSafe, UnwindSafe,);
    }

    mod _slice {
        use super::slice::*;
        impl_auto_coerce!(
            SliceIndex[[?Sized] U + [[?Sized] Output=O]]
        );
    }

    // FromStr is not object safe.
}

/// Reverse the order of a sequence of ident tokens.
macro_rules! rev_args {
    ($($swapped:ident)* |) => {
        impl_fn_auto!($($swapped)*);
    };
    ($($swapped:ident)* | $first_arg:ident $($arg:ident)*) => {
        rev_args!($first_arg $($swapped)* | $($arg)*);
    };
}

macro_rules! impl_fn_auto {
    () => {
        impl<T: Fn() -> R + 'static, R> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<true>>
            for dyn Fn() -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<true>> =
                $crate::coerce!(T as dyn Fn() -> R);
        }

        impl<T: Fn() -> R + 'static, R> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<false>>
            for dyn Fn() -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<false>> =
                $crate::coerce!(T as ref dyn Fn() -> R);
        }

        impl<T: FnMut() -> R + 'static, R> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<true>>
            for dyn FnMut() -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<true>> =
                $crate::coerce!(T as dyn FnMut() -> R);
        }

        impl<T: FnMut() -> R + 'static, R> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<false>>
            for dyn FnMut() -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<false>> =
                $crate::coerce!(T as ref dyn FnMut() -> R);
        }

        impl<T: FnOnce() -> R + 'static, R> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<true>>
            for dyn FnOnce() -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<true>> =
                $crate::coerce!(T as dyn FnOnce() -> R);
        }

        impl<T: FnOnce() -> R + 'static, R> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<false>>
            for dyn FnOnce() -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<false>> =
                $crate::coerce!(T as ref dyn FnOnce() -> R);
        }
    };
    ($($arg:ident)*) => {
        impl<T: Fn($($arg),*) -> R + 'static, R, $($arg),*> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<true>>
            for dyn Fn($($arg),*) -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<true>> =
                $crate::coerce!(T as dyn Fn($($arg),*) -> R);
        }

        impl<T: Fn($($arg),*) -> R + 'static, R, $($arg),*> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<false>>
            for dyn Fn($($arg),*) -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<false>> =
                $crate::coerce!(T as ref dyn Fn($($arg),*) -> R);
        }

        impl<T: FnMut($($arg),*) -> R + 'static, R, $($arg),*> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<true>>
            for dyn FnMut($($arg),*) -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<true>> =
                $crate::coerce!(T as dyn FnMut($($arg),*) -> R);
        }

        impl<T: FnMut($($arg),*) -> R + 'static, R, $($arg),*> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<false>>
            for dyn FnMut($($arg),*) -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<false>> =
                $crate::coerce!(T as ref dyn FnMut($($arg),*) -> R);
        }

        impl<T: FnOnce($($arg),*) -> R + 'static, R, $($arg),*> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<true>>
            for dyn FnOnce($($arg),*) -> R where Self: 'static
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<true>> =
                $crate::coerce!(T as dyn FnOnce($($arg),*) -> R);
        }

        impl<T, R, $($arg),*> $crate::unsize::auto::AutoCoerceFrom<T, $crate::IsCoerceMut<false>>
            for dyn FnOnce($($arg),*) -> R
        where
            Self: 'static,
            T: 'static + FnOnce($($arg),*) -> R
        {
            const AUTO:  $crate::unsize::auto::CoercionVTable<T, Self, $crate::IsCoerceMut<false>> =
                $crate::coerce!(T as ref dyn FnOnce($($arg),*) -> R);
        }
    };
}

/// Recursively impl `CallableOnce` for all param amounts.
macro_rules! impl_fn_auto_all {
    () => {
        impl_fn_auto!();
    };
    ($last_arg:ident $($arg:ident)*) => {
        impl_fn_auto_all!($($arg)*);
        rev_args!(| $last_arg $($arg)*);
    };
}

// Implement for everything with 30 params or less.
#[cfg(feature = "many_arg_fn")]
impl_fn_auto_all!(A30 A29 A28 A27 A26 A25 A24 A23 A22 A21 A20 A19 A18 A17 A16 A15 A14 A13 A12 A11 A10 A9 A8 A7 A6 A5 A4 A3 A2 A1);

#[cfg(not(feature = "many_arg_fn"))]
impl_fn_auto_all!(A10 A9 A8 A7 A6 A5 A4 A3 A2 A1);

#[cfg(feature = "alloc")]
mod _alloc {
    use crate::alloc::*;

    // ToOwned is not object safe.

    mod _string {
        use super::string::*;
        impl_auto_coerce!(ToString);
    }

    // Wake is not object safe.
}

#[cfg(feature = "std")]
mod _std {
    use ::std::*;

    mod _error {
        use super::error::*;
        impl_auto_coerce!(Error);
    }

    mod _io {
        use super::io::*;
        impl_auto_coerce!(BufRead, Read, Seek, Write);
    }

    mod _net {
        use super::net::*;
        impl_auto_coerce!(ToSocketAddrs[+[[?Sized] Iter=I]]);
    }

    #[cfg(has_path_os_fd)]
    mod _os {
        use super::os::*;
        mod _fs {
            use super::fd::*;
            impl_auto_coerce!(AsFd, AsRawFd, IntoRawFd);
        }
    }

    #[cfg(has_path_process_termination)]
    mod _process {
        use super::process::*;
        impl_auto_coerce!(Termination);
    }
}

#[cfg(test)]
mod test {
    use crate::{AutoCoerceFrom, IsCoerceMut};

    fn assert<T: AutoCoerceFrom<U, IsCoerceMut<true>> + ?Sized, U>() {}
    fn assert_by_example<
        T: AutoCoerceFrom<U, IsCoerceMut<true>> + ?Sized,
        U,
    >(
        _: U,
    ) {
    }

    #[test]
    fn core() {
        #[cfg(has_path_future_intofuture)]
        struct IntoFutureTest;

        #[cfg(has_path_future_intofuture)]
        impl core::future::IntoFuture for IntoFutureTest {
            type Output = ();

            type IntoFuture = core::future::Pending<()>;

            fn into_future(self) -> Self::IntoFuture {
                unimplemented!()
            }
        }

        assert::<dyn core::alloc::GlobalAlloc, std::alloc::System>();
        assert::<dyn core::any::Any, ()>();
        assert::<dyn core::borrow::Borrow<str>, String>();
        assert::<dyn core::borrow::BorrowMut<str>, String>();
        assert::<dyn core::cmp::PartialEq<str>, String>();
        assert::<dyn core::cmp::PartialOrd<i32>, i32>();
        assert::<dyn core::convert::AsMut<str>, String>();
        assert::<dyn core::convert::AsRef<str>, String>();
        assert::<dyn core::fmt::Binary, i32>();
        assert::<dyn core::fmt::Debug, i32>();
        assert::<dyn core::fmt::Display, i32>();
        assert::<dyn core::fmt::LowerExp, i32>();
        assert::<dyn core::fmt::LowerHex, i32>();
        assert::<dyn core::fmt::Octal, i32>();
        assert::<dyn core::fmt::Pointer, &i32>();
        assert::<dyn core::fmt::UpperExp, i32>();
        assert::<dyn core::fmt::UpperHex, i32>();
        assert::<dyn core::fmt::Write, String>();
        assert_by_example::<dyn core::future::Future<Output = ()>, _>(async {});

        #[cfg(has_path_future_intofuture)]
        assert::<
            dyn core::future::IntoFuture<
                Output = (),
                IntoFuture = core::future::Pending<()>,
            >,
            IntoFutureTest,
        >();

        assert::<
            dyn core::hash::BuildHasher<
                Hasher = std::collections::hash_map::DefaultHasher,
            >,
            std::collections::hash_map::RandomState,
        >();

        assert::<
            dyn core::hash::Hasher,
            std::collections::hash_map::DefaultHasher,
        >();

        assert::<
            dyn core::iter::DoubleEndedIterator<Item = u8>,
            std::vec::IntoIter<u8>,
        >();

        assert::<
            dyn core::iter::ExactSizeIterator<Item = u8>,
            std::vec::IntoIter<u8>,
        >();

        assert::<
            dyn core::iter::FusedIterator<Item = u8>,
            std::vec::IntoIter<u8>,
        >();

        assert::<
            dyn core::iter::IntoIterator<
                Item = u8,
                IntoIter = std::vec::IntoIter<u8>,
            >,
            Vec<u8>,
        >();

        assert::<dyn core::iter::Iterator<Item = u8>, std::vec::IntoIter<u8>>();

        assert::<dyn core::marker::Send, u8>();
        assert::<dyn core::marker::Sync, u8>();
        assert::<dyn core::marker::Unpin, u8>();
        assert::<dyn core::ops::Add<u8, Output = u8>, u8>();
        assert::<dyn core::ops::AddAssign<u8>, u8>();
        assert::<dyn core::ops::BitAnd<u8, Output = u8>, u8>();
        assert::<dyn core::ops::BitAndAssign<u8>, u8>();
        assert::<dyn core::ops::BitOr<u8, Output = u8>, u8>();
        assert::<dyn core::ops::BitOrAssign<u8>, u8>();
        assert::<dyn core::ops::BitXor<u8, Output = u8>, u8>();
        assert::<dyn core::ops::BitXorAssign<u8>, u8>();
        assert::<dyn core::ops::Deref<Target = [u8]>, Vec<u8>>();
        assert::<dyn core::ops::DerefMut<Target = [u8]>, Vec<u8>>();
        assert::<dyn core::ops::Div<u8, Output = u8>, u8>();
        assert::<dyn core::ops::DivAssign<u8>, u8>();

        assert_by_example::<dyn Fn(i32) -> u8, _>(|_| unimplemented!());
        assert_by_example::<dyn FnMut(i32) -> u8, _>(|_| unimplemented!());
        assert_by_example::<dyn FnOnce(i32) -> u8, _>(|_| unimplemented!());

        assert::<dyn core::ops::Index<usize, Output = u8>, Vec<u8>>();
        assert::<dyn core::ops::IndexMut<usize, Output = u8>, Vec<u8>>();
        assert::<dyn core::ops::Mul<u8, Output = u8>, u8>();
        assert::<dyn core::ops::MulAssign<u8>, u8>();
        assert::<dyn core::ops::Neg<Output = i8>, i8>();
        assert::<dyn core::ops::Not<Output = u8>, u8>();
        assert::<dyn core::ops::Rem<u8, Output = u8>, u8>();
        assert::<dyn core::ops::RemAssign<u8>, u8>();
        assert::<dyn core::ops::Shl<u8, Output = u8>, u8>();
        assert::<dyn core::ops::ShlAssign<u8>, u8>();
        assert::<dyn core::ops::Shr<u8, Output = u8>, u8>();
        assert::<dyn core::ops::ShrAssign<u8>, u8>();
        assert::<dyn core::ops::Sub<u8, Output = u8>, u8>();
        assert::<dyn core::ops::SubAssign<u8>, u8>();

        #[cfg(has_unwind_safe)]
        assert::<dyn core::panic::RefUnwindSafe, u8>();

        #[cfg(has_unwind_safe)]
        assert::<dyn core::panic::UnwindSafe, u8>();

        assert::<dyn core::slice::SliceIndex<[u8], Output = u8>, usize>();
    }

    #[cfg(feature = "alloc")]
    #[test]
    fn alloc() {
        assert::<dyn crate::alloc::string::ToString, u8>();
    }

    #[cfg(feature = "std")]
    #[test]
    fn std() {
        assert::<dyn std::error::Error, std::convert::Infallible>();
        assert::<dyn std::io::BufRead, &[u8]>();
        assert::<dyn std::io::Read, &[u8]>();
        assert::<dyn std::io::Seek, std::io::Cursor<&[u8]>>();
        assert::<dyn std::io::Write, &mut [u8]>();

        assert::<
            dyn std::net::ToSocketAddrs<
                Iter = std::vec::IntoIter<std::net::SocketAddr>,
            >,
            (&str, u16),
        >();

        #[cfg(has_path_os_fd)]
        assert::<dyn std::os::fd::AsFd, std::fs::File>();

        #[cfg(has_path_os_fd)]
        assert::<dyn std::os::fd::AsRawFd, std::fs::File>();

        #[cfg(has_path_os_fd)]
        assert::<dyn std::os::fd::IntoRawFd, std::fs::File>();

        #[cfg(has_path_process_termination)]
        assert::<dyn std::process::Termination, ()>();
    }
}
*/
