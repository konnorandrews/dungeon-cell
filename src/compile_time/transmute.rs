//! Transmute that bypasses the normal restrictions of being used on generics.

use core::mem::ManuallyDrop;

/// Transmute for const contexts.
///
/// This transmute bypasses basically all restrictions on the normal
/// [`core::mem::transmute`]. As such, this function is extremely unsafe to use.
/// It is only used because we can't use traits in const expressions to get the internals
/// if associated types.
///
/// # Safety
/// `T` must be transmutable to `U`. This means `U` must be the same layout as `T` or contained in
/// the layout of `T`.
pub const unsafe fn const_transmute<T, U>(value: T) -> U {
    // Create union type that can store a `T` or a `U`.
    // We can then use this to convert between them.
    //
    // The repr(C) layout forces no offset between `t` and `u` as talked about here
    // https://rust-lang.github.io/unsafe-code-guidelines/layout/unions.html#c-compatible-layout-repr-c
    #[repr(C)]
    union Transmute<T, U> {
        t: ManuallyDrop<T>,
        u: ManuallyDrop<U>,
    }

    // Create the union in the `T` state.
    let value = Transmute {
        t: ManuallyDrop::new(value),
    };

    // Read from the union in the `U` state.
    // SAFETY: This is safe because the caller has promised that `T` can be transmuted to `U`.
    // The following reference link talks about repr(C) unions being used this way.
    // https://doc.rust-lang.org/reference/items/unions.html#reading-and-writing-union-fields
    ManuallyDrop::into_inner(unsafe { value.u })
}

#[cfg(test)]
mod test {
    use core::mem::MaybeUninit;

    use super::*;

    #[test]
    fn transmuting_to_the_same_type_results_in_the_original_value() {
        #[derive(PartialEq, Debug, Clone)]
        struct MyType {
            value: i32,
        }

        let original = MyType { value: 123 };
        let to_be_transmuted = original.clone();

        // SAFETY: `MyType` to `MyType` doesn't change anything.
        let transmuted: MyType = unsafe { const_transmute(to_be_transmuted) };

        assert_eq!(transmuted, original);
    }

    #[test]
    fn const_transmute_can_be_wrapped_in_a_generic_const_function() {
        // The normal transmute doesn't work if wrapped.
        const fn _broken_wrapper<T, U>(_t: T) -> U {
            // error[E0512]: cannot transmute between types of different sizes, or dependently-sized types
            //   --> src/const_transmute.rs:58:22
            //    |
            // 58 |             unsafe { core::mem::transmute(t) }
            //    |                      ^^^^^^^^^^^^^^^^^^^^
            //    |
            //    = note: source type: `T` (this type does not have a fixed size)
            //    = note: target type: `U` (this type does not have a fixed size)

            // unsafe { core::mem::transmute(t) }
            unimplemented!();
        }

        // Our transmute does work if wrapped.
        const fn _working_wrapper<T, U>(t: T) -> U {
            // SAFETY: This function isnt actually called.
            unsafe { const_transmute(t) }
        }
    }

    #[test]
    fn transmuting_from_a_type_to_a_type_with_the_same_layout_is_sound() {
        #[repr(C)]
        struct MyTypeA {
            value_a: i32,
        }

        #[derive(PartialEq, Debug)]
        #[repr(C)]
        struct MyTypeB {
            value_b: i32,
        }

        let original = MyTypeA { value_a: 123 };

        // SAFETY: `MyTypeA` and `MyTypeB` have the same layout because of repr(C).
        let transmuted: MyTypeB = unsafe { const_transmute(original) };

        assert_eq!(transmuted, MyTypeB { value_b: 123 });
    }

    #[test]
    fn transmuting_from_a_transparent_wrapper_type_to_its_internal_type_is_sound(
    ) {
        #[repr(transparent)]
        struct MyTypeA {
            _inner: MyTypeB,
        }

        #[derive(PartialEq, Debug, Clone)]
        struct MyTypeB {
            value: i32,
        }

        let original = MyTypeB { value: 123 };
        let to_be_transmuted = MyTypeA {
            _inner: original.clone(),
        };

        // SAFETY: `MyTypeA` is repr transparent to `MyTypeB`.
        let transmuted: MyTypeB = unsafe { const_transmute(to_be_transmuted) };

        assert_eq!(transmuted, original);
    }

    #[test]
    fn transmuting_from_a_type_to_a_longer_maybe_uninit_buffer_then_back_is_sound(
    ) {
        #[derive(PartialEq, Debug, Clone)]
        struct MyType {
            value: i32,
        }

        let original = MyType { value: 123 };
        let to_be_transmuted = original.clone();

        type Buffer = [MaybeUninit<u8>; core::mem::size_of::<MyType>() + 4];

        // SAFETY: `Buffer` is 4 bytes longer than `MyType`.
        let transmuted: Buffer = unsafe { const_transmute(to_be_transmuted) };

        // SAFETY: We just put a `MyType` value into the buffer.
        let transmuted_back: MyType = unsafe { const_transmute(transmuted) };

        assert_eq!(transmuted_back, original);
    }
}
