// Enable `std` only if the feature is enabled or in test or doc mode.
#![cfg_attr(not(any(test, doc, feature = "std")), no_std)]
// Feature needed for more friendly compile time errors.
#![cfg_attr(has_feature_const_type_name, feature(const_type_name))]
// This crate **doesn't** use generic_const_exprs for correctness.
// This feature is only used to trigger the compile time errors earlier.
#![cfg_attr(has_feature_generic_const_exprs, allow(incomplete_features))]
#![cfg_attr(has_feature_generic_const_exprs, feature(generic_const_exprs))]
// Feature allows for automatic implementation of the coercion helpers.
#![cfg_attr(feature = "unsize", feature(unsize))]
// Consider unsafe functions as a external constraint only.
#![deny(unsafe_op_in_unsafe_fn)]
// Enable some extra warnings.
#![warn(
    clippy::undocumented_unsafe_blocks,
    clippy::default_union_representation
)]

//! Cell and Cell-like types that can store any type without dynamic memory.
//!
//! Currently only the [`DungeonCore`] primitive is implemented (unsized, cell, stack vec, ... are in progress).
//!
//! #### Example
//! ```
//! use dungeon_cell::{DungeonCore, layout_for};
//!
//! // a default DungeonCore can store any 'static type that fits in the layout
//! let mut core = DungeonCore::<layout_for!(String)>::default();
//!
//! // we can store a i32 and get it back
//! core.store(1234);
//! assert_eq!(core.take(), Some(1234i32));
//!
//! // we can store a f64 and get it back
//! core.store(1.234);
//! assert_eq!(core.take(), Some(1.234f64));
//!
//! // lets get adventurous and store a String
//! core.store(String::from("hello world"));
//! assert_eq!(core.take(), Some(String::from("hello world")));
//!
//! // we can't take a type the core isn't storing
//! core.store(1234);
//! assert_eq!(core.take(), None::<f32>);
//!
//! // we can borrow both unique and shared
//! core.store(1234);
//! *core.borrow_mut::<i32>().unwrap() += 10;
//! assert_eq!(core.borrow(), Some(&1244i32));
//! ```
//!
//! # Features
//!
//! - `"alloc"` - Enable support for `alloc` types.
//! - `"std"` - Enable support for `std` types. Also enables `"alloc"`.
//! - `"unsize"` - Enable use of the nightly feature `unsize`. Requires a nightly compiler.
//! - `"many_arg_fn"` - Enable implementations for functions with 11-30 arguments.
//!
//! # No-std Support
//!
//! This crate is `#![no_std]` by default, it can be used anywhere Rust can.
//!
//! # Minimum Supported Rust Version
//!
//! Requires Rust 1.64.0.
//!
//! This crate follows the ["Latest stable Rust" policy](https://gist.github.com/alexheretic/d1e98d8433b602e57f5d0a9637927e0c). The listed MSRV won't be changed unless needed.
//! However, updating the MSRV anywhere up to the latest stable at time of release
//! is allowed.

use core::marker::PhantomData;

#[cfg(all(feature = "alloc", not(feature = "std")))]
extern crate alloc;
// #[cfg(all(feature = "alloc", feature = "std"))]
// use std as alloc;

// mod cfg_if;
// pub mod lifetime_type_id;

pub mod bound;
// pub mod layout;
// pub mod marker_traits;
// pub mod type_match;

// mod const_transmute;
// mod prison;
// pub mod vtable;

// pub mod align;
// mod core;

// pub mod buffer;
// pub mod compile_time;
// mod unsize;
// mod cell;
// mod any;
// mod cell;

// pub use const_transmute::*;
// pub use prison::*;

// pub use self::core::*;
// pub use unsize::*;
// pub use cell::*;
// pub use any::*;

/// Used by doctests to panic even if const asserts have been disabled.
#[doc(hidden)]
#[cfg(use_const_assert)]
pub const HAS_CONST_PANIC: () = ();

unsafe fn unreachable() -> ! {
    #[cfg(debug_assertions)]
    unreachable!();

    #[cfg(not(debug_assertions))]
    unsafe {
        ::core::hint::unreachable_unchecked()
    }
}

/// Type encoded `true`.
pub enum True {}

/// Type encoded `false`.
pub enum False {}

/// Check if a flag is either [`Yes`] or [`No`].
///
/// `Yes` implements `Flag<Yes>` and `No` implements `Flag<No>`.
pub trait TypeEq {
    type Other: ?Sized;
}

impl<T: ?Sized> TypeEq for T {
    type Other = Self;
}

/// A dynamic trait bound.
///
/// The trait is given by the generic `Bound`.
pub trait Dynamic<Bound> {
    /// Type of the vtable to describe the properties the trait requires.
    type VTable: LockedThis<This = Self>;
}

/// Trait to lock a VTable to a specific `Self` type.
///
/// This reduces the bounds needed at usage sites of [`Dynamic`].
pub trait LockedThis {
    /// The `Self` type.
    type This: ?Sized;
}

pub type BClone<Flag = True> = clone::BClone<Flag>;
pub use clone::CloneVTable;

pub type VTable<T, B> = <T as Dynamic<B>>::VTable;

mod clone {
    use super::*;

    /// Bound on the [`Clone`] trait.
    ///
    /// The `Flag` generic can either be [`Yes`] or [`No`].
    pub struct BClone<Flag>(PhantomData<Flag>);

    impl<F, T> Dynamic<BClone<F>> for T {
        type VTable = HasClone<F, T>;
    }

    /// Trait for the [`Clone`] trait vtable.
    pub unsafe trait CloneVTable: LockedThis {
        /// Either [`Yes`] or [`No`] depending on if the [`Clone`] trait was bounded on.
        type Flag;

        /// Same as `Self::Flag`.
        ///
        /// This allows for runtime checking if the [`Clone`] trait was bounded on.
        const FLAG: bool;

        /// Clone the type using it's [`Clone`] impl.
        ///
        /// # Safety
        /// This function can only be called if `Self::Flag` is `Yes` or `Self::FLAG` is
        /// `true`.
        unsafe fn clone_unchecked(this: &Self::This) -> Self::This
        where
            Self::This: Sized;

        /// Clone the type using it's [`Clone`] impl.
        ///
        /// To call this function `Self::Flag` must be bounded to `Yes`.
        fn clone(this: &Self::This) -> Self::This
        where
            Self::This: Sized,
            Self::Flag: TypeEq<Other = True>,
        {
            // SAFETY: Self::Flag must be Yes because of the bound above.
            unsafe { Self::clone_unchecked(this) }
        }
    }

    /// Marker type that implements the [`Clone`] vtable for some `T`.
    pub struct HasClone<Flag, T: ?Sized>(PhantomData<(Flag, fn() -> *const T)>);

    // We lock the `Self` of `Dynamic<BClone>` to the `T` generic.
    impl<F, This: ?Sized> LockedThis for HasClone<F, This> {
        type This = This;
    }

    // This is implemented for all types that implement `Clone`.
    unsafe impl<This: Clone> CloneVTable for HasClone<True, This> {
        type Flag = True;

        const FLAG: bool = true;

        unsafe fn clone_unchecked(this: &This) -> This {
            This::clone(this)
        }
    }

    // This is implemented for all types.
    unsafe impl<This: ?Sized> CloneVTable for HasClone<False, This> {
        type Flag = False;

        const FLAG: bool = false;

        unsafe fn clone_unchecked(_this: &This) -> This
        where
            This: Sized,
        {
            // SAFETY: This function can't be called because of the safety requirements.
            // And, we set Flag to No and FLAG to false.
            unsafe { core::hint::unreachable_unchecked() }
        }
    }
}

fn use_clone<T: Dynamic<B>, B>(a: &T) -> T
where
    VTable<T, B>: CloneVTable<Flag = True>,
{
    <VTable<T, B> as CloneVTable>::clone(a)
}

fn use_clone_runtime<T: Dynamic<B>, B>(a: &T) -> T
where
    VTable<T, B>: CloneVTable,
{
    if <VTable<T, B> as CloneVTable>::FLAG {
        unsafe { <VTable<T, B> as clone::CloneVTable>::clone_unchecked(a) }
    } else {
        panic!()
    }
}

struct B<T>(T);

impl<T: Dynamic<BClone>> Clone for B<T>
where
    VTable<T, BClone>: CloneVTable<Flag = True>,
{
    fn clone(&self) -> Self {
        Self(<VTable<T, BClone> as CloneVTable>::clone(&self.0))
    }
}

#[test]
fn demo() {
    struct A;

    use_clone::<_, BClone<False>>(&A);
    // use_clone::<_, BClone<()>>(&A);
    // use_clone2::<_, BClone<No>>(&42i32);
}
