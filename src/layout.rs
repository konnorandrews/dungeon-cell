//! Compile time layout description.

mod alignment;
mod size;

use core::marker::PhantomData;
use core::mem;

use crate::compile_time::{
    const_usize_assert, AlignmentSmallerOrEqualTo, SizeSmallerOrEqualTo,
    UsizeAssert,
};
use crate::marker_traits::{IsAlignment, IsLayout, IsSize};

pub use alignment::Alignment;
pub use size::Size;

/// Combination of size and alignment.
///
/// This is the type to use where a [`IsLayout`] generic is required.
///
/// Unlike the [`std::alloc::Layout`], this type is for layouts known at compile time.
/// Additionally, this type doesn't require size to be a multiple of alignment.
///
/// The layout given to a [`dungeon_cell`][crate] type determines the types it can store.
/// To find the layout for a given set of types the
/// [`layout_for!()`][crate::layout_for] macro can be used.
///
/// # Examples
///
/// ```
/// use dungeon_cell::layout::{Layout, Size, Alignment};
///
/// type Test = Layout<Size<4>, Alignment<2>>;
///
/// assert_eq!(Test::size(), 4);
/// assert_eq!(Test::alignment(), 2);
/// ```
#[derive(Copy, Clone)]
pub struct Layout<Size: IsSize, Alignment: IsAlignment> {
    _phantom: PhantomData<(Size, Alignment)>,
}

impl<Size: IsSize, Alignment: IsAlignment> IsLayout
    for Layout<Size, Alignment>
{
    type Size = Size;

    type Alignment = Alignment;
}

impl<Size: IsSize, Alignment: IsAlignment> Layout<Size, Alignment> {
    /// Check if this layout exactly matches the layout for `T`.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::layout::{Layout, Size, Alignment};
    ///
    /// type TestLayout = Layout<Size<4>, Alignment<4>>;
    ///
    /// assert!(TestLayout::is_layout_of::<i32>());
    /// assert!(!TestLayout::is_layout_of::<i16>());
    /// ```
    #[inline]
    pub const fn is_layout_of<T>() -> bool {
        (mem::size_of::<T>() == Size::VALUE)
            && (mem::align_of::<T>() == Alignment::VALUE)
    }

    /// Size in bytes.
    ///
    /// This is the value of [`S::VALUE`][crate::marker_traits::IsSize::VALUE].
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::layout_for;
    ///
    /// assert_eq!(<layout_for!(String)>::size(), 24);
    /// ```
    #[inline]
    pub const fn size() -> usize {
        Size::VALUE
    }

    /// Alignment in bytes.
    ///
    /// This is the value of [`Alignment::VALUE`][crate::marker_traits::IsAlignment::VALUE].
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::layout_for;
    ///
    /// assert_eq!(<layout_for!(String)>::alignment(), 8);
    /// ```
    #[inline]
    pub const fn alignment() -> usize {
        Alignment::VALUE
    }

    /// Create a [`std::alloc::Layout`] with the size and alignment of this layout.
    ///
    /// This function is available on `no_std`.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::layout_for;
    /// use std::alloc::Layout;
    ///
    /// let l: Layout = <layout_for!(String)>::to_std_layout().unwrap();
    ///
    /// assert_eq!(l.size(), 24);
    /// assert_eq!(l.align(), 8);
    /// ```
    #[inline]
    pub const fn to_std_layout(
    ) -> Result<core::alloc::Layout, core::alloc::LayoutError> {
        core::alloc::Layout::from_size_align(Size::VALUE, Alignment::VALUE)
    }

    /// Check if memory with this layout would allow a `T` to be stored.
    ///
    /// This checks that the size of `T` is equal or less than [`Self::size()`],
    /// and that the alignment of `T` is equal or less than [`Self::alignment()`].
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::layout_for;
    ///
    /// type LayoutI32 = layout_for!(i32);
    ///
    /// assert!(LayoutI32::can_store::<u8>());
    /// assert!(LayoutI32::can_store::<u16>());
    /// assert!(LayoutI32::can_store::<i32>());
    /// assert!(!LayoutI32::can_store::<String>());
    /// ```
    #[inline]
    pub const fn can_store<T>() -> bool {
        (mem::size_of::<T>() <= Size::VALUE)
            && (mem::align_of::<T>() <= Alignment::VALUE)
    }

    /// Assert memory with this layout can store a value of `T`.
    ///
    /// This checks that the size of `T` is equal or less than [`Self::size()`],
    /// and that the alignment of `T` is equal or less than [`Self::alignment()`].
    ///
    /// Unsafe code can use the above if this function returns.
    ///
    /// This function uses the assert system in [`compile_time`][crate::compile_time]
    /// which means by default it will cause a compile time error if the assertion fails.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::layout_for;
    ///
    /// // a i16 is smaller than a i32
    /// <layout_for!(i32)>::assert_can_store::<i16>();
    /// ```
    /// ```compile_fail
    /// use dungeon_cell::layout_for;
    ///
    /// // a i32 is bigger than a i16
    /// <layout_for!(i16)>::assert_can_store::<i32>();
    /// ```
    #[track_caller]
    pub fn assert_can_store<T>() {
        SizeSmallerOrEqualTo::<T, Size>::assert();
        AlignmentSmallerOrEqualTo::<T, Alignment>::assert();
    }

    /// Const form of [`Self::assert_can_store()`].
    ///
    /// Prefer using [`Self::assert_can_store()`] if possible because it can give
    /// better error messages.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::layout_for;
    ///
    /// const fn test() {
    ///     // a i16 is smaller than a i32
    ///     <layout_for!(i32)>::const_assert_can_store::<i16>();
    /// }
    ///
    /// test();
    /// ```
    /// ```compile_fail
    /// use dungeon_cell::layout_for;
    ///
    /// const fn test() {
    ///     // a i32 is bigger than a i16
    ///     <layout_for!(i16)>::const_assert_can_store::<i32>();
    /// }
    ///
    /// test();
    /// ```
    #[track_caller]
    pub const fn const_assert_can_store<T>() {
        const_usize_assert::<SizeSmallerOrEqualTo<T, Size>>();
        const_usize_assert::<AlignmentSmallerOrEqualTo<T, Alignment>>();
    }
}

/// Helper to get [`Layout`] from a generic bounded by [`IsLayout`].
pub type LayoutType<L> =
    Layout<<L as IsLayout>::Size, <L as IsLayout>::Alignment>;

/// Calculate the [`Layout`] needed to store a set of types.
///
/// This macro expects input of the form `layout_for!(Type1, Type2, Type3)`.
/// The created [`Layout`] type will have a size equal to the maximum size
/// of the given types, and will have an alignment equal to the maximum alignment
/// of the given types. If you want the layout for the combination of types `Type1`,
/// `Type2`, `Type3` as one value then use `layout_for!((Type1, Type2, Type3))`.
///
/// # Examples
/// ```
/// use dungeon_cell::layout_for;
///
/// // sizes
///
/// assert_eq!(<layout_for!()>::size(), 0);
/// assert_eq!(<layout_for!(())>::size(), 0);
///
/// assert_eq!(<layout_for!(u8)>::size(), 1);
/// assert_eq!(<layout_for!(u8, i8)>::size(), 1);
///
/// assert_eq!(<layout_for!(u32)>::size(), 4);
/// assert_eq!(<layout_for!(u32, i32, u8, i16, ())>::size(), 4);
///
/// assert_eq!(<layout_for!(u32, u8, String, &'static str, ())>::size(), 24);
///
/// assert_eq!(<layout_for!([u8; 100], [i32; 5], String)>::size(), 100);
///
///
/// // alignments
///
/// assert_eq!(<layout_for!()>::alignment(), 1);
/// assert_eq!(<layout_for!(u8)>::alignment(), 1);
///
/// assert_eq!(<layout_for!(i32)>::alignment(), 4);
///
/// assert_eq!(<layout_for!([u8; 100], [i32; 5], String)>::alignment(), 8);
/// ```
///
/// # Example Expansion
/// ```
/// # use dungeon_cell::layout_for;
/// # type __ =
/// layout_for!(i32, u8)
/// # ;
/// ```
/// expands to
/// ```
/// # type __ =
/// ::dungeon_cell::layout::Layout::<
///     ::dungeon_cell::layout::Size<{ 
///         <(i32, u8) as ::dungeon_cell::layout::Measure>::SIZE 
///     }>,
///     ::dungeon_cell::layout::Alignment<{ 
///         <(i32, u8) as ::dungeon_cell::layout::Measure>::ALIGN 
///     }>,
/// >
/// # ;
/// ```
#[macro_export]
macro_rules! layout_for {
    ($($type:ty),* $(,)?) => {
        $crate::layout::Layout::<
            $crate::layout::Size<{
                <($($type,)*) as $crate::layout::Measure>::SIZE
            }>,
            $crate::layout::Alignment<{
                <($($type,)*) as $crate::layout::Measure>::ALIGN
            }>
        >
    };
    ($($t:tt)*) => {
        compile_error!("Expected input of the form `layout_for!(Type1, Type2, Type3)`")
    }
}

/// Measure the max size and max alignment of a set of types.
///
/// A set of types is represented as a tuple with each type being a field.
/// The tuples can be in any order.
///
/// # Example
/// ```
/// use dungeon_cell::layout::Measure;
/// use std::mem::{size_of, align_of};
///
/// assert_eq!(<() as Measure>::SIZE, 0);
/// assert_eq!(<(i32, f64) as Measure>::SIZE, size_of::<f64>());
/// assert_eq!(<(u8, f32, String) as Measure>::SIZE, size_of::<String>());
///
/// assert_eq!(<() as Measure>::ALIGN, 1);
/// assert_eq!(<(i32, f64) as Measure>::ALIGN, align_of::<f64>());
/// assert_eq!(<(u8, f32, String) as Measure>::ALIGN, align_of::<String>());
/// ```
pub trait Measure {
    /// Size of the largest type in bytes.
    ///
    /// This is **not** the size of all the types together.
    const SIZE: usize;

    /// Alignment of the largest type in bytes.
    const ALIGN: usize;
}

macro_rules! impl_measure {
    ($($generic:ident),*) => {
        impl<$($generic),*> Measure for ($($generic,)*) {
            const SIZE: usize = {
                #[allow(unused_mut)]
                let mut max = 0;
                $(
                    let size = ::core::mem::size_of::<$generic>();
                    if size > max {
                        max = size;
                    }
                )*
                max
            };

            const ALIGN: usize = {
                #[allow(unused_mut)]
                let mut max = 1;
                $(
                    let align = ::core::mem::align_of::<$generic>();
                    if align > max {
                        max = align;
                    }
                )*
                max
            };
        }
    }
}
impl_measure!();
impl_measure!(A);
impl_measure!(A, B);
impl_measure!(A, B, C);
impl_measure!(A, B, C, D);
impl_measure!(A, B, C, D, E);
impl_measure!(A, B, C, D, E, F);
impl_measure!(A, B, C, D, E, F, G);
impl_measure!(A, B, C, D, E, F, G, H);
impl_measure!(A, B, C, D, E, F, G, H, I);
impl_measure!(A, B, C, D, E, F, G, H, I, J);
impl_measure!(A, B, C, D, E, F, G, H, I, J, K);
impl_measure!(A, B, C, D, E, F, G, H, I, J, K, L);

#[cfg(test)]
mod test {
    use crate::layout::Alignment;

    use super::*;

    #[test]
    fn assert_can_store() {
        <Layout<Size<1>, Alignment<2>>>::assert_can_store::<u8>();
        <Layout<Size<1>, Alignment<2>>>::const_assert_can_store::<u8>();
    }

    #[test]
    fn is_layout_of() {
        assert!(Layout::<Size<2>, Alignment<2>>::is_layout_of::<i16>());
        assert!(Layout::<Size<1>, Alignment<1>>::is_layout_of::<u8>());
        assert!(Layout::<Size<0>, Alignment<4>>::is_layout_of::<[i32; 0]>());
        assert!(!Layout::<Size<0>, Alignment<4>>::is_layout_of::<[u8; 4]>());
        assert!(!Layout::<Size<1>, Alignment<2>>::is_layout_of::<i16>());
        assert!(!Layout::<Size<2>, Alignment<1>>::is_layout_of::<i16>());
    }

    #[test]
    fn size() {
        assert_eq!(Layout::<Size<0>, Alignment<1>>::size(), 0);
        assert_eq!(Layout::<Size<1>, Alignment<1>>::size(), 1);
        assert_eq!(Layout::<Size<2>, Alignment<1>>::size(), 2);
    }

    #[test]
    fn alignment() {
        assert_eq!(Layout::<Size<0>, Alignment<1>>::alignment(), 1);
        assert_eq!(Layout::<Size<0>, Alignment<2>>::alignment(), 2);
        assert_eq!(Layout::<Size<0>, Alignment<4>>::alignment(), 4);
    }

    #[cfg(feature = "alloc")]
    #[test]
    fn to_std_layout() {
        let l = Layout::<Size<3>, Alignment<8>>::to_std_layout().unwrap();
        assert_eq!(l.size(), 3);
        assert_eq!(l.align(), 8);
    }

    #[test]
    fn can_store() {
        assert!(!Layout::<Size<2>, Alignment<1>>::can_store::<u16>());
        assert!(Layout::<Size<2>, Alignment<1>>::can_store::<i8>());

        assert!(Layout::<Size<4>, Alignment<2>>::can_store::<i16>());
        assert!(!Layout::<Size<4>, Alignment<2>>::can_store::<u32>());
    }
}
