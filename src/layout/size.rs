/// Size in bytes.
///
/// This is the type to use where a [`IsSize`][crate::marker_traits::IsSize] generic is required.
/// This type acts as a wrapper for a `usize` const generic value to improve the readability of the
/// API.
///
/// # Examples
/// ```
/// use dungeon_cell::layout::Size;
/// use dungeon_cell::marker_traits::IsSize;
///
/// assert_eq!(Size::<4>::VALUE, 4);
/// ```
#[derive(Copy, Clone)]
pub struct Size<const N: usize> {
    _private: (),
}
