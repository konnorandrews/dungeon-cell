/// Alignment in bytes.
///
/// This is the type to use where a [`IsAlignment`][crate::marker_traits::IsAlignment] generic is required.
/// This type acts as a wrapper for a `usize` const generic value to improve the readability of the
/// API.
///
/// Alignment values with a power of two from 1 up to 2²⁹ are allowed as
/// given by the [rust reference](https://doc.rust-lang.org/reference/type-layout.html#the-alignment-modifiers).
///
/// # Examples
/// ```
/// use dungeon_cell::layout::Alignment;
/// use dungeon_cell::marker_traits::IsAlignment;
///
/// assert_eq!(Alignment::<8>::VALUE, 8);
/// ```
#[derive(Copy, Clone)]
pub struct Alignment<const N: usize> {
    _private: (),
}
