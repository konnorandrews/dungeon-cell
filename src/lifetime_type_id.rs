//! [`TypeId`] for types with one non-`'static` lifetime.
//!
//! [`TypeId`] only exists for `'static` types. This module implements a technique inspired by the [`better_any`](https://docs.rs/better_any/latest/better_any/index.html) crate.
//! With this technique, we can give a `T`, with a single lifetime parameter, a `'static`
//! marker type and take the [`TypeId`] of that marker. The [`LifetimeTypeId`] type
//! wraps around the [`TypeId`] this gives to have the borrow checker verify the
//! lifetime.

use core::any::TypeId;
use core::cmp::Ordering;
use core::marker::PhantomData;

/// Unique identifier for a type with zero or one non-'`static` lifetime.
///
/// If two [`LifetimeTypeId`] are equal with `==` then the type they represent
/// is identical including the lifetime. This is possible because we use the borrow
/// checker to prove that `'inv` is the same lifetime for both the left and right hand side
/// of the `==`. If the borrow checker can't prove they are the same then it won't let
/// the code compile. This is needed because [`TypeId`] can't encode lifetime information
/// for inspection at runtime.
#[derive(Copy, Clone, Debug, Eq, Hash)]
pub struct LifetimeTypeId<'inv> {
    /// Type ID of the static form of the type.
    static_type_id: TypeId,

    /// Marker to hold the lifetime.
    ///
    /// This must be invariant over `'inv` to make equality correct.
    ///
    /// The marker used is taken from:
    /// <https://doc.rust-lang.org/nomicon/phantom-data.html#table-of-phantomdata-patterns>
    _marker: PhantomData<fn(&'inv ()) -> &'inv ()>,
}

impl<'inv> LifetimeTypeId<'inv> {
    /// Get the [`LifetimeTypeId`] for a `T`.
    ///
    /// The `'inv` lifetime will be connected to the single lifetime of `T` if it has one.
    ///
    /// Types must enroll into this system by implementing [`StaticForm`].
    ///
    /// # Examples
    ///
    /// ```
    /// use dungeon_cell::lifetime_type_id::{LifetimeTypeId};
    ///
    /// assert_eq!(LifetimeTypeId::of::<&i32>(), LifetimeTypeId::of::<&i32>());
    /// assert_ne!(LifetimeTypeId::of::<&i32>(), LifetimeTypeId::of::<i32>());
    /// ```
    ///
    /// ```compile_fail
    /// use dungeon_cell::lifetime_type_id::{LifetimeTypeId, StaticForm};
    ///
    /// let foo = 123i32;
    /// let mut foo_borrow = &foo;
    ///
    /// {
    ///     let bar = 456i32;
    ///     let mut bar_borrow = &bar;
    ///     
    ///     // forces foo_borrow and bar_borrow to have the same lifetime
    ///     assert_eq!(of_value(&mut foo_borrow), of_value(&mut bar_borrow));
    /// }
    ///
    /// foo_borrow.set(42);
    ///
    /// // helper to get LifetimeTypeId from value
    /// fn of_value<'a, T: StaticForm<'a>>(_: &'_ mut T) -> LifetimeTypeId<'a> {
    ///     LifetimeTypeId::of::<T>()
    /// }
    /// ```
    pub fn of<T: ?Sized + StaticMarker<'inv>>() -> Self {
        Self {
            static_type_id: TypeId::of::<T::Marker>(),
            _marker: PhantomData,
        }
    }
}

impl<'inv> PartialEq for LifetimeTypeId<'inv> {
    fn eq(&self, other: &Self) -> bool {
        // Because `LifetimeTypeId` is invariant over `'inv` we know that both
        // `self` and `other` have the same lifetime parameter and we know that
        // all types that implement `StaticForm<'inv>` are unique except for the
        // lifetime parameter.
        //
        // Because of these two things we know the types must be the same including
        // the lifetime.

        self.static_type_id == other.static_type_id
    }
}

impl<'a> PartialOrd for LifetimeTypeId<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Ord for LifetimeTypeId<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        // See `PartialEq::eq` for why equality works.

        self.static_type_id.cmp(&other.static_type_id)
    }
}

/// Trait implemented for types with a static marker type associated with them.
///
/// This is for use with [`LifetimeTypeId`].
pub unsafe trait StaticMarker<'inv>: 'inv {
    type Marker: 'static;
}

/// Implement [`StaticMarker`] for type.
///
/// This macro uses a subset of the `impl` syntax.
/// However, you do not need to name the [`StaticMarker`] trait in the macro call or provide a body.
/// Additionally, a `'a` lifetime (it doesn't have to be literally `'a`) is required as the first
/// generic.
///
/// All generics will automatically be bounded by `StaticMarker<'a>`. To opt out of this
/// use `T: 'static` in the `impl` generic definitions. Additionally, extra trait bounds
/// can be placed in a `where` clause at the end.
///
/// See the examples below for the different ways to invoke the macro.
///
/// # Limitations
///
/// This macro can only implement [`StaticMarker`] for nameable types. For opaque types
/// you must use the [`give_opaque_static_marker`][crate::give_opaque_static_marker] macro to wrap a specific value.
///
/// # Examples
/// ```
/// use dungeon_cell::has_static_marker;
/// use dungeon_cell::lifetime_type_id::LifetimeTypeId;
///
/// // no lifetimes and no generics
/// struct BasicType;
/// has_static_marker!(impl<'a> BasicType);
/// # let _ = LifetimeTypeId::of::<BasicType>();
///
/// // one generic
/// struct OneGeneric<T>(T);
/// has_static_marker!(impl<'a, T> OneGeneric<T>);
/// # let _ = LifetimeTypeId::of::<OneGeneric<()>>();
///
/// // multiple generics
/// struct MultipleGeneric<A, B, C>(A, B, C);
/// has_static_marker!(impl<'a, A, B, C> MultipleGeneric<A, B, C>);
/// # let _ = LifetimeTypeId::of::<MultipleGeneric<(), (), ()>>();
///
/// // a lifetime
/// struct Lifetime<'a>(&'a str);
/// has_static_marker!(impl<'a> Lifetime<'a>);
/// # let _ = LifetimeTypeId::of::<Lifetime<'static>>();
///
/// // a lifetime and a generic
/// struct LifetimeGeneric<'a, T>(&'a T);
/// has_static_marker!(impl<'a, T> LifetimeGeneric<'a, T>);
/// # let _ = LifetimeTypeId::of::<LifetimeGeneric<'static, ()>>();
///
/// // a lifetime and a static generic
/// struct StaticGeneric<'a, T>(&'a T);
/// has_static_marker!(impl<'a, T: 'static> StaticGeneric<'a, T>);
///
/// struct WithoutStaticMarker;
/// let _ = LifetimeTypeId::of::<StaticGeneric<WithoutStaticMarker>>();
///
/// // extra trait bounds
/// struct AlwaysCopy<T: Copy>(T);
/// has_static_marker!(impl<'a, T> AlwaysCopy<T> where T: Copy);
/// # let _ = LifetimeTypeId::of::<AlwaysCopy<i32>>();
/// ```
#[macro_export]
macro_rules! impl_static_marker {
    ($($tail:tt)+) => {
        $crate::has_static_marker_helper!(
            @filter_static
            static
            impl $($tail)+
        );
    };
}

/// `tt` muncher for the [`has_static_marker`] macro.
///
/// This helper will filter out `: 'static` generics then find
/// the `where` clause if it exists.
#[doc(hidden)]
#[macro_export]
macro_rules! has_static_marker_helper {
    (
        @filter_static
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl<$($lt:lifetime)? $(,)? $token:ident : 'static $($tail:tt)+
    ) => {
        $crate::has_static_marker_helper!(
            @filter_static
            $(,$generic)*
            static $(,$static_generic)* ,$token
            impl<$($lt)? $($tail)+
        );
    };
    (
        @filter_static
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl<$($lt:lifetime)? $(,)? $token:ident $($tail:tt)+
    ) => {
        $crate::has_static_marker_helper!(
            @filter_static
            $(,$generic)* ,$token
            static $(,$static_generic)*
            impl<$($lt)? $($tail)+
        );
    };
    (
        @filter_static
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl<$lt:lifetime> $($tail:tt)+
    ) => {
        $crate::has_static_marker_helper!(
            @filter_where
            $(,$generic)*
            static $(,$static_generic)*
            impl<$lt> {} $($tail)+
        );
    };
    (
        @filter_static
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl<> $($tail:tt)+
    ) => {
        $crate::has_static_marker_helper!(
            @filter_where
            $(,$generic)*
            static $(,$static_generic)*
            impl<'a> {} $($tail)+
        );
    };
    (
        @filter_static
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl $($tail:tt)+
    ) => {
        $crate::has_static_marker_helper!(
            @filter_where
            $(,$generic)*
            static $(,$static_generic)*
            impl<'a> {} $($tail)+
        );
    };
    (
        @filter_where
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl<$lt:lifetime> { $($type:tt)* } where $($tail:tt)*
    ) => {
        const _: () = {
            #[allow(unused_imports)]
            use ::core::marker::PhantomData;

            pub struct __Marker<$($generic: ?Sized,)* $($static_generic: ?Sized),*>(
                $(PhantomData<$generic>,)* 
                $(PhantomData<$static_generic>),*
            );

            unsafe impl<
                $lt,
                $($generic: $crate::lifetime_type_id::StaticMarker<$lt>,)*
                $($static_generic: 'static),*
            > $crate::lifetime_type_id::StaticMarker<$lt> for $($type)*
            where
                $($tail)* {
                type Marker = __Marker<$($generic::Marker,)* $($static_generic),*>;
            }
        };
    };
    (
        @filter_where
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl<$lt:lifetime> { $($type:tt)* } $token:tt $($tail:tt)*
    ) => {
        $crate::has_static_marker_helper!(
            @filter_where
            $(,$generic)*
            static $(,$static_generic)*
            impl<$lt> { $($type)* $token } $($tail)*
        );
    };
    (
        @filter_where
        $(,$generic:ident)*
        static $(,$static_generic:ident)*
        impl<$lt:lifetime> { $($type:tt)* }
    ) => {
        const _: () = {
            #[allow(unused_imports)]
            use ::core::marker::PhantomData;

            pub struct __Marker<$($generic: ?Sized,)* $($static_generic: ?Sized),*>(
                $(PhantomData<$generic>,)* 
                $(PhantomData<$static_generic>),*
            );

            unsafe impl<
                $lt,
                $($generic: $crate::lifetime_type_id::StaticMarker<$lt>,)*
                $($static_generic: 'static),*
            > $crate::lifetime_type_id::StaticMarker<$lt> for $($type)* {
                type Marker = __Marker<$($generic::Marker,)* $($static_generic),*>;
            }
        };
    };
}

/// Wrap opaque typed value to implement [`StaticMarker`].
///
/// This macro generates a `#[repr(transparent)]` wrapper struct
/// that implements [`StaticMarker`]. The resulting wrapper is also
/// an opaque type.
///
/// Use the `value.0` accessor to get the original value.
///
/// # Examples
/// ```
/// use dungeon_cell::give_opaque_static_marker;
/// use dungeon_cell::lifetime_type_id::{LifetimeTypeId, StaticMarker};
///
/// let mut a = 42;
/// let b = &mut a;
///
/// // closures have an opaque type
/// let x = || *b += 1;
///
/// // wrap the closure so it works with of_value
/// let mut z = give_opaque_static_marker!(x);
///
/// // we can still access the closure.
/// z();
///
/// // the type ID of the closure isn't the same as a i32
/// assert_ne!(of_value(&z), of_value(&0));
///
/// // calling the closure changed the variable so it has a lifetime
/// assert_eq!(a, 43);
///
/// fn of_value<'a, T: StaticMarker<'a>>(_: &T) -> LifetimeTypeId<'a> {
///     LifetimeTypeId::of::<T>()
/// }
/// ```
#[macro_export]
macro_rules! give_opaque_static_marker {
    ($value:expr) => {{
        // This type will act as a opaque type from the outside of the macro scope.
        #[repr(transparent)]
        struct __Wrapper<T>(pub T);

        // SAFETY: The type `Wrapper` cannot be named outside of the macro scope.
        // Because of this no other uses of __Wrapper can exist.
        unsafe impl<'a, T: 'a> $crate::lifetime_type_id::StaticMarker<'a> for __Wrapper<T> {
            // This will be unique to the type of `$value`.
            type Static = __Wrapper<()>;
        }

        impl<T> ::core::ops::Deref for __Wrapper<T> {
            type Target = T;

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl<T> ::core::ops::DerefMut for __Wrapper<T> {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.0
            }
        }

        // Have type inference construct a new opaque type for us.
        __Wrapper($value)
    }}
}

const _: () = {
    pub struct __Marker<T: ?Sized, const N: usize>([PhantomData<T>; N]);

    unsafe impl<'a, T: StaticMarker<'a>, const N: usize> StaticMarker<'a> for [T; N] {
        type Marker = __Marker<T::Marker, N>;
    }
};

impl_static_marker!(bool);
impl_static_marker!(char);
impl_static_marker!(f32);
impl_static_marker!(f64);

impl_static_marker!(<R> fn() -> R);

impl_static_marker!(<A1, R> fn(A1) -> R);
impl_static_marker!(<A1, A2, R> fn(A1, A2) -> R);

impl_static_marker!(i8);
impl_static_marker!(i16);
impl_static_marker!(i32);
impl_static_marker!(i64);
impl_static_marker!(i128);
impl_static_marker!(isize);

impl_static_marker!(<T> *const T where T: ?Sized);
impl_static_marker!(<T> *mut T where T: ?Sized);

impl_static_marker!(<'a, T> &'a T where T: ?Sized);
impl_static_marker!(<'a, T> &'a mut T where T: ?Sized);

impl_static_marker!(<T> [T]);
impl_static_marker!(str);

impl_static_marker!(());
impl_static_marker!(<A> (A,));
impl_static_marker!(<A, B> (A, B));

impl_static_marker!(u8);
impl_static_marker!(u16);
impl_static_marker!(u32);
impl_static_marker!(u64);
impl_static_marker!(u128);
impl_static_marker!(usize);

fn test<'a, 'b, A: StaticMarker<'a>, B: StaticMarker<'b>>() -> bool {
    let x = LifetimeTypeId::of::<A>();
    let y = LifetimeTypeId::of::<B>();

    x == y
}
