use super::sealed::Sealed;
use crate::unsize::Mut;

/// Marker only implemented for [`IsCoerceMut`][crate::IsSend].
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented by external types.
pub trait IsMut: Sealed {
    const VALUE: bool;
}

impl Sealed for Mut<true> {}
impl IsMut for Mut<true> {
    const VALUE: bool = true;
}

impl Sealed for Mut<false> {}
impl IsMut for Mut<false> {
    const VALUE: bool = false;
}
