use core::fmt::Debug;
use core::panic::{RefUnwindSafe, UnwindSafe};

use super::sealed::Sealed;
use crate::bound::bounds;

mod do_gen;
// mod gen;

/// Marker only implemented for [`Bound`][crate::bound::Bound].
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented by external types.
pub unsafe trait IsBound {
    const SEND: bool = false;
    const SYNC: bool = false;
    const UNPIN: bool = false;
    const COPY: bool = false;
    const CLONE: bool = false;
    const DISPLAY: bool = false;
    const DEBUG: bool = false;
    const HASH: bool = false;
    const EQ: bool = false;
    const PARTIAL_EQ: bool = false;
    const ORD: bool = false;
    const PARTIAL_ORD: bool = false;

    type SendMarker;
    type SyncMarker;
    type UnpinMarker;
    type CloneMarker;
    type DebugMarker;
    type PartialEqMarker;
}

// pub(crate) use do_gen::cfg_impl;

mod sealed {
    use core::marker::{PhantomData, PhantomPinned};

    pub struct SendMarker;
    pub struct NotSendMarker(PhantomData<*const ()>);

    pub struct SyncMarker;
    pub struct NotSyncMarker(PhantomData<*const ()>);

    pub struct UnpinMarker;
    pub struct NotUnpinMarker(PhantomPinned);

    #[derive(Copy, Clone)]
    pub struct CopyMarker;
    #[derive(Clone)]
    pub struct CloneMarker;
    pub struct NotCloneMarker;

    #[derive(Debug)]
    pub struct DisplayMarker;

    impl core::fmt::Display for DisplayMarker {
        fn fmt(&self, _f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
            Ok(())
        }
    }

    #[derive(Debug)]
    pub struct DebugMarker;
    pub struct NotDebugMarker;

    #[derive(PartialEq, Eq, Hash, PartialOrd, Ord)]
    pub struct HashOrdMarker;
    #[derive(PartialEq, Eq, Hash, PartialOrd)]
    pub struct HashPartialOrdMarker;
    #[derive(PartialEq, Eq, Hash)]
    pub struct HashMarker;

    #[derive(PartialEq, Eq, PartialOrd)]
    pub struct EqPartialOrdMarker;
    #[derive(PartialEq, Eq)]
    pub struct EqMarker;

    #[derive(PartialOrd, Ord, PartialEq, Eq)]
    pub struct OrdMarker;
    #[derive(PartialOrd, PartialEq)]
    pub struct PartialOrdMarker;

    #[derive(PartialEq)]
    pub struct PartialEqMarker;
    pub struct NotPartialEqMarker;
}
