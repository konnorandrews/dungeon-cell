mod do_gen {
    use super::sealed::*;
    use super::IsBound;
    use crate::bound::{traits::*, Bound};
    pub(crate) use cfg_impl;
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, Unpin, __, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_sync")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, Sync, __, __, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, Unpin, __, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_send")]
    unsafe impl IsBound for Bound<Send, __, __, __, __, __> {
        type DebugMarker = NotDebugMarker;
        const SEND: bool = true;
        type SendMarker = SendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_unpin")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, Unpin, __, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_sync")]
    unsafe impl IsBound for Bound<__, Sync, __, __, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_unpin")]
    unsafe impl IsBound for Bound<__, __, Unpin, __, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Clone, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_clone")]
    unsafe impl IsBound for Bound<__, __, __, Copy, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Debug, PartialOrd> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Debug, Ord> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Debug, Eq<PartialOrd>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Debug, Eq<__>> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Debug, PartialEq> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Debug, __> {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Display, PartialOrd> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Display, Ord> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Display, Eq<PartialOrd>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Display, Eq<__>> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Display, PartialEq> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    #[cfg(feature = "bound_debug")]
    unsafe impl IsBound for Bound<__, __, __, __, Display, __> {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    unsafe impl IsBound for Bound<__, __, __, __, __, PartialOrd> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    unsafe impl IsBound for Bound<__, __, __, __, __, Ord> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    }
    #[cfg(feature = "bound_partial_ord")]
    unsafe impl IsBound for Bound<__, __, __, __, __, Eq<PartialOrd>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    unsafe impl IsBound for Bound<__, __, __, __, __, Eq<__>> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    }
    #[cfg(feature = "bound_partial_eq")]
    unsafe impl IsBound for Bound<__, __, __, __, __, PartialEq> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    }
    unsafe impl IsBound for Bound<__, __, __, __, __, __> {
        type DebugMarker = NotDebugMarker;
        type SendMarker = NotSendMarker;
        type SyncMarker = NotSyncMarker;
        type UnpinMarker = NotUnpinMarker;
        type CloneMarker = NotCloneMarker;
        type PartialEqMarker = NotPartialEqMarker;
    }
}
