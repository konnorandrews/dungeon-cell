use super::sealed::*;
use super::IsBound;
use crate::bound::{traits::*, Bound};

macro_rules! cfg_impl {
    ({$($a:tt)*} @send, __, $($t:tt)*) => {
        cfg_impl! { {$($a)*} $($t)* }
    };
    ({$($a:tt)*} @send, Send, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_send")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @sync, __, $($t:tt)*) => {
        cfg_impl! { {$($a)*} $($t)* }
    };
    ({$($a:tt)*} @sync, Sync, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_sync")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @unpin, __, $($t:tt)*) => {
        cfg_impl! { {$($a)*} $($t)* }
    };
    ({$($a:tt)*} @unpin, Unpin, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_unpin")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @clone, __, $($t:tt)*) => {
        cfg_impl! { {$($a)*} $($t)* }
    };
    ({$($a:tt)*} @clone, Copy, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_clone")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @clone, Clone, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_clone")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @debug, __, $($t:tt)*) => {
        cfg_impl! { {$($a)*} $($t)* }
    };
    ({$($a:tt)*} @debug, Display, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_debug")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @debug, Debug, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_debug")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, __, $($t:tt)*) => {
        cfg_impl! { {$($a)*} $($t)* }
    };
    ({$($a:tt)*} @partial_eq, PartialEq, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_partial_eq")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, Eq, __, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_partial_eq")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, Eq, PartialOrd, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_partial_ord")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, PartialOrd, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_partial_ord")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, Ord, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_partial_ord")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, Hash, __, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(feature = "bound_partial_hash")]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, Hash, PartialOrd, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(all(feature = "bound_partial_hash", feature = "bound_partial_ord"))]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*} @partial_eq, Hash, Ord, $($t:tt)*) => {
        cfg_impl! { {
            #[cfg(all(feature = "bound_partial_hash", feature = "bound_partial_ord"))]
            $($a)*
        } $($t)* }
    };
    ({$($a:tt)*}) => {$($a)*};
}
pub(crate) use cfg_impl;

macro_rules! bound_impl {
    (@send, __) => {
        type SendMarker = NotSendMarker;
    };
    (@sync, __) => {
        type SyncMarker = NotSyncMarker;
    };
    (@unpin, __) => {
        type UnpinMarker = NotUnpinMarker;
    };
    (@clone, __) => {
        type CloneMarker = NotCloneMarker;
    };
    (@debug, __) => {
        type DebugMarker = NotDebugMarker;
    };
    (@partial_eq, __) => {
        type PartialEqMarker = NotPartialEqMarker;
    };
    (@send, Send) => {
        const SEND: bool = true;
        type SendMarker = SendMarker;
    };
    (@sync, Sync) => {
        const SYNC: bool = true;
        type SyncMarker = SyncMarker;
    };
    (@unpin, Unpin) => {
        const UNPIN: bool = true;
        type UnpinMarker = UnpinMarker;
    };
    (@clone, Copy) => {
        const COPY: bool = true;
        const CLONE: bool = true;
        type CloneMarker = CopyMarker;
    };
    (@clone, Clone) => {
        const CLONE: bool = true;
        type CloneMarker = CloneMarker;
    };
    (@debug, Display) => {
        const DISPLAY: bool = true;
        const DEBUG: bool = true;
        type DebugMarker = DisplayMarker;
    };
    (@debug, Debug) => {
        const DEBUG: bool = true;
        type DebugMarker = DebugMarker;
    };

    (@partial_eq, PartialEq) => {
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = PartialEqMarker;
    };

    (@partial_eq, Eq, __) => {
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        type PartialEqMarker = EqMarker;
    };
    (@partial_eq, Eq, PartialOrd) => {
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = EqPartialOrdMarker;
    };

    (@partial_eq, Hash, __) => {
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const HASH: bool = true;
        type PartialEqMarker = HashMarker;
    };
    (@partial_eq, Hash, Ord) => {
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        const HASH: bool = true;
        type PartialEqMarker = HashOrdMarker;
    };
    (@partial_eq, Hash, PartialOrd) => {
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const HASH: bool = true;
        type PartialEqMarker = HashPartialOrdMarker;
    };

    (@partial_eq, Ord) => {
        const EQ: bool = true;
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        const ORD: bool = true;
        type PartialEqMarker = OrdMarker;
    };
    (@partial_eq, PartialOrd) => {
        const PARTIAL_EQ: bool = true;
        const PARTIAL_ORD: bool = true;
        type PartialEqMarker = PartialOrdMarker;
    };
}

macro_rules! powerset {
    (
        [$send:ident, $sync:ident, $unpin:ident, $clone:ident, $debug:ident, $partial_eq:ident $(($extra:ident))?]
        |
    ) => {
        cfg_impl! {
            // {
            //     impl Sealed for Bound<
            //       traits::$send, traits::$sync, traits::$unpin, traits::$clone, traits::$debug, traits::$partial_eq$(<traits::$extra>)?
            //     > {
            //     }
            // }

            {
                // SAFETY: Correctly marks the type as a bound.
                unsafe impl IsBound for Bound<
                  $send, $sync, $unpin, $clone, $debug, $partial_eq$(<$extra>)?
                > {
                    bound_impl!(@debug, $debug);
                    bound_impl!(@send, $send);
                    bound_impl!(@sync, $sync);
                    bound_impl!(@unpin, $unpin);
                    bound_impl!(@clone, $clone);
                    bound_impl!(@partial_eq, $partial_eq $(, $extra)?);
                }
            }

            @send, $send,
            @sync, $sync,
            @unpin, $unpin,
            @clone, $clone,
            @debug, $debug,
            @partial_eq, $partial_eq, $($extra, )?
        }
    };
    (
        [$($bounds:ident $(($($extras:ident)*))?),*]
        |
        []
        $($t:tt)*
    ) => {
        // There are no more bounds so do nothing.
    };
    (
        [$($bounds:ident $(($($extras:ident),*))?),*]
        |
        [
            $bound:ident $(($($extra:ident),*))?
            $(, $next_bound:ident $(($($next_extra:ident),*))?)*
        ]
        $(, [$($next_bounds:ident $(($($next_extras:ident),*))?),+])*
    ) => {
        // Move to next bound of the group.
        powerset!(
            [$($bounds $(($($extras)*))?),*]
            |
            [
                $($next_bound $(($($next_extra),*))?),*
            ]
            $(, [$($next_bounds $(($($next_extras),*))?),+])*
        );

        // With the trait bounded.
        powerset!(
            [$($bounds $(($($extras)*))?,)* $bound $(($($extra)*))?]
            |
            $([$($next_bounds $(($($next_extras),*))?),+]),*
        );
    };
}

powerset![
    [] | [__, Send],
    [__, Sync],
    [__, Unpin],
    [__, Copy, Clone],
    [__, Display, Debug],
    [
        __,
        PartialEq,
        Eq(__),
        Eq(PartialOrd),
        Hash(__),
        Hash(PartialOrd),
        Hash(Ord),
        Ord,
        PartialOrd
    ]
];
