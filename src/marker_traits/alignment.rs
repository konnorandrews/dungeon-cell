use core::panic::{RefUnwindSafe, UnwindSafe};

use super::sealed::Sealed;

/// Marker only implemented for [`Alignment`][crate::layout::Alignment].
///
/// Alignment values with a power of two from 1 up to 2²⁹ are allowed as
/// given by the [rust reference](https://doc.rust-lang.org/reference/type-layout.html#the-alignment-modifiers).
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented by external types.
pub trait IsAlignment:
    Sealed
    + Sized
    + Copy
    + Clone
    + Sync
    + Send
    + Unpin
    + RefUnwindSafe
    + UnwindSafe
    + 'static
{
    /// Type of alignment marker.
    type Marker: Send
        + Sync
        + Unpin
        + Clone
        + Copy
        + RefUnwindSafe
        + UnwindSafe
        + 'static;

    /// Instance of the associated [`Self::Marker`].
    const MARKER_INSTANCE: Self::Marker;

    /// Alignment of the associated [`Self::Marker`].
    const VALUE: usize;
}

pub(crate) mod sealed {
    use super::Sealed;
    use crate::layout::Alignment;

    macro_rules! impl_alignment_marker {
        ($(($power:literal, $num:literal, $name:ident)),*) => {
            $(
                /// ZST to with given alignment.
                #[repr(align($num))]
                #[derive(Clone, Copy)]
                pub struct $name;

                // Implement the public version of the trait.
                impl super::IsAlignment for Alignment<$num> {
                    const VALUE: usize = $num;

                    type Marker = $name;

                    const MARKER_INSTANCE: $name = $name;
                }

                impl Sealed for Alignment<$num> {}
            )*
        }
    }

    // Implement `ValidAlignment` for all valid alignments.
    //
    // "The alignment value must be a power of two from 1 up to 2^29"
    // https://doc.rust-lang.org/reference/type-layout.html#the-alignment-modifiers
    impl_alignment_marker![
        (0, 1, Marker0),
        (1, 2, Marker1),
        (2, 4, Marker2),
        (3, 8, Marker3),
        (4, 16, Marker4),
        (5, 32, Marker5),
        (6, 64, Marker6),
        (7, 128, Marker7),
        (8, 256, Marker8),
        (9, 512, Marker9),
        (10, 1024, Marker10),
        (11, 2048, Marker11),
        (12, 4096, Marker12),
        (13, 8192, Marker13),
        (14, 16384, Marker14),
        (15, 32768, Marker15),
        (16, 65536, Marker16),
        (17, 131072, Marker17),
        (18, 262144, Marker18),
        (19, 524288, Marker19),
        (20, 1048576, Marker20),
        (21, 2097152, Marker21),
        (22, 4194304, Marker22),
        (23, 8388608, Marker23),
        (24, 16777216, Marker24),
        (25, 33554432, Marker25),
        (26, 67108864, Marker26),
        (27, 134217728, Marker27),
        (28, 268435456, Marker28),
        (29, 536870912, Marker29)
    ];
}
