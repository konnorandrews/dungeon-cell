use core::mem::MaybeUninit;
use core::panic::{RefUnwindSafe, UnwindSafe};

use super::sealed::Sealed;
use crate::layout::Size;

/// Marker only implemented for [`Size`].
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented by external types.
pub trait IsSize:
    Sealed
    + Sized
    + Copy
    + Clone
    + Sync
    + Send
    + Unpin
    + RefUnwindSafe
    + UnwindSafe
    + 'static
{
    /// Type of the buffer with a size equal to [`Self::VALUE`].
    ///
    /// This type is always `[MaybeUninit<u8>; Self::VALUE]`.
    type Buffer: Send
        + Sync
        + Unpin
        + Clone
        + Copy
        + RefUnwindSafe
        + UnwindSafe
        + 'static;

    /// An uninitialized instance of the associated [`Self::Buffer`].
    const UNINIT_BUFFER: Self::Buffer;

    /// Size of the associated [`Self::Buffer`].
    const VALUE: usize;
}

impl<const N: usize> Sealed for Size<N> {}

impl<const N: usize> IsSize for Size<N> {
    type Buffer = [MaybeUninit<u8>; N];

    const UNINIT_BUFFER: Self::Buffer = [MaybeUninit::<u8>::uninit(); N];

    const VALUE: usize = N;
}
