use core::panic::{RefUnwindSafe, UnwindSafe};

use crate::layout::Layout;

use super::sealed::Sealed;

use super::{IsAlignment, IsSize};

/// Marker only implemented for [`Layout`][crate::layout::Layout].
///
/// This trait is [sealed](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed)
/// and cannot be implemented by external types.
pub trait IsLayout:
    Sealed
    + Sized
    + Sync
    + Send
    + Copy
    + Clone
    + Unpin
    + RefUnwindSafe
    + UnwindSafe
    + 'static
{
    /// Size of the layout.
    type Size: IsSize;

    /// Alignment requirement of the layout;
    type Alignment: IsAlignment;
}

impl<Size: IsSize, Alignment: IsAlignment> Sealed for Layout<Size, Alignment> {}
