//! Compile time utilities.
//!
//! # Compile Time Assertions
//!
//! By default, [`dungeon_cell`][crate] will generate compile time panics if
//! an assertion made with this module fails. These panics will happen when rustc
//! attempts to instantiate associated const values. This **does not** happen when
//! running `cargo check`. They will only happen when actually fully building a project.
//! This limitation is because generics can't be used in normal const expressions
//! (see the [tracking issue](https://github.com/rust-lang/rust/issues/76560) for `generic_const_exprs`).
//!
//! The resulting compiler errors don't have any information on where the
//! failing assert is located. The environment variable
//! `DUNGEON_CELL_RUNTIME_CHECKS` can be enabled (it can have any value) while
//! building [`dungeon_cell`][crate] to have asserts made with this module generate runtime
//! panics with a backtrace instead.
//!
//! Example of enabling the environment variable using a `.config/cargo.toml`.
//! ```toml
//! [env]
//! DUNGEON_CELL_RUNTIME_CHECKS = "true"
//! ```
//!
//! ### Example Assertion Failure
//! ```compile_fail
//! use dungeon_cell::compile_time::{SmallerOrEqualTo, UsizeAssert};
//! use dungeon_cell::Size;
//!
//! SmallerOrEqualTo::<u16, Size<1>>::assert();
//! ```
//! Output:
//! ```text
//! error[E0080]: evaluation of `<compile_time::SmallerOrEqualTo<u16, layout::Size<1>> as compile_time::UsizeAssert>::FAILED` failed
//!   --> src/compile_time.rs:40:26
//!    |
//! 40 |     const FAILED: bool = do_usize_const_assert::<Self>(PanicMode::Monomorphized);
//!    |                          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ the evaluated program panicked at '
//!
//! assertion failed: size of type T <= 1, type T has size = 2.
//!
//! This error happens after monomorphization so the location of the code that caused
//! this to happen is not known. Add the `DUNGEON_CELL_RUNTIME_CHECKS` environment
//! variable when compiling to enable runtime panics with a backtrace.
//!
//! ', src/compile_time.rs:40:26
//! ```

mod transmute;

pub use transmute::const_transmute;

use core::marker::PhantomData;

use crate::marker_traits::{IsAlignment, IsSize};

/// Compile time evaluable assert for [`usize`] values.
///
/// See [module docs](self#compile-time-assertions) for what happens when an assertion fails.
///
/// # Safety
/// [`Self::assert()`] and [`Self::FAILED`] must keep the default implementation.
pub unsafe trait UsizeAssert: Sized {
    /// Type with the property being checked.
    type Type;

    /// Associated constant used to trigger compile time errors.
    ///
    /// By making rustc evaluate this constant, you can trigger a compile time error.
    ///
    /// See [module docs](self#compile-time-assertions) for details.
    const FAILED: bool =
        do_usize_const_assert::<Self>(PanicMode::Monomorphized);

    /// Left hand side of the comparison.
    ///
    /// This is the value the type has.
    const LHS: usize;

    /// Right hand side of the comparison.
    ///
    /// This is the value to check against.
    const RHS: usize;

    /// Type of comparison to make between [`Self::LHS`] and [`Self::RHS`].
    const COMPARE: Comparison;

    /// Same of the property being checked.
    ///
    /// This is printed in the failure message.
    const PROPERTY: &'static str;

    /// Assert the defined comparison is true.
    ///
    /// # Panics
    /// This function will panic if the `DUNGEON_CELL_RUNTIME_CHECKS` environment variable is
    /// set and the assertion fails.
    /// Otherwise, this function is a no-op for the program.
    ///
    /// See [module docs](self#compile-time-assertions) for details.
    #[track_caller]
    fn assert() {
        #[cfg(use_const_assert)]
        let failed = Self::FAILED;

        #[cfg(not(use_const_assert))]
        let failed = do_assert::<Self>();

        if failed {
            panic!("assert failed");
        }
    }
}

/// Type of comparison.
#[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Hash, Eq, Ord)]
pub enum Comparison {
    /// `a < b`
    LessThan,

    /// `a <= b`
    LessThanEqual,

    /// `a == b`
    Equal,

    /// `a >= b`
    GreaterThanEqual,

    /// `a > b`
    GreaterThan,

    /// `a != b`
    NotEqual,
}

/// Const form of [`UsizeAssert::assert()`].
///
/// Prefer using [`UsizeAssert::assert()`] when possible because it can give better error
/// messages.
///
/// # Panics
/// This function will panic if the `DUNGEON_CELL_RUNTIME_CHECKS` environment variable is
/// set and the assertion fails.
/// Otherwise, this function is a no-op for the program.
///
/// See [module docs](self#compile-time-assertions) for details.
#[track_caller]
pub const fn const_usize_assert<T: UsizeAssert>() {
    #[cfg(use_const_assert)]
    let failed = T::FAILED;

    #[cfg(not(use_const_assert))]
    let failed = do_usize_const_assert::<T>(PanicMode::Normal);

    if failed {
        panic!("assert failed");
    }
}

/// Assert size of `T` is smaller or equal to given size.
///
/// # Examples
/// ```
/// use dungeon_cell::compile_time::{SizeSmallerOrEqualTo, UsizeAssert};
/// use dungeon_cell::layout::Size;
///
/// // this will pass
/// SizeSmallerOrEqualTo::<u16, Size<4>>::assert();
/// ```
/// ```compile_fail
/// use dungeon_cell::compile_time::{SizeSmallerOrEqualTo, UsizeAssert};
/// use dungeon_cell::Size;
///
/// // this won't pass
/// SizeSmallerOrEqualTo::<u16, Size<1>>::assert();
/// ```
pub struct SizeSmallerOrEqualTo<T, Size>(PhantomData<T>, PhantomData<Size>);

// SAFETY: We don't override the default impls.
unsafe impl<T, Size: IsSize> UsizeAssert for SizeSmallerOrEqualTo<T, Size> {
    const LHS: usize = core::mem::size_of::<T>();

    const RHS: usize = Size::VALUE;

    const COMPARE: Comparison = Comparison::LessThanEqual;

    type Type = T;

    const PROPERTY: &'static str = "size";
}

pub struct AlignmentSmallerOrEqualTo<T, Alignment>(
    PhantomData<T>,
    PhantomData<Alignment>,
);

// SAFETY: We don't override the default impls.
unsafe impl<T, Alignment: IsAlignment> UsizeAssert
    for AlignmentSmallerOrEqualTo<T, Alignment>
{
    const LHS: usize = core::mem::align_of::<T>();

    const RHS: usize = Alignment::VALUE;

    const COMPARE: Comparison = Comparison::LessThanEqual;

    type Type = T;

    const PROPERTY: &'static str = "alignment";
}

#[track_caller]
const fn do_usize_const_assert<T: UsizeAssert>(mode: PanicMode) -> bool {
    let (passed, symbol) = {
        let a = T::LHS;
        let b = T::RHS;

        match T::COMPARE {
            Comparison::LessThan => (a < b, " < "),
            Comparison::LessThanEqual => (a <= b, " <= "),
            Comparison::Equal => (a == b, " == "),
            Comparison::GreaterThanEqual => (a >= b, " >= "),
            Comparison::GreaterThan => (a > b, " > "),
            Comparison::NotEqual => (a != b, " != "),
        }
    };

    if passed {
        false
    } else {
        #[cfg(has_const_type_name)]
        let type_name = core::any::type_name::<T::Type>();

        #[cfg(not(has_const_type_name))]
        let type_name = "type T";

        let property_name = T::PROPERTY;
        let value = T::RHS;
        let type_value = T::LHS;

        // assertion failed: size of T <= 1. T has size 4.
        match mode {
            PanicMode::Normal => {
                const_panic::concat_panic!(
                    "assertion failed: ",
                    display: property_name,
                    " of ",
                    display: type_name,
                    display: symbol,
                    value,
                    ", ",
                    display: type_name,
                    " has ",
                    display: property_name,
                    " = ",
                    type_value,
                    "."
                );
            }
            PanicMode::Monomorphized => {
                const_panic::concat_panic!(
                    "\n\nassertion failed: ",
                    display: property_name,
                    " of ",
                    display: type_name,
                    display: symbol,
                    value,
                    ", ",
                    display: type_name,
                    " has ",
                    display: property_name,
                    " = ",
                    type_value,
                    ".\n\nThis error happens after monomorphization so \
                    the location of the code that caused this to happen is not known. \
                    Add the `DUNGEON_CELL_RUNTIME_CHECKS` environment variable \
                    when compiling to enable runtime panics with a backtrace.\n\n"
                );
            }
        }
    }
}

#[track_caller]
#[cfg(not(use_const_assert))]
fn do_assert<T: UsizeAssert>() -> bool {
    let (passed, symbol) = {
        let a = T::LHS;
        let b = T::RHS;

        match T::COMPARE {
            Comparison::LessThan => (a < b, " < "),
            Comparison::LessThanEqual => (a <= b, " <= "),
            Comparison::Equal => (a == b, " == "),
            Comparison::GreaterThanEqual => (a >= b, " >= "),
            Comparison::GreaterThan => (a > b, " > "),
            Comparison::NotEqual => (a != b, " != "),
        }
    };

    if passed {
        false
    } else {
        let type_name = core::any::type_name::<T::Type>();
        let property_name = T::PROPERTY;
        let value = T::RHS;
        let type_value = T::LHS;

        panic!(
            "assertion failed: {property_name} of \
            {type_name}{symbol}{value}. {type_name} \
            has {property_name} = {type_value}.",
        );
    }
}

#[allow(dead_code)]
enum PanicMode {
    Normal,
    Monomorphized,
}
