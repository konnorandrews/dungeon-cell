use core::ops::{Deref, DerefMut};

use crate::bound::bounds;
use crate::marker_traits::{IsBound, IsLayout, IsMut};
use crate::vtable::cleanup::Cleanup;
use crate::vtable::{
    BasicVTable, Coerce, CoerceMut, CoercionFor, CoercionVTable, DefaultVtable,
    Static, VTable,
};
use crate::DungeonCore;

mod auto;

#[cfg(feature = "unsize")]
pub use auto::unsize::Unsize;

pub trait AutoCoerce<'a, U: ?Sized + 'a, M: 'a, V: 'a>: Sized {
    const AUTO: CoercionFor<Self, &'a CoercionVTable<'a, U, M, V>>;
}

pub struct Mut<const F: bool> {
    _private: (),
}

pub type DefaultUnsizeVtable<'a, U, M, V> = &'a CoercionVTable<'a, U, M, V>;

// Owned container that dereferences into a possibly dynamically sized type.
//
// The generic `D` can be a dynamically sized type.
// #[repr(transparent)]
// pub struct DungeonUnsized<
//     U: ?Sized,
//     L,
//     V = DefaultUnsizeVtable<'static, U, Mut<true>, BasicVTable<bounds::Normal, Static>>,
// > where
//     L: IsLayout + Cleanup<V>,
//     V: VTable,
// {
//     core: DungeonCore<L, V>,
// }

// impl<'a, U: ?Sized, L, V: VTable> DungeonUnsized<U, L, V>
// where
//     L: IsLayout + Cleanup<&'a CoercionVTable<'a, U, V>>,
// {
//     pub const fn new<T: 'a>(value: T) -> Self
//     where
//         T: AutoCoerce<'a, U, M, V>,
//     {
//         let core =
//             unsafe { DungeonCore::new_unchecked(value, T::AUTO.vtable()) };
//
//         DungeonUnsized { core }
//     }
//
//     pub const fn new_with_coercion<T: 'a>(
//         value: T,
//         coerce: &'_ CoercionFor<'a, T, U, M, V>,
//     ) -> Self {
//         let core =
//             unsafe { DungeonCore::new_unchecked(value, coerce.vtable()) };
//
//         DungeonUnsized { core }
//     }
//
//     pub fn set_with_coercion<T: 'a>(
//         &mut self,
//         value: T,
//         coerce: &'_ CoercionFor<'a, T, U, M, V>,
//     ) {
//         unsafe {
//             self.core.store_unchecked(value, coerce.vtable());
//         }
//     }
//
//     pub fn set<T: 'a>(&mut self, value: T)
//     where
//         T: AutoCoerce<'a, U, M, V>,
//     {
//         unsafe {
//             self.core.store_unchecked(value, T::AUTO.vtable());
//         }
//     }
// }

impl<L, V> Deref for DungeonCore<L, V>
where
    L: IsLayout + Cleanup<V>,
    V: VTable + Coerce,
{
    type Target = V::Target;

    #[inline]
    fn deref(&self) -> &Self::Target {
        let (buffer, vtable) =
            unsafe { DungeonCore::raw_internals(self) };

        unsafe { &*(*vtable).coerce(buffer) }
    }
}

// impl<'a, U: ?Sized, L, V: VTable> DerefMut
//     for DungeonUnsized<'a, U, L, Mut<true>, V>
// where
//     L: IsLayout + Cleanup<&'a CoercionVTable<'a, U, Mut<true>, V>>,
// {
//     fn deref_mut(&mut self) -> &mut Self::Target {
//         let (buffer, vtable) =
//             unsafe { DungeonCore::raw_internals_mut(&mut self.core) };
//         unsafe { &mut *(*vtable).coerce_mut(buffer) }
//     }
// }
//
