use core::mem::ManuallyDrop;
use core::ptr::{addr_of, addr_of_mut, read_unaligned, write_unaligned};

use crate::vtable::{ConstVTableOf, Inspect, VTable, VTableOf};

#[repr(C, packed)]
pub struct Prison<T> {
    value: T,
}

impl<T> Prison<T> {
    #[inline]
    pub const fn new(value: T) -> Self {
        Self { value }
    }

    #[inline]
    pub fn into_inner(self) -> T {
        self.value
    }

    #[inline]
    pub fn replace(&mut self, value: T) -> T {
        let old = unsafe { read_unaligned(addr_of!(self.value)) };

        unsafe { write_unaligned(addr_of_mut!(self.value), value) };

        old
    }
}

impl<T: Copy> Copy for Prison<T> {}

impl<T: Copy> Clone for Prison<T> {
    #[inline]
    fn clone(&self) -> Self {
        *self
    }
}

unsafe impl<T: Copy + VTable> VTable for Prison<T> {
    type Id = T::Id;

    fn type_name(&self) -> &'static str {
        self.into_inner().type_name()
    }

    fn size(&self) -> usize {
        self.into_inner().size()
    }

    fn alignment(&self) -> usize {
        self.into_inner().alignment()
    }

    unsafe fn clone(
        &self,
        buffer: *const core::mem::MaybeUninit<u8>,
        other_buffer: *mut core::mem::MaybeUninit<u8>,
    ) {
        unsafe {
            <T as VTable>::clone(&self.into_inner(), buffer, other_buffer)
        };
    }

    unsafe fn drop_in_place(&self, buffer: *mut core::mem::MaybeUninit<u8>) {
        unsafe { <T as VTable>::drop_in_place(&self.into_inner(), buffer) };
    }

    fn id(&self) -> Self::Id {
        self.into_inner().id()
    }

    fn bounds(&self) -> crate::vtable::DynamicBounds {
        self.into_inner().bounds()
    }
}

unsafe impl<T: Copy, U, B> VTableOf<U, B> for Prison<T>
where
    T: VTableOf<U, B>,
{
    fn instance() -> Self {
        Prison::new(T::instance())
    }
}

unsafe impl<'a, T: Copy, U, B> ConstVTableOf<U, B> for Prison<T>
where
    T: ConstVTableOf<U, B>,
{
    const INSTANCE: Self = Prison::new(T::INSTANCE);
}
