use std::{any::TypeId, cell::UnsafeCell};

use crate::{
    can_store,
    vtable::{CoreVTable, VTableOf},
    Alignment, DungeonCore, IsSend, Size, ValidAlignment, ValidIsSend,
    ValidSize,
};

type CellVtable = &'static CoreVTable;

#[repr(transparent)]
pub struct DungeonCell<S = Size<0>, A = Alignment<1>, Z = IsSend<false>>
where
    S: ValidSize,
    A: ValidAlignment,
    Z: ValidIsSend,
{
    core: UnsafeCell<DungeonCore<S, A, Z, CellVtable>>,
}

impl<S, A> DungeonCell<S, A, IsSend<true>>
where
    S: ValidSize,
    A: ValidAlignment,
{
    pub const fn new<T: Send + 'static>(value: T) -> Self {
        DungeonCell {
            core: UnsafeCell::new(
                DungeonCore::<_, _, IsSend<true>>::const_from(value),
            ),
        }
    }

    pub fn set<T: Send + 'static>(&self, value: T) {
        let this = unsafe { &mut *self.core.get() };
        this.store(value);
    }

    pub fn swap(&self, other: &Self) {
        if core::ptr::eq(self, other) {
            return;
        }

        let this = unsafe { &mut *self.core.get() };
        let other = unsafe { &mut *other.core.get() };

        unsafe { core::ptr::swap(this, other) }
    }

    pub fn replace<T: Send + 'static, U: 'static>(
        &self,
        value: T,
    ) -> Result<U, T> {
        can_store::assert_can_store::<T, S, A>();

        let this = unsafe { &mut *self.core.get() };
        if this.is_type::<U>() {
            let old_value = unsafe { this.take_unchecked::<U>() };
            unsafe { this.store_unchecked(value, CellVtable::of::<T>()) };
            Ok(old_value)
        } else {
            Err(value)
        }
    }
}

impl<S, A> DungeonCell<S, A, IsSend<false>>
where
    S: ValidSize,
    A: ValidAlignment,
{
    pub const fn new<T: 'static>(value: T) -> Self {
        DungeonCell {
            core: UnsafeCell::new(
                DungeonCore::<_, _, IsSend<false>>::const_from(value),
            ),
        }
    }

    pub fn set<T: 'static>(&self, value: T) {
        let this = unsafe { &mut *self.core.get() };
        this.store(value);
    }

    pub fn swap(&self, other: &Self) {
        if core::ptr::eq(self, other) {
            return;
        }

        let this = unsafe { &mut *self.core.get() };
        let other = unsafe { &mut *other.core.get() };

        unsafe { core::ptr::swap(this, other) }
    }

    pub fn replace<T: 'static, U: 'static>(&self, value: T) -> Result<U, T> {
        can_store::assert_can_store::<T, S, A>();

        let this = unsafe { &mut *self.core.get() };
        if this.is_type::<U>() {
            let old_value = unsafe { this.take_unchecked::<U>() };
            unsafe { this.store_unchecked(value, CellVtable::of::<T>()) };
            Ok(old_value)
        } else {
            Err(value)
        }
    }
}

impl<S, A, Z> DungeonCell<S, A, Z>
where
    S: ValidSize,
    A: ValidAlignment,
    Z: ValidIsSend,
{
    pub fn into_inner<T: 'static>(mut self) -> Result<T, Self> {
        self.core.get_mut().take::<T>().ok_or(self)
    }

    pub fn get<T: Copy + 'static>(&self) -> Option<T> {
        unsafe { &mut *self.core.get() }.borrow::<T>().copied()
    }

    pub fn get_mut<T: 'static>(&mut self) -> Option<&mut T> {
        self.core.get_mut().borrow_mut::<T>()
    }

    pub fn take<T: 'static>(&self) -> Option<T> {
        unsafe { &mut *self.core.get() }.take()
    }

    pub const fn as_ptr(&self) -> *mut DungeonCore<S, A, Z> {
        self.core.get()
    }

    pub fn is_type<T: 'static>(&self) -> bool {
        unsafe { &*self.core.get() }.is_type::<T>()
    }

    pub fn current_type_id(&self) -> TypeId {
        unsafe { &*self.core.get() }.current_type_id()
    }
}

impl<S, A, Z> Default for DungeonCell<S, A, Z>
where
    S: ValidSize,
    A: ValidAlignment,
    Z: ValidIsSend,
{
    fn default() -> Self {
        DungeonCell {
            core: UnsafeCell::new(DungeonCore::<_, _, _>::new()),
        }
    }
}
