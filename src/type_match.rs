//! Pattern match by type.
//!
//! This module provides the [`TypeMatch`] trait. This trait allows a
//! value to be used in the [`by_type!()`][crate::by_type] macro to pattern
//! match by types.

use core::mem::ManuallyDrop;

use crate::{vtable::{UniqueId, cleanup::Cleanup, VTable}, marker_traits::IsLayout, DungeonCore};

/// Trait for pattern matching by type.
///
/// This trait is implemented by types that act like they store multiple type's values.
///
/// # Safety
/// If [`Self::is_type()`] returns `true` then immediately calling 
/// [`Self::extract_unchecked()`] must be safe to do.
pub unsafe trait TypeMatch<T>: Sized {
    /// Check if a value of type `T` can be extracted from the value.
    ///
    /// This acts like a pattern match but between types.
    fn is_type(this: &Self) -> bool;

    /// Extract the value without checking the type.
    ///
    /// # Safety
    /// [`Self::is_type()`] must have returned `true` for the same `Self`.
    unsafe fn extract_unchecked(this: Self) -> T;

    /// Extract value.
    fn extract(this: Self) -> Option<T> {
        if Self::is_type(&this) {
            // SAFETY: We just checked that is_type returns true.
            unsafe { Some(Self::extract_unchecked(this)) }
        } else {
            None
        }
    }
}

// SAFETY: Self can be made into a T when is_type is true.
unsafe impl<T: UniqueId<V::Id>, L: IsLayout + Cleanup<V>, V: VTable> TypeMatch<T>
    for DungeonCore<L, V>
{
    fn is_type(this: &Self) -> bool {
        this.type_descriptor().is_type::<T>()
    }

    unsafe fn extract_unchecked(this: Self) -> T {
        // SAFETY: is_type returned try for this function to be ran.
        let (value, vtable) = unsafe { this.into_unchecked::<T>() };

        // Drop the vtable.
        drop(ManuallyDrop::into_inner(vtable));

        // Return the owned value.
        ManuallyDrop::into_inner(value)
    }
}

// SAFETY: Self can be made into a T when is_type is true.
unsafe impl<'a, T: UniqueId<V::Id>, L: IsLayout + Cleanup<V>, V: VTable>
    TypeMatch<&'a T> for &'a DungeonCore<L, V>
{
    fn is_type(this: &Self) -> bool {
        this.type_descriptor().is_type::<T>()
    }

    unsafe fn extract_unchecked(this: Self) -> &'a T {
        // SAFETY: is_type returned try for this function to be ran.
        unsafe { this.borrow_unchecked() }
    }
}

// SAFETY: Self can be made into a T when is_type is true.
unsafe impl<'a, T: UniqueId<V::Id>, L: IsLayout + Cleanup<V>, V: VTable>
    TypeMatch<&'a mut T> for &'a mut DungeonCore<L, V>
{
    fn is_type(this: &Self) -> bool {
        this.type_descriptor().is_type::<T>()
    }

    unsafe fn extract_unchecked(this: Self) -> &'a mut T {
        // SAFETY: is_type returned try for this function to be ran.
        unsafe { this.borrow_mut_unchecked() }
    }
}

/// Create match statement that matches by type.
///
/// The macro takes a input of the following form.
/// ```text
/// by_type!(match <some expression> {
///     <variable name> @ <type> => {
///         <code>
///     }
///     <other variable name> @ <other type> => {
///         <other code>
///     }
///     <fallback variable name> => {}
/// })
/// ```
/// The fallback arm must always exist and it must have a variable name given to it.
/// Because of macro_rules limitations you cant use `_`, but you can use `__` to ignore it.
/// Additionally, each arm must be a block. They cannot be just an expression.
///
/// The arm with the type matching the input expression's type will be ran. The other
/// arms will not be executed. Just like in a normal match.
///
/// # Example
/// ```
/// use dungeon_cell::{DungeonCore, layout_for, by_type};
///
/// let value = DungeonCore::<layout_for!(i32, f32)>::new(123);
///
/// // match on the DungeonCore's stored type
/// let int = by_type!(match value {
///     int @ i32 => { int }
///     __ @ String => { 0 }
///     __ => { 0 }
/// });
///
/// assert_eq!(int, 123);
/// ```
/// ```
/// use dungeon_cell::{DungeonCore, layout_for, by_type};
///
/// let mut value = DungeonCore::<layout_for!(i32, f64)>::new(123u8);
///
/// // match on DungeonCore's stored type but mutably
/// by_type!(match &mut value {
///     int @ &mut u8 => { *int += 100 }
///     float @ &mut f64 => { *float += 100.0 }
///     __ => { }
/// });
///
/// assert_eq!(value.take::<u8>().unwrap(), 223);
/// ```
#[macro_export]
macro_rules! by_type {
    (match $($t:tt)*) => {
        $crate::by_type!(
            @helper
            []
            $($t)*
        )
    };
    (@helper [$($e:tt)*] {$($t:tt)*}) => {
        $crate::by_type!(@body [$($e)*] $($t)*)
    };
    (@helper [$($e:tt)*] $t:tt $($tail:tt)*) => {
        $crate::by_type!(
            @helper
            [$($e)* $t]
            $($tail)*
        )
    };
    (@body [$($e:tt)*]
        $($var:ident $(@ $type:ty)? => $arm:block $(,)?)*
    ) => {
        match $($e)* {
            $($var $(if $crate::type_match::TypeMatch::<$type>::is_type(&$var))? => {
                $(
                    let $var = {
                        // SAFETY: The match guard checks that the type is correct.
                        unsafe { $crate::type_match::TypeMatch::<$type>::extract_unchecked($var) }
                    };
                )?
                $arm
            }),*
        }
    };
}
