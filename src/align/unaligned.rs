use core::ptr::{addr_of, addr_of_mut, read_unaligned, write_unaligned};

use crate::bound::Dynamic;
use crate::marker_traits::IsBound;
use crate::vtable::{ConstVTableOf, VTable, VTableOf, VTableSubset};

/// Force value to be unaligned.
///
/// Because all borrows must be properly aligned, this type doesn't give out
/// borrows to the inner type. However, if the inner type is [`Copy`] then the stored
/// value can be accessed using [`Self::into_inner`].
#[repr(C, packed)]
pub struct Unaligned<T> {
    value: T,
}

impl<T> Unaligned<T> {
    /// Create new unaligned value.
    ///
    /// The returned type will have an alignment of 1 byte.
    pub const fn new(value: T) -> Self {
        Self { value }
    }

    /// Convert into inner value.
    ///
    /// The returned value will have it's normal alignment.
    pub fn into_inner(self) -> T {
        self.value
    }

    /// Replace the currently stored value.
    pub fn replace(&mut self, value: T) -> T {
        // Read the value currently stored (acts as move out).
        // SAFETY: We know the pointer is valid because we have a &mut self.
        let old = unsafe { read_unaligned(self.as_ptr()) };

        // Write the new value (acts as move int).
        // SAFETY: We know the pointer is valid because we have a &mut self.
        unsafe { write_unaligned(self.as_mut_ptr(), value) };

        old
    }

    /// Get unaligned pointer to the inner value.
    ///
    /// The returned pointer cannot be made into a borrow or used
    /// with normal pointer functions unless the type `T` already has
    /// an alignment of 1 byte.
    pub fn as_ptr(&self) -> *const T {
        addr_of!(self.value)
    }

    /// Get unaligned mutable pointer to the inner value.
    ///
    /// The returned pointer cannot be made into a borrow or used
    /// with normal pointer functions unless the type `T` already has
    /// an alignment of 1 byte.
    pub fn as_mut_ptr(&mut self) -> *mut T {
        addr_of_mut!(self.value)
    }
}

impl<T: Copy> Copy for Unaligned<T> {}

impl<T: Copy> Clone for Unaligned<T> {
    #[inline]
    fn clone(&self) -> Self {
        *self
    }
}

// SAFETY: Forwards implementation to T.
unsafe impl<T: VTable> VTable for Unaligned<&T> {
    type Bounds = T::Bounds;

    type Id = T::Id;

    fn descriptor(&self) -> &crate::vtable::Descriptor<Self::Id> {
        T::descriptor(self.into_inner())
    }

    fn bound_impl(&self) -> &crate::vtable::BoundsImpl<Self::Bounds> {
        T::bound_impl(self.into_inner())
    }
}

// SAFETY: The type we describe doesnt change.
unsafe impl<'a, T: VTable, Bnew: IsBound> VTableSubset<Bnew>
    for Unaligned<&'a T>
where
    &'a T: VTableSubset<Bnew>,
    <&'a T as VTableSubset<Bnew>>::Subset: Copy,
    Unaligned<<&'a T as VTableSubset<Bnew>>::Subset>: VTable<Bounds = Bnew>,
{
    type Subset = Unaligned<<&'a T as VTableSubset<Bnew>>::Subset>;

    fn into_subset(self) -> Self::Subset {
        Unaligned::new(self.into_inner().into_subset())
    }
}

// SAFETY: Forwards implementation to T.
unsafe impl<'a, T: VTable, U: Dynamic<T::Bounds>> VTableOf<U>
    for Unaligned<&'a T>
where
    &'a T: VTableOf<U, Bounds = T::Bounds>,
{
    fn instance() -> Self {
        Unaligned::new(<&T>::instance())
    }
}

// SAFETY: Forwards implementation to T.
unsafe impl<'a, T: VTable, U: Dynamic<T::Bounds>> ConstVTableOf<U>
    for Unaligned<&'a T>
where
    &'a T: ConstVTableOf<U, Bounds = T::Bounds>,
{
    const INSTANCE: Self = Unaligned::new(<&T>::INSTANCE);
}
