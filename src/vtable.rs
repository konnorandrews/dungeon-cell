//! Virtual method tables.
//!
//! [`dungeon_cell`][crate] uses custom vtables to track what types are stored and
//! provide methods for those stored types.

use core::any::TypeId;
use ::core::cmp::Ordering;
use ::core::fmt;

mod coerce;
mod bound_impl;
pub mod cleanup;
mod basic_vtable;

use crate::align::Unaligned;
use crate::bound::{Dynamic, bounds};
use crate::layout::LayoutType;
use crate::lifetime_type_id::{LifetimeTypeId, StaticForm};
use crate::marker_traits::{IsBound, IsLayout};

pub use self::basic_vtable::*;
pub use bound_impl::BoundsImpl;
pub use coerce::*;

/// Unique ID for a type.
///
/// `T` will act as a type ID for `Self`.
///
/// # Safety
/// - The returned `T` for a `Self` type must always be the same value.
/// - The returned `T` for different `Self` types must always return not equal values.
///     A type is considered the same if its safe to transmute between them.
/// - `T` follows the rules of `Eq` equality.
pub unsafe trait UniqueId<T> {
    /// Get the unique `T` for `Self`.
    fn unique_id() -> T;
}

/// Static type ID.
///
/// Available for all `'static` types.
///
/// # Examples
/// ```
/// use dungeon_cell::vtable::{Static, UniqueId};
///
/// assert_ne!(
///     <i32 as UniqueId<Static>>::unique_id(),
///     <f32 as UniqueId<Static>>::unique_id()
/// );
/// ```
#[derive(Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Hash, Debug)]
pub struct Static(TypeId);

// SAFETY: TypeId is well behaved and we always return the one for T.
unsafe impl<T: 'static> UniqueId<Static> for T {
    fn unique_id() -> Static {
        Static(TypeId::of::<T>())
    }
}

/// Single lifetime type ID.
///
/// Available for all types implementing [`StaticForm`].
///
/// # Examples
/// ```
/// use dungeon_cell::vtable::{Lifetime, UniqueId};
///
/// assert_ne!(
///     <i32 as UniqueId<Lifetime<'static>>>::unique_id(),
///     <f32 as UniqueId<Lifetime<'static>>>::unique_id()
/// );
/// ```
#[derive(Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Hash, Debug)]
pub struct Lifetime<'a>(LifetimeTypeId<'a>);

// SAFETY: LifetimeTypeId is well behaved (see the lifetime_type_id module)
// and we always return the one for T.
unsafe impl<'a, T: StaticForm<'a>> UniqueId<Lifetime<'a>> for T {
    fn unique_id() -> Lifetime<'a> {
        Lifetime(LifetimeTypeId::of::<T>())
    }
}

/// The default vtable implementation.
///
/// This is a pointer to a [`StaticVTable`] where the pointer has had it's alignment
/// requirements removed. The alignment requirements are removed for better space efficiency when
/// used with layouts having a low alignment. On some platforms this may result in a lower
/// performance. In which case, you can switch to using a `&'static StaticVTable<B>` directly.
pub type DefaultVtable<'a, B = bounds::Normal, Id = Static> =
    Unaligned<&'a BasicVTable<B, Id>>;

/// Virtual method table and type descriptor.
///
/// Rust automatically generates vtables for trait objects. However, its not currently
/// possible to access these vtables directly on stable. Instead, this trait provides
/// a unified interface for custom vtables. Custom vtables can be generated thanks to
/// Rust's ability to static promote constants to give them stable addresses.
///
/// A vtable is created from a type [`Descriptor`] and a [`BoundsImpl`]. A `Descriptor`
/// describes a type, and includes things like the type's size and name. The
/// [`Descriptor::is_type()`] method can be used to check if a vtable is for a particular type. A
/// `BoundsImpl` contains the implementations of the virtual methods for the type.
/// Currently, only [`Drop`][std::ops::Drop], [`Clone`][std::clone::Clone], and
/// [`Debug`][std::fmt::Debug] are included in the virtual methods.
///
/// All [`dungeon_cell`][crate] types use this trait as the basis for their type erasure.
///
/// Use [`VTableOf`] or [`ConstVTableOf`] to construct a instance of a generic vtable for a type.
/// Additionally, use [`VTableSubset`] to reduce the number of traits bound by [`Self::Bounds`].
///
/// # Safety
/// - The type described by [`Self::descriptor()`] must be the same that [`Self::bound_impl`] was
/// created for. This means calling [`Descriptor::new::<T>()`][Descriptor::new] and
/// [`BoundsImpl::new::<T>()`][BoundsImpl::new] with the same `T`.
/// - The type [`Self::Id`] must be well behaved. This means that when performing equality
/// between two [`Self::Id`] if the result is `true` then the type `T` that [`Self::descriptor()`]
/// describes is the same type. Additionally, [`Inspect::<Self::Id>::inspect::<T>()`][Inspect::inspect] must return the
/// [`Self::Id`] for the type `T` following this equality.
/// - The type the vtable describes must implement the traits given by [`Self::Bounds`].
///     This means `T: Dynamic<Self::Bounds>` must be true.
pub unsafe trait VTable: Clone {
    /// The dynamic trait bounds applied.
    ///
    /// This may not match the trait bounds used when creating the vtable,
    /// but it will always be equal to or a subset of it.
    type Bounds: IsBound;

    /// Type to store the ID information for which type the vtable is for.
    ///
    /// ID types must always implement `Eq` to allow comparing them.
    type Id: Eq;

    /// Get the type descriptor.
    ///
    /// This is the descriptor for the type the vtable was made for.
    fn descriptor(&self) -> &Descriptor<Self::Id>;

    /// Get the bound implementation.
    ///
    /// This is the method implementation for the type the vtable was made for.
    fn bound_impl(&self) -> &BoundsImpl<Self::Bounds>;
}

/// Convert vtable into a vtable with less dynamic trait bounds.
///
/// # Safety
/// The type the vtable describes must not change.
pub unsafe trait VTableSubset<B>: VTable {
    /// The type of the new vtable.
    type Subset: VTable<Bounds = B>;

    /// Convert a vtable.
    fn into_subset(self) -> Self::Subset;
}

/// Describes a type.
///
/// The static properties of a type are exposed here.
///
/// Equality between [`Descriptor`] instances is based only on the `Id` type.
pub struct Descriptor<Id> {
    get_id: fn() -> Id,
    get_type_name: fn() -> &'static str,
    size: usize,
    alignment: usize,
}

impl<Id> Copy for Descriptor<Id> {}
impl<Id> Clone for Descriptor<Id> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<Id: Eq> Descriptor<Id> {
    /// Create a [`Descriptor`] for type `T`.
    ///
    /// This function can be used in const contexts.
    pub const fn new<T: UniqueId<Id>>() -> Self {
        // The id and type_name are done in a closure because they aren't available
        // in a const context.
        Self {
            get_id: || T::unique_id(),
            get_type_name: || ::core::any::type_name::<T>(),
            size: ::core::mem::size_of::<T>(),
            alignment: ::core::mem::align_of::<T>(),
        }
    }

    /// Check if the [`Descriptor`] is for the type `T`.
    ///
    /// If this method returns `true` then the descriptor was created for type `T`.
    pub fn is_type<T: UniqueId<Id>>(&self) -> bool {
        self.id() == T::unique_id()
    }

    /// Get the ID for the type.
    pub fn id(&self) -> Id {
        (self.get_id)()
    }

    /// Get the name of the type.
    ///
    /// This uses [`std::any::type_name()`] so should only be used for diagnostics.
    pub fn type_name(&self) -> &'static str {
        (self.get_type_name)()
    }

    /// Size of the type in bytes.
    pub const fn size(&self) -> usize {
        self.size
    }

    /// Alignment of the type in bytes.
    pub const fn alignment(&self) -> usize {
        self.alignment
    }

    /// Alignment of the type in bytes.
    pub const fn layout_can_store<L: IsLayout>(&self) -> bool {
        self.size <= LayoutType::<L>::size()
            && self.alignment <= LayoutType::<L>::alignment()
    }
}

impl<Id: Eq> fmt::Debug for Descriptor<Id> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Descriptor")
            .field("type_name", &self.type_name())
            .field("size", &self.size())
            .field("alignment", &self.alignment())
            .finish()
    }
}

impl<Id: Eq> PartialEq for Descriptor<Id> {
    fn eq(&self, other: &Self) -> bool {
        self.id() == other.id()
    }
}

impl<Id: Eq> Eq for Descriptor<Id> {}

impl<Id: Eq + PartialOrd> PartialOrd for Descriptor<Id> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.id().partial_cmp(&other.id())
    }
}

impl<Id: Eq + Ord> Ord for Descriptor<Id> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.id().cmp(&other.id())
    }
}

impl<Id: Eq + ::core::hash::Hash> ::core::hash::Hash for Descriptor<Id> {
    fn hash<H: ::core::hash::Hasher>(&self, state: &mut H) {
        self.id().hash(state);
    }
}

// Automatically implement for borrows of a vtable.
// This allows for borrows to static vtable instances to be valid.
//
// SAFETY: Forwards everything to the impl of `T`.
unsafe impl<T: VTable> VTable for &T {
    type Bounds = T::Bounds;

    type Id = T::Id;

    fn descriptor(&self) -> &Descriptor<Self::Id> {
        T::descriptor(self)
    }

    fn bound_impl(&self) -> &BoundsImpl<Self::Bounds> {
        T::bound_impl(self)
    }
}

/// Generate [`VTable`] instance for type.
///
/// The type `T` must satisfy the dynamic trait bounds given by `Self::Bounds`.
///
/// # Safety
/// - The vtable instance [`Self::instance()`] returns must be for the type `T`.
/// This means [`VTable::descriptor().is_type::<T>()`][Descriptor::is_type] must return `true`.
/// - The [`Self::instance()`] method must not panic.
pub unsafe trait VTableOf<T: Dynamic<Self::Bounds>>: VTable {
    /// Get an instance of the vtable for the type `T`.
    fn instance() -> Self;
}

/// Generate [`VTable`] instance for type in const context.
///
/// The type `T` must satisfy the dynamic trait bounds given by `Self::Bounds`.
///
/// This is the const form of [`VTableOf`]. Because traits can't have const methods
/// the instance is an associated constant.
///
/// # Safety
/// - The vtable instance [`Self::INSTANCE`] must be for the type `T`.
/// This means [`VTable::descriptor().is_type::<T>()`][Descriptor::is_type] must return `true`.
pub unsafe trait ConstVTableOf<T: Dynamic<Self::Bounds>>:
    VTableOf<T>
{
    /// Instance of the vtable for the type `T`.
    const INSTANCE: Self;
}
