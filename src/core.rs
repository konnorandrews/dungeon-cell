use core::mem::{ManuallyDrop, MaybeUninit};
use core::ptr::{addr_of, addr_of_mut};

use crate::bound::Dynamic;
use crate::buffer::Buffer;
use crate::compile_time::{
    const_transmute, const_usize_assert, AlignmentSmallerOrEqualTo,
};
use crate::layout::LayoutType;
use crate::marker_traits::{IsAlignment, IsBound, IsLayout};
use crate::vtable::cleanup::{
    into_storage, split_storage, storage_as_split_ptr,
    storage_as_split_ptr_mut, Cleanup,
};
use crate::vtable::{
    ConstVTableOf, Descriptor, UniqueId, VTable, VTableOf, VTableSubset, DefaultVtable, CoercionFor,
};

/// Core of a dungeon type.
///
/// [`DungeonCore`] can store any type that fits in
/// it's layout `L` and a vtable of type `V` can be generated for. This type does not perform
/// any dynamic memory allocation. It can be thought of as a single value arena.
///
/// [`DungeonCore`] always contains a value of some type.
///
/// The space overhead of [`DungeonCore`] is kept as small as possible. In most cases,
/// the space overhead of [`DungeonCore`] is the size of a single [`usize`]. The
/// space overhead is directly tied to the size of the `V` type.
///
/// [`DungeonCore`] is generic over the traits that the values it stores must
/// implement. The required traits are set by the `V` type. When using [`DefaultVtable`],
/// the default required traits are `Send + Sync + Unpin + Debug`.
///
/// # Examples
/// ```
/// use dungeon_cell::{DungeonCore, layout_for};
///
/// // create a DungeonCore with a layout required to store i32 or f64
/// let mut core = DungeonCore::<layout_for!(i32, f64)>::default();
///
/// // we can store a i32 and get it back
/// core.store(1234);
/// assert_eq!(core.take::<i32>(), Some(1234));
///
/// // we can store a f64 and get it back
/// core.store(1.234);
/// assert_eq!(core.take::<f64>(), Some(1.234));
///
/// // we can't take a type the core isn't storing
/// core.store(1234);
/// assert_eq!(core.take::<f64>(), None);
///
/// // we can borrow both unique and shared
/// core.store(1234);
/// *core.borrow_mut::<i32>().unwrap() += 10;
/// assert_eq!(core.borrow::<i32>(), Some(&1244));
/// ```
#[repr(transparent)]
pub struct DungeonCore<L, V = DefaultVtable<'static>>
where
    L: IsLayout + Cleanup<V>,
    V: VTable,
{
    /// Storage of the active value and vtable.
    inner: <L as Cleanup<V>>::Storage,
}

// SAFETY: If the vtable and the bound are Send then the stored type must be Send.
unsafe impl<L: IsLayout + Cleanup<V>, V: VTable> Send for DungeonCore<L, V>
where
    V: Send,
    <V::Bounds as IsBound>::SendMarker: Send,
{
}

// SAFETY: If the vtable and the bound are Sync then the stored type must be Sync.
unsafe impl<L: IsLayout + Cleanup<V>, V: VTable> Sync for DungeonCore<L, V>
where
    V: Sync,
    <V::Bounds as IsBound>::SyncMarker: Sync,
{
}

// If the vtable and the bound are Unpin then the stored type must be Unpin.
impl<L: IsLayout + Cleanup<V>, V: VTable> Unpin for DungeonCore<L, V>
where
    V: Unpin,
    <V::Bounds as IsBound>::UnpinMarker: Unpin,
{
}

// If the vtable and the bound are Copy then the stored type must be Copy.
impl<L: IsLayout + Cleanup<V>, V: VTable> Copy for DungeonCore<L, V>
where
    V: Copy,
    <L as Cleanup<V>>::Storage: Copy,
{
}

// If the vtable and the bound are Clone then the stored type must be Clone.
impl<L: IsLayout + Cleanup<V>, V: VTable> Clone for DungeonCore<L, V>
where
    V: Clone,
    <L as Cleanup<V>>::Storage: Clone,
{
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl<L: IsLayout + Cleanup<V>, V: VTable> Default for DungeonCore<L, V>
where
    V: VTableOf<()>,
    (): Dynamic<V::Bounds>,
{
    /// Create a [`DungeonCore`] storing the value `()`.
    #[track_caller]
    fn default() -> Self {
        // SAFETY: The vtable is for ().
        unsafe { Self::new_unchecked((), V::instance()) }
    }
}

impl<L: IsLayout + Cleanup<V>, V: VTable> DungeonCore<L, V> {
    /// Create a [`DungeonCore`] storing a value of type `T`.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let core = DungeonCore::<layout_for!(i32)>::new(123_i32);
    /// ```
    #[track_caller]
    pub const fn new<T: Dynamic<V::Bounds>>(value: T) -> Self
    where
        V: ConstVTableOf<T>,
    {
        // SAFETY: The vtable is for T.
        unsafe { Self::new_unchecked(value, V::INSTANCE) }
    }

    /// Store a value of type `T`.
    ///
    /// The currently stored value will be dropped.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let mut core = DungeonCore::<layout_for!(u8)>::default();
    ///
    /// core.store(4u8);
    ///
    /// assert_eq!(core.take::<u8>(), Some(4));
    /// ```
    #[track_caller]
    pub fn store<T: Dynamic<V::Bounds>>(&mut self, value: T)
    where
        V: VTableOf<T>,
    {
        // Assert the alignment is correct because we give out borrows.
        const_usize_assert::<AlignmentSmallerOrEqualTo<T, L::Alignment>>();

        // This asserts that the size is correct so we don't have to check.
        let buffer = Buffer::<L::Size>::new(value);

        // SAFETY: The buffer has a valid value because we just made it from value.
        // The vtable matches the value because of the safety requirements of the function.
        let inner = unsafe { into_storage::<L, V>(buffer, V::instance()) };

        // Replace the old inner with the new value.
        // We can now drop the value in the old_inner without causing a double drop.
        // By dropping Inner it will automatically run the drop code for the value
        // it is storing if it needs to.
        //
        // If the drop code panics then this DungeonCore has a new value to drop.
        let _ = ::core::mem::replace(&mut self.inner, inner);
    }

    /// Replace the value stored with a new one.
    ///
    /// If the currently stored value is of type `U`, then the `value` will be stored.
    /// If the [`DungeonCore`] is storing a value of another
    /// type, then a `Err` is returned with `value` and the stored value is unaffected.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let mut core = DungeonCore::<layout_for!(i32)>::new(123i32);
    ///
    /// assert_eq!(core.replace(4u8), Ok(123i32));
    /// assert_eq!(core.replace(()), Ok(4u8));
    /// assert_eq!(core.replace(456i32), Err::<u8, _>(456i32));
    /// ```
    pub fn replace<T: Dynamic<V::Bounds>, U: UniqueId<V::Id>>(
        &mut self,
        value: T,
    ) -> Result<U, T>
    where
        V: VTableOf<T>,
    {
        if self.type_descriptor().is_type::<U>() {
            let (old_value, _) = {
                // SAFETY: The dungeon_cell is storing a value of type U,
                // and the vtable is for the value.
                unsafe { self.replace_unchecked(value, V::instance()) }
            };

            Ok(old_value)
        } else {
            Err(value)
        }
    }

    /// Gain ownership of the value inside the [`DungeonCore`].
    ///
    /// This will leave the [`DungeonCore`] storing a `()` if the currently stored
    /// value is of type `T`. If the [`DungeonCore`] is storing a value of another
    /// type, then a `None` is returned and the stored value is unaffected.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let mut core = DungeonCore::<layout_for!(u8)>::new(4u8);
    ///
    /// assert_eq!(core.take::<u8>(), Some(4));
    /// assert_eq!(core.take::<u8>(), None);
    /// ```
    pub fn take<T: UniqueId<V::Id>>(&mut self) -> Option<T>
    where
        V: VTableOf<()>,
        (): Dynamic<V::Bounds>,
    {
        self.replace(()).ok()
    }

    /// Borrow the stored value.
    ///
    /// If the [`DungeonCore`] is storing a value of another type, then a `None` is returned.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let core = DungeonCore::<layout_for!(u8)>::new(4u8);
    ///
    /// assert_eq!(core.borrow::<u8>(), Some(&4));
    /// assert_eq!(core.borrow::<i8>(), None);
    /// ```
    pub fn borrow<T: UniqueId<V::Id>>(&self) -> Option<&T> {
        if self.type_descriptor().is_type::<T>() {
            // SAFETY:
            // We just checked that the current stored type is a `T`.
            Some(unsafe { self.borrow_unchecked::<T>() })
        } else {
            None
        }
    }

    /// Mutably borrow the stored value.
    ///
    /// If the [`DungeonCore`] is storing a value of another type, then a `None` is returned.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let mut core = DungeonCore::<layout_for!(u8)>::new(4u8);
    ///
    /// *core.borrow_mut::<u8>().unwrap() += 2;
    ///
    /// assert_eq!(core.take::<u8>(), Some(6));
    ///
    /// assert_eq!(core.borrow_mut::<u8>(), None);
    /// ```
    pub fn borrow_mut<T: UniqueId<V::Id>>(&mut self) -> Option<&mut T> {
        if self.type_descriptor().is_type::<T>() {
            // SAFETY:
            // We just checked that the current stored type is a `T`.
            Some(unsafe { self.borrow_mut_unchecked::<T>() })
        } else {
            None
        }
    }

    /// Return type [`Descriptor`] for the currently stored value.
    ///
    /// descriptor.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    /// use dungeon_cell::vtable::Descriptor;
    ///
    /// let core = DungeonCore::<layout_for!(i32)>::new(123i32);
    ///
    /// assert_eq!(*core.type_descriptor(), Descriptor::new::<i32>());
    ///
    /// assert_eq!(core.type_descriptor().size(), 4);
    /// assert_eq!(core.type_descriptor().alignment(), 4);
    /// assert_eq!(core.type_descriptor().type_name(), "i32");
    /// ```
    pub fn type_descriptor(&self) -> &Descriptor<V::Id> {
        // SAFETY: The vtable cannot be changed via the descriptor.
        let vtable = unsafe { &*storage_as_split_ptr::<L, V>(&self.inner).1 };

        vtable.descriptor()
    }

    /// Try to swap the values of this [`DungeonCore`] with another one.
    ///
    /// The values can only be swapped if they can be stored in
    /// the opposite core. A `true` will be returned if the swap happened.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let mut a = DungeonCore::<layout_for!(i32)>::new(123u8);
    /// let mut b = DungeonCore::<layout_for!(i16)>::new(456u16);
    ///
    /// assert!(a.try_swap(&mut b));
    ///
    /// assert_eq!(a.take::<u16>(), Some(456));
    /// assert_eq!(b.take::<u8>(), Some(123));
    /// ```
    ///
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let mut a = DungeonCore::<layout_for!(u8)>::new(123u8);
    /// let mut b = DungeonCore::<layout_for!(u16)>::new(456u16);
    ///
    /// assert!(!a.try_swap(&mut b));
    ///
    /// assert_eq!(a.take::<u8>(), Some(123));
    /// assert_eq!(b.take::<u16>(), Some(456));
    /// ```
    pub fn try_swap<L2: IsLayout + Cleanup<V>>(
        &mut self,
        other: &mut DungeonCore<L2, V>,
    ) -> bool {
        if self.type_descriptor().layout_can_store::<L2>()
            && other.type_descriptor().layout_can_store::<L>()
        {
            // This buffer doesn't need to be alligned because we are
            // not using it as a T.
            let mut temp = Buffer::<L::Size>::uninit();

            let (buffer, vtable) = {
                // SAFETY: By the end of the borrow the vtable once again
                // matches the stored value;
                unsafe { storage_as_split_ptr_mut::<L, V>(&mut self.inner) }
            };

            let (other_buffer, other_vtable) = {
                // SAFETY: By the end of the borrow the vtable once again
                // matches the stored value;
                unsafe { storage_as_split_ptr_mut::<L2, V>(&mut other.inner) }
            };

            // Other to temp.
            // SAFETY: The other_buffer is valid to read from because it must be storing
            // a value with size given by the vtable.
            // Temp is nonoverlaping with it because it's a new local variable.
            unsafe {
                core::ptr::copy_nonoverlapping(
                    other_buffer,
                    Buffer::as_ptr_mut(&mut temp),
                    (*other_vtable).descriptor().size(),
                );
            }

            // Self to other.
            // SAFETY: The buffer is valid to read from because it must be storing
            // a value with size given by the vtable.
            // other_buffer is nonoverlaping with it because DungeonCore owns
            // the inner values and we got two &mut at the start.
            unsafe {
                core::ptr::copy_nonoverlapping(
                    buffer,
                    other_buffer,
                    (*vtable).descriptor().size(),
                );
            }

            // Temp to self.
            // SAFETY: The temp is valid to read from because it must be storing
            // a value with size given by the vtable.
            // Buffer is nonoverlaping with it because temp was just made above.
            unsafe {
                core::ptr::copy_nonoverlapping(
                    Buffer::as_ptr(&temp),
                    buffer,
                    (*other_vtable).descriptor().size(),
                );
            }

            // Swap the vtables to match the new values.
            // SAFETY: Both pointers are valid because we got them from the storage
            // above.
            unsafe { core::ptr::swap(vtable, other_vtable) };

            true
        } else {
            false
        }
    }

    /// Convert the [`DungeonCore`] into a larger size or alignment [`DungeonCore`].
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let core = DungeonCore::<layout_for!(u8)>::new(123u8);
    /// let mut core = core.into_larger::<layout_for!(u16)>();
    ///
    /// assert_eq!(core.take::<u8>(), Some(123));
    /// ```
    ///
    /// ```compile_fail
    /// # let _ = dungeon_cell::HAS_CONST_PANIC;
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let core = DungeonCore::<layout_for!(u16)>::new(123u16);
    /// let core = core.into_larger::<layout_for!(u8)>();
    /// ```
    pub const fn into_larger<L2: IsLayout + Cleanup<V>>(
        self,
    ) -> DungeonCore<L2, V> {
        // The Buffer::new below checks the size but we need to check the alignment.
        const_usize_assert::<
            AlignmentSmallerOrEqualTo<
                <L::Alignment as IsAlignment>::Marker,
                L2::Alignment,
            >,
        >();

        let (buffer, vtable) = {
            // SAFETY: Self is repr(transparent) to the storage.
            unsafe { split_storage::<L, V>(const_transmute(self)) }
        };

        let new_buffer =
            Buffer::<L2::Size>::new(ManuallyDrop::into_inner(buffer));

        // SAFETY: The vtable matches the stored value because we just got it from
        // a DungeonCore.
        let inner = unsafe {
            into_storage::<L2, V>(new_buffer, ManuallyDrop::into_inner(vtable))
        };

        DungeonCore { inner }
    }

    /// Try to convert the [`DungeonCore`] into a smaller size or alignment [`DungeonCore`]
    ///
    /// This is only possible if the value stored can be stored in the smaller [`DungeonCore`].
    /// A `Err` with the current [`DungeonCore`] will be returned if the value cannot be
    /// stored in the smaller core.
    ///
    /// This method will return `Ok` for all cases where [`Self::into_larger()`] works.
    ///
    /// # Examples
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let core = DungeonCore::<layout_for!(u16)>::new(123u8);
    /// let mut core = core.try_into_smaller::<layout_for!(u8)>().unwrap();
    ///
    /// assert_eq!(core.take::<u8>(), Some(123));
    /// ```
    ///
    /// ```
    /// use dungeon_cell::{DungeonCore, layout_for};
    ///
    /// let core = DungeonCore::<layout_for!(u16)>::new(123u16);
    ///
    /// assert!(core.try_into_smaller::<layout_for!(u8)>().is_err());
    /// ```
    pub fn try_into_smaller<L2: IsLayout + Cleanup<V>>(
        self,
    ) -> Result<DungeonCore<L2, V>, Self> {
        if self.type_descriptor().layout_can_store::<L2>() {
            // This buffer doesn't need to be aligned because we copy
            // by byte.
            let mut new_buffer = Buffer::<L2::Size>::uninit();

            let (buffer, vtable) = split_storage::<L, V>(self.inner);

            // SAFETY: We checked above that the value in the buffer
            // can fit in the new buffer.
            unsafe {
                core::ptr::copy_nonoverlapping(
                    Buffer::as_ptr(&ManuallyDrop::into_inner(buffer)),
                    Buffer::as_ptr_mut(&mut new_buffer),
                    vtable.descriptor().size(),
                );
            }

            // SAFETY: The vtable is for the stored value because we just got
            // it from a DungeonCore.
            let inner = unsafe {
                into_storage::<L2, V>(
                    new_buffer,
                    ManuallyDrop::into_inner(vtable),
                )
            };

            Ok(DungeonCore { inner })
        } else {
            Err(self)
        }
    }

    pub fn into_less_bounds<Bnew>(
        self,
    ) -> DungeonCore<L, <V as VTableSubset<Bnew>>::Subset>
    where
        V: VTableSubset<Bnew>,
        L: Cleanup<<V as VTableSubset<Bnew>>::Subset>,
    {
        let (buffer, vtable) = split_storage::<L, V>(self.inner);

        // SAFETY: The new vtable is for the same value because
        // of the safety bounds on VTableSubset.
        let inner = unsafe {
            into_storage::<L, <V as VTableSubset<Bnew>>::Subset>(
                ManuallyDrop::into_inner(buffer),
                ManuallyDrop::into_inner(vtable).into_subset(),
            )
        };

        DungeonCore { inner }
    }
}

impl<L: IsLayout + Cleanup<V>, V: VTable + Copy> DungeonCore<L, V> {
    pub fn store_with_coercion<T>(&mut self, value: T, coercion: &CoercionFor<T, V>) {
        // Assert the alignment is correct because we give out borrows.
        const_usize_assert::<AlignmentSmallerOrEqualTo<T, L::Alignment>>();

        // This asserts that the size is correct so we don't have to check.
        let buffer = Buffer::<L::Size>::new(value);

        // SAFETY: The buffer has a valid value because we just made it from value.
        // The vtable matches the value because of the safety requirements of the function.
        let inner = unsafe { into_storage::<L, V>(buffer, coercion.vtable()) };

        // Replace the old inner with the new value.
        // We can now drop the value in the old_inner without causing a double drop.
        // By dropping Inner it will automatically run the drop code for the value
        // it is storing if it needs to.
        //
        // If the drop code panics then this DungeonCore has a new value to drop.
        let _ = ::core::mem::replace(&mut self.inner, inner);
    }
}

impl<L: IsLayout + Cleanup<V>, V: VTable> DungeonCore<L, V> {
    /// Create [`DungeonCore`] storing a given value and vtable.
    ///
    /// # Safety
    /// The `vtable` must be for the type `T`.
    ///
    /// A valid vtable for a type `T` can be created using
    /// [`VTableOf`] or [`ConstVTableOf`].
    #[track_caller]
    pub const unsafe fn new_unchecked<T>(value: T, vtable: V) -> Self {
        // Assert the alignment is correct because we give out borrows.
        const_usize_assert::<AlignmentSmallerOrEqualTo<T, L::Alignment>>();

        // This asserts that the size is correct so we don't have to check.
        let buffer = Buffer::<L::Size>::new(value);

        // SAFETY: The buffer has a valid value because we just made it from value.
        // The vtable matches the value because of the safety requirements of the function.
        let inner = unsafe { into_storage::<L, V>(buffer, vtable) };

        // Convert into the inner type.
        DungeonCore { inner }
    }

    /// Unchecked form of [Self::store].
    ///
    /// The stored value will be forgotten. Its drop logic will **not** be ran.
    ///
    /// # Safety
    /// The `vtable` must be for the type `T`.
    ///
    /// A valid vtable for a type `T` can be created using
    /// [`VTableOf`] or [`ConstVTableOf`].
    #[track_caller]
    pub unsafe fn store_unchecked<T>(&mut self, value: T, vtable: V) {
        // We must check that we can store the type in the layout.
        LayoutType::<L>::assert_can_store::<T>();

        let (buffer, vtable_mut) = {
            // SAFETY: The vtable is for the stored value once the borrow ends.
            unsafe { storage_as_split_ptr_mut::<L, V>(&mut self.inner) }
        };

        // SAFETY: We checked above that the type cal be stored in the layout.
        unsafe { move_into_buffer(buffer, value) };

        // A panic can't happen here.

        // Set the current type to `T`.
        // SAFETY: The vtable is valid to deref because its in the storage.
        unsafe {
            *vtable_mut = vtable;
        }
    }

    /// Unchecked form of [`Self::replace`].
    ///
    /// The vtable instance for the value is also returned.
    ///
    /// # Safety
    /// - The [`DungeonCore`] must be storing a valid value of type `U`.
    /// - The `vtable` must be for the type `T`.
    #[track_caller]
    pub unsafe fn replace_unchecked<T, U>(
        &mut self,
        value: T,
        vtable: V,
    ) -> (U, V) {
        // Assert the alignment is correct because we give out borrows.
        const_usize_assert::<AlignmentSmallerOrEqualTo<T, L::Alignment>>();

        // This asserts that the size is correct so we don't have to check.
        let buffer = Buffer::<L::Size>::new(value);

        // SAFETY: The buffer has a valid value because we just made it from value.
        // The vtable matches the value because of the safety requirements of the function.
        let inner = unsafe { into_storage::<L, V>(buffer, vtable) };

        // Replace the old inner with the new value.
        let old_inner = ::core::mem::replace(&mut self.inner, inner);

        // Convert old inner into the buffer and vtable.
        let (buffer, vtable) = split_storage::<L, V>(old_inner);

        // Convert the buffer back into a value.
        // SAFETY: We know this is a value of U because of the safety requirements
        // of the function.
        let old_value = unsafe {
            Buffer::into_value::<U>(ManuallyDrop::into_inner(buffer))
        };

        (old_value, ManuallyDrop::into_inner(vtable))
    }

    /// Convert into a type.
    ///
    /// The returned values are wrapped in [`ManuallyDrop`] for easier use in
    /// const contexts.
    ///
    /// # Safety
    /// The currently stored type must be `T`.
    #[must_use]
    pub const unsafe fn into_unchecked<T>(
        self,
    ) -> (ManuallyDrop<T>, ManuallyDrop<V>) {
        let (buffer, vtable) = {
            // SAFETY: Self is repr(transparent) to the storage type.
            unsafe { split_storage::<L, V>(const_transmute(self)) }
        };

        // SAFETY: By the requirements of the function, the buffer is holding a
        // value of T.
        unsafe {
            (
                ManuallyDrop::new(
                    ManuallyDrop::into_inner(buffer).into_value::<T>(),
                ),
                vtable,
            )
        }
    }

    /// Unchecked form of [Self::borrow].
    ///
    /// # Safety
    /// The [`DungeonCore`] must be storing a valid value of type `T`.
    pub const unsafe fn borrow_unchecked<'a, T>(&'a self) -> &'a T {
        // SAFETY: The buffer stays the same type because we only give out
        // a &T.
        let (buffer, _) = unsafe { storage_as_split_ptr::<L, V>(&self.inner) };

        // SAFETY: The function safety requirements require that the core
        // is storing a value of `T`.
        unsafe { buffer_as_type::<'a, T>(buffer) }
    }

    /// Unchecked form of [Self::borrow_mut].
    ///
    /// # Safety
    /// The [`DungeonCore`] must be storing a valid value of type `T`.
    pub unsafe fn borrow_mut_unchecked<T>(&mut self) -> &mut T {
        let (buffer, _) = {
            // SAFETY: The buffer stays the same type because we only give out
            // a &mut T.
            unsafe { storage_as_split_ptr_mut::<L, V>(&mut self.inner) }
        };

        // SAFETY: The function safety requirements require that the core
        // is storing a value of `T`.
        unsafe { buffer_as_type_mut::<T>(buffer) }
    }

    /// Get the internal vtable and buffer of the [`DungeonCore`].
    ///
    /// # Invariants
    /// The buffer contains a valid value of the type the vtable is for.
    /// The buffer has the layout described by generic `L`.
    /// The type implements the bounds given by `B`.
    ///
    /// # Safety
    /// After the borrow of `self` ends, the buffer must still contain a valid
    /// value of the type the vtable is for. The vtable must be valid in the event of a panic.
    pub const unsafe fn raw_internals(
        this: *const Self,
    ) -> (*const MaybeUninit<u8>, *const V) {
        // SAFETY: The safety requirements of the function makes sure that
        // the vtable can drop the buffer when it is dropped.
        unsafe { storage_as_split_ptr::<L, V>(addr_of!((*this).inner)) }
    }

    /// Get the internal vtable and buffer of the [`DungeonCore`] mutably.
    ///
    /// # Invariants
    /// The buffer contains a valid value of the type the vtable is for.
    /// The buffer has the layout described by generic `L`.
    /// The type implements the bounds given by `B`.
    ///
    /// # Safety
    /// After the borrow of `self` ends, the buffer must still contain a valid
    /// value of the type the vtable is for. Both the vtable and value in the buffer
    /// can change. The vtable and buffer must be valid in the event of a panic.
    pub unsafe fn raw_internals_mut(
        this: *mut Self,
    ) -> (*mut MaybeUninit<u8>, *mut V) {
        // SAFETY: The safety requirements of the function makes sure that
        // the vtable can drop the buffer when it is dropped.
        unsafe { storage_as_split_ptr_mut::<L, V>(addr_of_mut!((*this).inner)) }
    }

    /// Convert into the internal vtable and buffer.
    pub const fn into_raw_internals(
        self,
    ) -> (ManuallyDrop<Buffer<L::Size>>, ManuallyDrop<V>) {
        // SAFETY: DungeonCore is `repr(transparent)` to the inner type.
        let inner = unsafe { const_transmute(self) };

        split_storage::<L, V>(inner)
    }
}

impl<L: IsLayout + Cleanup<V>, V: VTable> core::fmt::Debug
    for DungeonCore<L, V>
{
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let (buffer, vtable) = {
            // SAFETY: The value of the vtable is not changed.
            unsafe { storage_as_split_ptr::<L, V>(&self.inner) }
        };

        // SAFETY: The vtable is valid to dereference because of DungeonCore's
        // invariants. The vtable is always valid.
        let vtable = unsafe { &*vtable };

        if <V::Bounds as IsBound>::BOUND_BY_DEBUG {
            // SAFETY: We checked that the bound has Debug.
            unsafe { vtable.bound_impl().debug_value(buffer, f) }
        } else {
            f.debug_struct("DungeonCore")
                .field("type_name", &vtable.descriptor().type_name())
                .field("size", &vtable.descriptor().size())
                .field("alignment", &vtable.descriptor().alignment())
                .finish()
        }
    }
}

/// Move a value into a byte buffer.
///
/// The buffer will gain ownership of the value.
///
/// # Safety
/// The `buffer` slice must be the size and alignment of `T` (or bigger).
unsafe fn move_into_buffer<T>(buffer: *mut MaybeUninit<u8>, value: T) {
    // Wrap the value so we can manually move it.
    // `value` will be considered invalid after the read below.
    let value = MaybeUninit::new(value);

    // Get a pointer to the aligned buffer.
    // This is done in long form to make it easier to read.
    let buffer: *mut MaybeUninit<T> = buffer.cast();

    // Manually move the value out of binding `value`.

    // SAFETY:
    // `value` is valid for a read because we just made it above
    // from a valid value.
    //
    // `buffer` is valid for a write because of the safety requirements
    // for the outer function.
    //
    // They do not overlap.
    unsafe {
        core::ptr::copy_nonoverlapping(addr_of!(value), buffer, 1);
    }

    // Value will not have it's drop code run.
}

/// View a byte buffer as a type `T`.
///
/// # Safety
/// The `buffer` slice must contain a value of type `T`.
const unsafe fn buffer_as_type<'a, T>(buffer: *const MaybeUninit<u8>) -> &'a T {
    // Get a pointer to the aligned buffer.
    // This is done in long form to make it easier to read.
    let buffer: *const MaybeUninit<T> = buffer.cast();

    // SAFETY:
    // We just borrowed `buffer` so it's valid to dereference.
    let buffer: &MaybeUninit<T> = unsafe { &*buffer };

    // SAFETY:
    // The outer safety requirements makes this valid.
    // `buffer` is a valid value of `T`.
    unsafe { buffer.assume_init_ref() }
}

/// View a mutable byte buffer as a type `T`.
///
/// # Safety
/// The `buffer` slice must contain a value of type `T`.
unsafe fn buffer_as_type_mut<'a, T>(buffer: *mut MaybeUninit<u8>) -> &'a mut T {
    // Get a pointer to the aligned buffer.
    // This is done in long form to make it easier to read.
    let buffer: *mut MaybeUninit<T> = buffer.cast();

    // SAFETY:
    // We just borrowed `buffer` so it's valid to dereference.
    let buffer: &mut MaybeUninit<T> = unsafe { &mut *buffer };

    // SAFETY:
    // The outer safety requirements makes this valid.
    // `buffer` is a valid value of `T`.
    unsafe { buffer.assume_init_mut() }
}

#[cfg(test)]
mod test {
    // use crate::{vtable::{Descriptor, CoreVTable}, Prison};

    #[test]
    fn a() {
        // type X = Prison<&'static CoreVTable>;
        // let x: Descriptor<X> = Descriptor::of::<u8>();
        //
        // let x: Descriptor<X> = Descriptor::<X>::of::<u8>();
    }
}
