use dungeon_cell::layout::{Alignment, Layout, Size};
use dungeon_cell::{layout_for, DungeonCore};

#[test]
fn correct_layout() {
    type A = DungeonCore<layout_for!()>;
    type B = DungeonCore<layout_for!(u8)>;
    type C = DungeonCore<layout_for!(u32)>;
    type D = DungeonCore<layout_for!(u64)>;
    type E = DungeonCore<layout_for!(u128)>;

    fn assert<T, U>() {
        assert_eq!(
            std::mem::size_of::<T>(),
            std::mem::size_of::<U>() + std::mem::size_of::<usize>(),
            "size doesn't match expected for {}",
            std::any::type_name::<U>()
        );

        assert_eq!(
            std::mem::align_of::<T>(),
            std::mem::align_of::<U>(),
            "alignmnet doesn't match expected for {}",
            std::any::type_name::<U>()
        );
    }

    assert::<A, ()>();
    assert::<B, u8>();
    assert::<C, u32>();
    assert::<D, u64>();
    assert::<E, u128>();
}

#[test]
fn const_new() {
    const _X: DungeonCore<layout_for!(i32)> = DungeonCore::new(123_i32);
    // std::hint::black_box(X);
}

#[test]
fn const_a() {
    type X = DungeonCore<Layout<Size<1>, Alignment<16>>>;

    assert_eq!(std::mem::size_of::<X>(), 16,);

    assert_eq!(std::mem::align_of::<X>(), 16,);
}
