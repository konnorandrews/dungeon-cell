use dungeon_cell::{Alignment, DungeonCell, Size};

fn main() {
    let x: DungeonCell<Size<24>, Alignment<8>> = DungeonCell::default();

    println!("Size {}", std::mem::size_of_val(&x));
    println!("Align {}", std::mem::align_of_val(&x));

    x.set(1234_i32);
    println!("{:?}", x.take::<i32>());

    x.set(1.234_f32);
    println!("{:?}", x.take::<f32>());

    x.set(1234_i32);
    println!("{:?}", x.take::<f32>());

    x.set("hello");
    println!("{:?}", x.take::<&'static str>());

    x.set("world".to_owned());
    println!("{:?}", x.take::<String>());

    x.set("a".to_owned());
    x.set("b".to_owned());
    println!("{:?}", x.take::<String>());
}
