extern crate dungeon_cell;

use std::sync::Mutex;

use _core::cell::UnsafeCell;
use dungeon_cell::bound::traits::__;
use dungeon_cell::bound::{bounds, traits, Bound};
use dungeon_cell::layout::{Alignment, Layout, Size};
use dungeon_cell::vtable::BasicVTable;
use dungeon_cell::{layout_for, DefaultVtable, DungeonCore};
use static_assertions::*;

assert_not_impl_all!(DungeonCore<layout_for!(), DefaultVtable<bounds::Empty>>: Send);
assert_impl_all!(DungeonCore<layout_for!(), DefaultVtable<bounds::Send>>: Send);

static TEST: DungeonCore<layout_for!(i32)> = DungeonCore::new(42);

type DebugClone = Bound<__, __, __, traits::Clone, __, traits::Debug>;

static X: DungeonCore<
    layout_for!(Mutex<i32>),
    DefaultVtable<bounds::SendSync>,
> = DungeonCore::new(Mutex::new(123));

// const Z: &DungeonCore<
//     layout_for!(Mutex<i32>),
//     DefaultVtable<bounds::SendSync>,
// > = &DungeonCore::new(Mutex::new(123));

const W: DungeonCore<layout_for!(i32), DefaultVtable<bounds::Debug>> =
    DungeonCore::new(UnsafeCell::new(123));

fn main() {
    #![forbid(unsafe_code)]

    let mut core = DungeonCore::<
        layout_for!(i32, f64),
        DefaultVtable<bounds::Empty>,
    >::new(123);

    dungeon_cell::by_type!(match &mut core {
        int @ &mut i32 => {
            let x = *int as f64 + 101.0;
            dbg!(int);
            core.store(x);
        }
        __ => {}
    });

    dbg!(core);

    // test static
    println!("{:?}", TEST.borrow::<i32>());

    let mutex: &Mutex<i32> = X.borrow().unwrap();
    *mutex.lock().unwrap() += 12;

    let mutex: &Mutex<i32> = X.borrow().unwrap();
    dbg!(mutex.lock().unwrap());

    dbg!(W);

    // test debug
    let y: Vec<DungeonCore<layout_for!(String)>> = vec![
        DungeonCore::new(String::from("hello")),
        DungeonCore::new(1234),
        DungeonCore::new(123.431),
    ];
    dbg!(y);

    // test clone
    let y: DungeonCore<layout_for!(String), DefaultVtable<DebugClone>> =
        DungeonCore::new(String::from("hello"));

    let z = Clone::clone(&y);

    println!("{:?}", y.borrow::<String>());
    println!("{:?}", z.borrow::<String>());

    // test many types
    let mut x: DungeonCore<layout_for!(i32, f32, &'static str, String)> =
        DungeonCore::new(());

    println!("Size {}", std::mem::size_of_val(&x));
    println!("Align {}", std::mem::align_of_val(&x));

    dbg!(&x);
    println!("{:?}", x.take::<i32>());

    x.store(1.234_f32);

    match &x {
        x if x.type_descriptor().is_type::<f32>() => {
            let x = x.borrow::<f32>().unwrap();
            println!("float: {}", x);
        }
        x if x.type_descriptor().is_type::<i32>() => {
            let x = x.borrow::<i32>().unwrap();
            println!("int: {}", x);
        }
        _ => {}
    }

    if let Some(x) = x.borrow::<f32>() {
        println!("float: {}", x);
    } else if let Some(x) = x.borrow::<i32>() {
        println!("int: {}", x);
    }

    dbg!(&x);
    println!("{:?}", x.take::<f32>());

    x.store(1234_i32);
    println!("{:?}", x.take::<f32>());

    x.store("hello");
    println!("{:?}", x.take::<&'static str>());

    x.store("world".to_owned());
    println!("{:?}", x.take::<String>());

    x.store("a".to_owned());
    x.store("b".to_owned());
    println!("{:?}", x.take::<String>());

    x.store("hello".to_owned());
    x.borrow_mut::<String>().unwrap().push_str(" world");
    println!("{:?}", x.borrow::<String>());

    let mut x: DungeonCore<Layout<Size<32>, Alignment<8>>> = x.into_larger();
    println!("Size {}", std::mem::size_of_val(&x));
    println!("Align {}", std::mem::align_of_val(&x));
    println!("{:?}", x.borrow::<String>());

    x.store("smaller");

    let x: DungeonCore<Layout<Size<16>, Alignment<8>>> =
        x.try_into_smaller().unwrap();
    println!("size {}", std::mem::size_of_val(&x));
    println!("align {}", std::mem::align_of_val(&x));
    println!("{:?}", x.borrow::<&'static str>());

    let x = x.into_less_bounds::<bounds::Send>();

    std::thread::spawn(move || {
        println!("size {}", std::mem::size_of_val(&x));
        println!("align {}", std::mem::align_of_val(&x));
        println!("{:?}", x.borrow::<&'static str>());
    })
    .join()
    .unwrap();

    // ===

    let mut z = 42i32;

    let mut x: DungeonCore<
        layout_for!(i32, f32, &'static str, String),
        &BasicVTable<bounds::Normal>,
    > = DungeonCore::new(&z);

    dbg!(x.type_descriptor());

    dbg!(x.borrow::<&i32>());

    // z += 123;
    x.store("hi");

    dbg!(x.borrow::<&str>());

    // {
    //     let z = 123i32;
    //     x.store(&z);
    // }

    dbg!(x.borrow::<&i32>());

    let mut z = 42i32;

    let mut y = dungeon_cell::give_opaque_static_form!(|| z += 1);

    y.0();

    {
        let mut x: DungeonCore<
            layout_for!(&mut i32),
            &BasicVTable<bounds::Empty>,
        > = DungeonCore::new(y);
    }

    dbg!(z);

    hi();
}

fn hi() {
    // let mut z = 42i32;
    //
    // let mut x: DungeonCore<
    //     layout_for!(i32, f32, &'static str, String),
    //     bounds::Normal,
    //     &LifetimeVTable,
    // > = DungeonCore::new(&mut z);
    //
    // {
    //     let mut y = 123i32;
    //     x.store(&mut y);
    // }
}

type X<'a> = DungeonCore<
    layout_for!(i32, f32, &'static str, String),
    &'a BasicVTable<'a, bounds::Normal>,
>;

// fn evil<'a: 'b, 'b>(x: X<'a>) -> X<'b> {
//     x
// }
//
// fn not_evil<'a: 'b, 'b>(x: Box<&'a i32>) -> Box<&'b i32> {
//     x
// }
