use std::ops::{Deref, DerefMut};
use std::rc::Rc;

use dungeon_cell::bound::bounds;
use dungeon_cell::vtable::{BasicVTable, CoercionVTable, Lifetime, Static};
use dungeon_cell::{coerce, layout_for, DungeonCore, Mut};

// static STDIN_AS_READ: Coercion<std::io::Stdin, dyn std::io::Read, CoerceMut> =
//     coerce!(std::io::Stdin as dyn std::io::Read);

// static CURSOR_STR_AS_READ: Coercion<std::io::Cursor<&'static str>, dyn std::io::Read, CoerceMut> =
//     coerce!(std::io::Cursor<&'static str> as dyn std::io::Read);

// fn bytes_as_str<const N: usize>() -> Coercion<[u8; N], str, CoerceMut> {
//     coerce!(
//         [u8; N] as str,
//         ref |x| std::str::from_utf8(unsafe { &*x }).unwrap(),
//         mut |x| std::str::from_utf8_mut(unsafe { &mut *x }).unwrap()
//     )
// }

// fn array_as_slice<const N: usize, T>() -> Coercion<[T; N], [T], CoerceMut> {
//     coerce!([T; N] as [T])
// }

// fn unsized_as_u_mut<U, const S: usize, const A: usize, B>(
// ) -> Coercion<DungeonUnsized<U, S, A, B, CoerceMut>, U, CoerceMut>
// where
//     B: AlignableBuffer<S>,
//     Alignment<A>: ValidAlignment,
//     U: ?Sized,
// {
//     coerce!(deref DungeonUnsized<U, S, A, B, CoerceMut> as U)
// }

// fn unsized_as_u<U, const S: usize, const A: usize, B>(
// ) -> Coercion<DungeonUnsized<U, S, A, B, CoerceRef>, U, CoerceRef>
// where
//     Alignment<A>: ValidAlignment,
//     U: ?Sized,
// {
//     coerce!(deref DungeonUnsized<U, S, A, B, CoerceRef> as ref U)
// }

// static STRING_AS_STR: Coercion<String, str, CoerceMut> = coerce!(deref String as str);

static STATIC_STR_AS_STR: dungeon_cell::vtable::CoercionFor<
    &'static str,
    &'static CoercionVTable<
        'static,
        str,
        Mut<false>,
        BasicVTable<bounds::Normal, Lifetime>,
    >,
> = coerce!(deref &'static str as ref str);

// static X: DungeonUnsized<
//     dyn Fn(&mut i32),
//     layout_for!(()),
//     IsSend<true>,
//     IsCoerceMut<false>,
// > = DungeonUnsized::<_, _, IsSend<true>, _>::new(|_: &mut i32| {});

fn main() {
    let x = String::from("test2");
    let y = &*x;

    let mut x = DungeonCore::<
        layout_for!(String),
        &CoercionVTable<str, Mut<false>, BasicVTable<bounds::Normal, Lifetime>>,
    >::new("test");

    // x.store(y);
    x.store_with_coercion("hi", &STATIC_STR_AS_STR);

    dbg!(&*x);

    // x.store(String::from("hi"));
    // dbg!(&*x);
}

// use num_traits::cast::ToPrimitive;
//
// auto_coerce_wrapper! {
//     struct CoerceToPrimitive<T: ToPrimitive>(pub T);
// }
//
// auto_coerce_wrapper! {
//     struct CoerceFn<T: Fn(&mut i32)>(pub T);
// }
//
// trait A {}
// impl A for () {}
//
// auto_coerce!(A);
//
// fn main() {
//     let mut x = DungeonUnsized::<dyn ToPrimitive, layout_for!(u8)>::new(
//         CoerceToPrimitive(123u8),
//     );
//     dbg!(x.to_i32());
//
//     x.set(CoerceToPrimitive(-1i8));
//     dbg!(x.to_i32());
//
//     let x = DungeonUnsized::<dyn A, layout_for!()>::new(());
//
//     auto_coerce_wrapper! {
//         struct CoerceFn<T: Fn(&mut i32)>(pub T);
//     }
//
//     let x =
//         DungeonUnsized::<dyn Fn(&mut i32), layout_for!()>::new(CoerceFn(|x| {
//             *x += 1
//         }));
//     let mut y = 0;
//     (x)(&mut y);
//     dbg!(y);
//
//     // let x = DungeonUnsized::<dungeon_cell::Unsize<dyn Fn(&mut i32)>, layout_for!()>::new(|x: &mut i32| *x += 1);
//     // let mut y = 0;
//     // (x)(&mut y);
//     // dbg!(y);
//
//     // let mut x = 0;
//     // dbg!(X(&mut x));
//     // let mut y =
//     //     DungeonUnsized::new_mut::<dyn std::io::Read>(Config::new().size::<24>().alignment::<8>());
//
//     // y.store(std::io::stdin(), STDIN_AS_READ);
//     // let mut x = [0u8; 1];
//     // y.read(&mut x).unwrap();
//     // dbg!(x);
//
//     // y.store(std::io::Cursor::new("hello"), CURSOR_STR_AS_READ);
//     // let mut x = [0u8; 2];
//     // y.read(&mut x).unwrap();
//     // dbg!(x);
//     // y.read(&mut x).unwrap();
//     // dbg!(x);
//
//     let mut y = DungeonUnsized::<
//         str,
//         layout_for!(String),
//         IsSend<true>,
//         IsCoerceMut<false>,
//     >::new("abc");
//
//     dbg!(&*y);
//
//     y.set("static");
//     dbg!(&*y);
//
//     y.set(String::from("String"));
//     dbg!(&*y);
//
//     let mut y = DungeonUnsized::<[i32], layout_for!([i32; 4])>::new([]);
//     dbg!(&*y);
//
//     y.set([1]);
//     dbg!(&*y);
//
//     y.set([1, 2]);
//     dbg!(&*y);
//
//     y.set([1, 2, 3]);
//     dbg!(&*y);
//
//     y.set([1, 2, 3, 4]);
//     dbg!(&*y);
//
//     // println!("Size {}", std::mem::size_of_val(&y));
//     // println!("Align {}", std::mem::align_of_val(&y));
//     //
//     // y.set(*b"bytes");
//     // dbg!(&*y);
//     //
//     // y.store_with_coercion("static", STATIC_STR_AS_STR);
//     // dbg!(&*y);
//     //
//     // y.store(String::from("String"), STRING_AS_STR);
//     // dbg!(&*y);
//
//     // let mut y = DungeonUnsized::new::<[u32]>(Config::new().size::<16>().alignment::<4>());
//     // y.store([], array_as_slice());
//     // dbg!(&*y);
//     // y.store([1], array_as_slice());
//     // dbg!(&*y);
//     // y.store([1, 2], array_as_slice());
//     // dbg!(&*y);
//     // y.store([1, 2, 3], array_as_slice());
//     // dbg!(&*y);
//     // y.store([1, 2, 3, 4], array_as_slice());
//     // dbg!(&*y);
//
//     // let mut z = DungeonUnsized::new::<[u32]>(Config::new().size::<32>().alignment::<8>());
//     // z.store(y, unsized_as_u());
//     // dbg!(&*z);
//
//     let mut y = DungeonUnsized::<
//         dyn Fn(i32) -> i32,
//         layout_for!(i32),
//         IsSend<false>,
//         IsCoerceMut<false>,
//     >::new(|x: i32| x);
//
//     println!("Size {}", std::mem::size_of_val(&y));
//     println!("Align {}", std::mem::align_of_val(&y));
//
//     let a = 0;
//     dbg!((y)(a), a);
//
//     let z = |x: i32| x + 1;
//     y.set(z);
//     dbg!((y)(a), a);
//
//     let b = 5;
//     let z = move |x: i32| x + b;
//     y.set(z);
//     dbg!((y)(a), a);
// }
//
// #[no_mangle]
// pub fn test(
//     x: &DungeonUnsized<
//         dyn Fn(i32),
//         layout_for!(()),
//         IsSend<false>,
//         IsCoerceMut<false>,
//     >,
// ) {
//     (x)(42);
//     // print!("");
// }
//
// #[no_mangle]
// pub fn test2(x: &Rc<dyn Fn(i32)>) {
//     (x)(42);
//     print!("");
// }
//
// #[no_mangle]
// pub fn test3() {
//     let x = DungeonUnsized::<
//         dyn Fn(),
//         layout_for!(()),
//         IsSend<false>,
//         IsCoerceMut<false>,
//     >::new(|| println!("test"));
//     (x)()
// }
