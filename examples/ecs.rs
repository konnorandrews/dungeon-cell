use std::any::TypeId;
use std::collections::{HashMap, HashSet};

use dungeon_cell::bound::bounds;
use dungeon_cell::layout_for;
use slotmap::{DefaultKey, SlotMap};

type DungeonCore<L> = dungeon_cell::DungeonCore<L, bounds::Empty>;

fn main() {
    let mut ecs = Ecs::new();

    ecs.add_system(print_people);

    let world = ecs.world_mut();

    let a = world.spawn();
    world.add_component(a, Person);
    world.add_component(a, Name("foo".into()));

    let b = world.spawn();
    world.add_component(b, Person);
    world.add_component(b, Name("bar".into()));

    ecs.step();
}

struct Person;

struct Name(String);

fn print_people(world: &mut World) {
    for person in
        world.query(|entity| entity.has::<Person>() && entity.has::<Name>())
    {
        println!("Person with name {}", world.get::<Name>(person).unwrap().0);
    }
}

struct World {
    map: TypeMap,
    entities: SlotMap<DefaultKey, EntityInfo>,
}

struct EntityInfo {
    components: HashSet<(TypeId, DefaultKey)>,
}

impl EntityInfo {
    fn has<T: 'static>(&self) -> bool {
        self.components
            .iter()
            .any(|(id, _)| *id == TypeId::of::<T>())
    }
}

impl World {
    fn spawn(&mut self) -> DefaultKey {
        self.entities.insert(EntityInfo {
            components: HashSet::new(),
        })
    }

    fn add_component<T: Send + 'static>(
        &mut self,
        entity: DefaultKey,
        component: T,
    ) {
        if let Some(entity) = self.entities.get_mut(entity) {
            let key = self.map.get_mut::<T>().insert(component);
            entity.components.insert((TypeId::of::<T>(), key));
        }
    }

    fn get<T: 'static>(&self, entity: DefaultKey) -> Option<&T> {
        self.entities
            .get(entity)
            .and_then(|entity| {
                entity.components.iter().find(|(component_type, _)| {
                    *component_type == TypeId::of::<T>()
                })
            })
            .and_then(|(_, key)| {
                self.map
                    .get::<T>()
                    .map(|components| components.get(*key).unwrap())
            })
    }

    fn query<'a, F: 'a + FnMut(&EntityInfo) -> bool>(
        &'a self,
        mut filter: F,
    ) -> impl Iterator<Item = DefaultKey> + 'a {
        self.entities
            .iter()
            .filter(move |(_, info)| filter(info))
            .map(|(key, _)| key)
    }
}

struct Ecs {
    world: World,
    systems: Vec<fn(&mut World)>,
}

impl Ecs {
    fn new() -> Self {
        Self {
            world: World {
                map: TypeMap::new(),
                entities: SlotMap::new(),
            },
            systems: Vec::new(),
        }
    }

    fn add_system(&mut self, system: fn(&mut World)) {
        self.systems.push(system);
    }

    fn world_mut(&mut self) -> &mut World {
        &mut self.world
    }

    fn world(&self) -> &World {
        &self.world
    }

    fn step(&mut self) {
        for system in &mut self.systems {
            (system)(&mut self.world)
        }
    }
}

struct TypeMap {
    map: HashMap<TypeId, DungeonCore<layout_for!(SlotMap<DefaultKey, ()>)>>,
}

impl TypeMap {
    fn new() -> Self {
        Self {
            map: HashMap::new(),
        }
    }

    fn get_mut<T: Send + 'static>(&mut self) -> &mut SlotMap<DefaultKey, T> {
        self.map
            .entry(TypeId::of::<T>())
            .or_insert_with(|| DungeonCore::new(SlotMap::<_, T>::new()))
            .borrow_mut()
            .unwrap()
    }

    fn get<T: 'static>(&self) -> Option<&SlotMap<DefaultKey, T>> {
        self.map
            .get(&TypeId::of::<T>())
            .map(|vec| vec.borrow::<SlotMap<_, T>>().unwrap())
    }
}
